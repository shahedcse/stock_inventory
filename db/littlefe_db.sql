-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 29, 2019 at 06:04 AM
-- Server version: 10.2.30-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `littlefe_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_cost`
--

CREATE TABLE `additional_cost` (
  `id` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `purpose` text DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `remarks` varchar(150) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `additional_cost`
--

INSERT INTO `additional_cost` (`id`, `date`, `purpose`, `amount`, `remarks`, `created_at`) VALUES
(1, '2019-12-24', 'Food Bill', 200.00, 'kjhgjhgjhg', '2019-12-24 04:40:09'),
(2, '2019-12-17', 'Mobile Bill', 500.00, 'hgfhfhhgjhhg', '2019-12-24 04:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `allblood_group`
--

CREATE TABLE `allblood_group` (
  `id` int(11) NOT NULL,
  `blood_group` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allblood_group`
--

INSERT INTO `allblood_group` (`id`, `blood_group`) VALUES
(1, 'A+'),
(2, 'O+'),
(3, 'B+'),
(4, 'AB+'),
(5, 'A-'),
(6, 'O-'),
(7, 'B-'),
(8, 'AB-');

-- --------------------------------------------------------

--
-- Table structure for table `challan`
--

CREATE TABLE `challan` (
  `id` int(11) NOT NULL,
  `invoiceno` varchar(26) DEFAULT NULL,
  `parchase` varchar(26) DEFAULT NULL,
  `order_date` date NOT NULL,
  `challan_type` smallint(4) DEFAULT NULL,
  `dealerid` int(11) DEFAULT NULL,
  `customerid` int(11) DEFAULT NULL,
  `create_date` date NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan`
--

INSERT INTO `challan` (`id`, `invoiceno`, `parchase`, `order_date`, `challan_type`, `dealerid`, `customerid`, `create_date`, `created_by`) VALUES
(1, '396542', '...................', '2019-12-28', 2, 0, 210, '2019-12-29', 10);

-- --------------------------------------------------------

--
-- Table structure for table `challan_details`
--

CREATE TABLE `challan_details` (
  `id` int(11) NOT NULL,
  `challanmaster` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `batch` varchar(26) DEFAULT NULL,
  `exdate` varchar(56) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challan_details`
--

INSERT INTO `challan_details` (`id`, `challanmaster`, `productid`, `qty`, `batch`, `exdate`) VALUES
(2, 1, 2, 100, 'FBDBGA06', '2022-05-25');

-- --------------------------------------------------------

--
-- Table structure for table `cost_category`
--

CREATE TABLE `cost_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cost_category`
--

INSERT INTO `cost_category` (`id`, `category_name`) VALUES
(1, 'Bike Oil'),
(2, 'Mobile Bill'),
(3, 'Bike Rent'),
(6, 'Stationary '),
(7, 'Salary '),
(8, 'Advance Salary'),
(9, 'Food Bill'),
(10, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(56) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `address` varchar(156) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `phone`, `address`, `created_date`, `created_by`) VALUES
(1, 'Lab Aid Hospital Ltd.', '01766661910', 'House -6, Road -4, Dhanmodi,Dhaka 1205', '2019-12-08', 9),
(2, 'Fariha Mahmud', '01711345593', 'diana tower,House 04,Block 4/8 bonoshree', '2019-12-08', 9),
(3, 'United Hospital Ltd', '01911022611', 'Plot # 15, Road # 71, Gulshan Dhaka -1212', '2019-12-08', 10),
(4, 'Kawser Ahmed', '01924800480', 'L-3,H-16,R-3,Mohakhali DOSH,Dhaka', '2019-12-12', 10),
(5, 'Sirajul Islam Hospital', '01823372076', 'Mogbazar', '2019-12-12', 10),
(6, 'Padma Hospital', '01689503335', 'Banglamotor', '2019-12-12', 10),
(7, 'Syvia Khondokar', '01718236201', 'House# 33, Mollika Housing Socity, milk vita road,Mirpur', '2019-12-14', 10),
(8, 'Naima binte islam', '01755828107', 'BIDC Bazar, DUET, Gazipur', '2019-12-14', 10),
(9, 'Nowsin Moon', '01724815327', '1/33/h,Sikder Real State,Road no 7, Talli Offce more,Zig', '2019-12-14', 10),
(10, 'Anonna', '01303567332', 'B-8,5 Siddeshwari Dhaka', '2019-12-14', 10),
(11, 'Gulshan Ara', 'B1766598438', '7225th Floor,Elenbari Road,Tejgaon ', '2019-12-14', 10),
(12, 'Abida Sultana Oyshi', '01715576692', 'Flat-4D(L-4)Hajera Green,15/ka,Zigatala', '2019-12-14', 10),
(16, 'Adhora Shatu ', '01705004597', 'Concord regency, Flat no 13/B,West Panthapath', '2019-12-14', 9),
(17, 'Junayna ', '01727024050', '971(1st floor)Rokeya Sharani Road(Opposite to Chef Resta', '2019-12-14', 9),
(18, 'Ishita Tanzum', '01818650883', '85,Central Road,Flat No-A1.Dhanmondi-Dhaka-1205', '2019-12-14', 9),
(19, 'Sadid Ahsan', '01670819145', '9/26 Rupnogor, Mirpur -2. Beside BUBT', '2019-12-15', 9),
(20, 'Saiful Islam Shahed', '01952555000', 'Shajinaz Eximpack Limited\r\nBSCIC Industrial Estate, Bloc', '2019-12-15', 9),
(21, 'XYZ', '01914842801', '134/7,Darussalam (Darussalam Tower er pashe) ', '2019-12-15', 9),
(22, 'MD.Murad', '01914575586', 'Flate-8B, House 6/2/1/2, Rabi,Hanif before Nabana Garden', '2019-12-15', 9),
(23, 'Lazz Pharma (Tajmohol)', '01707006577', 'Tajmohol road,Mohammadpur', '2019-12-15', 10),
(24, 'Nafsin Nazia', '01709001448', 'Rajshahi', '2019-12-15', 9),
(25, 'Asik Neawas Mammud', '01872772179', 'Booo TV', '2019-12-15', 9),
(26, 'Makbula Nasrin', '01868677137', '32 ENA KINGDOM KALABAGAN LAKE CIRCUS-DHANMONDI ', '2019-12-15', 9),
(27, 'Aneeka Waleed', '01789109607', 'House 46, Road 7, Block G, Banani', '2019-12-15', 9),
(28, 'Biva Zarin ', '01951669339', '445/2 West Shewrapara,Rahman Nibash ,Shapla Shoroni,Mirpur', '2019-12-17', 9),
(29, 'Farzana Sumi', '01775793167', 'CEGIS, House No-6,Road no 23/C, Gulshan-1', '2019-12-17', 9),
(30, 'Tanzia Chowdhury Shanta', '01711082205', '128 north kafrul.near kochukhet bazar.contact', '2019-12-17', 9),
(31, 'Sadia Tiva', '01623929736', 'Regal Height\r\nFlat D3\r\n1 no south kollayanpur\r\nnear kollayanpur bus stand\r\nDhaka\r\n1 no south kollayanpur\r\nnear kollayanpur bus stand\r\nDhaka', '2019-12-17', 9),
(32, 'Nilufar Islam ', '01723900986', 'Address: Nas Vila, 21/ a Azimpur Shekh Shaheb Bazar, Aamtola road', '2019-12-17', 9),
(33, 'Tashfia Afreen', '01675945900', 'House 255 rd 1 block B bashundhara r/a 8th floor', '2019-12-17', 9),
(34, 'Meher Nigar Nishi', '01779769341', 'S.M.Masud Parvaz\r\nhouse no: 413/3,\r\nLake view road\r\nSouth paikpara,,\r\nKollyanpur,,Dhaka.', '2019-12-17', 9),
(35, 'Fatima Sinthi', '01727931329', 'Uposhor 1 No Sector ,Rajshahi', '2019-12-17', 9),
(36, 'Abida Sumi (01755620280)', '01755620290', 'House 10, road 3, sector 10, Uttara', '2019-12-17', 9),
(37, 'Moon Moon', '01757294424', 'Apt-A-1,House-1,R-3.Baridhara', '2019-12-17', 9),
(38, 'Kazi Monsura', '01817291810', 'House-227/A,R-7,Mohammudi Houseing ', '2019-12-17', 9),
(39, 'Kazi Monsura', '01817291810', 'House -227/A,Road-7,Mohammadi Houseing ', '2019-12-17', 9),
(40, 'Mahdia Toma', '01818699019', '58/1 kalabagan 1st lane', '2019-12-17', 9),
(41, 'Mostarshid Billah Towfik (', '01521437179', 'Flat A7 House 9 road 3 block G Bonosree Rampura', '2019-12-17', 9),
(42, 'Mostarshid Billah Towfik ', '01718731048', 'Flat No-A7, House 9,Road 3,Block G,  Bonosree, ', '2019-12-17', 9),
(43, 'Sylvia Khondoker 017182362', '01747054532', 'Plot no 33, mollika housing society ,milk vita road, Mirpur-7', '2019-12-17', 9),
(44, 'Sylvia Khondoker ', '01718236201', 'Plot no 33, mollika housing society ,milk vita road, Mirpur-7', '2019-12-17', 9),
(45, 'Maymuna Liza', '01964844030', 'E-5/5, Civil Aviation staff quater,Kawlar, Airport,Dhaka', '2019-12-17', 9),
(46, 'Nazia Tajrian Amin', '01671093247', 'House-604,Road-08,avenue-06,mirpur DOHS', '2019-12-17', 9),
(47, 'Sadman ', '01950959534', 'Dhaka ', '2019-12-17', 9),
(48, 'Tasfina Islam', '01916038547', 'HS#7, (Lift Er 8),Block-C, Haji Dil Mohammad Avenue, Dhaka Uddan Mohammadpur', '2019-12-17', 9),
(49, 'Nowrin', '01711220012', '33/1,House # Suraya\'s Dream.Flat# 7/B,Mirpur Road,Dhanmondi,Dhaka (Priangon Shopping Center,Science Lab over Bridge)', '2019-12-17', 9),
(50, 'Setara Yeasmin Runu', '01730084051', 'Mr.Samrat, Jamuna Bank Ltd., Banani Branch, 58/E, Kemal Ataturk Avenue, Banani, Dhaka-1212', '2019-12-17', 9),
(51, 'Zahid Anam', '01711971023', 'Navana Tower, Road-4, House-11, Flat- B4, Lift-4, Monipuri Para, Farmgate, Dhaka', '2019-12-17', 9),
(52, '123', '01847163534', '554/c, chairmangoli, noyatola, Moghbazar, Dhaka 1217', '2019-12-17', 9),
(53, 'Mohona Khan Momo', '01713036372', '501,Baitul Aman Housing ,Road-13, Adabor', '2019-12-17', 9),
(54, 'Maliha Keya ', '01761944961', 'ka-132/3  Naher Monjil,Uttorpara,Khilkhat', '2019-12-17', 9),
(55, 'Snigdha Reza', '01714086337', '164 East Raja Bazar farmgate,Dhaka', '2019-12-17', 9),
(56, 'Sadid Ahsan', '01670819145', '15 New Eskation Road', '2019-12-17', 9),
(57, 'Tanni Naireen', '01711080661', 'House -24, Road-27. Sector-7. Uttara', '2019-12-17', 9),
(58, 'Nasima Nusrat', '01722739727', 'House No-29,Road No-11, Merul Badda DIT project ', '2019-12-17', 9),
(59, 'Princessm Khan', ' 0171303637', 'House-28,Road-18,B-A,Banani', '2019-12-17', 9),
(60, 'Md Arif Hossain', '01778999005', 'Jinaidah', '2019-12-17', 10),
(61, 'Nafsin Nazia', '01709001448', 'Rajshahi ', '2019-12-17', 9),
(62, 'Biva Zarin ', '01951669339', '445/2 Wast Shewrapara,Mirpur,Dhaka', '2019-12-17', 9),
(63, 'Radin Raihan', '01968878888', '164,Hitech Hospital Building(AR RAIHAN TOURS &TRAVELS),EAST KAFRUL, DHAKA CANTONMENT DHAKA 1206', '2019-12-17', 9),
(64, 'Sharmin Jahan', '01837222135', 'Soddeshwari 5no ,Flat number A7. Building name Ananna. (Siddshwari boys school er opposite a)', '2019-12-17', 9),
(65, 'Nafila Shahin ', '01700763345', 'House-12,Road-32,L-4,Gulshan-1', '2019-12-17', 9),
(66, 'Sanjidatul Islam', '01716794826', '504, SH tower,Kajir Dewri,Chittagong', '2019-12-17', 9),
(67, 'Muna Ahamed/Tanvir', '01915248283', 'Banani army officers quarter Besides Navy Head Quarter, Building no#123/2', '2019-12-17', 9),
(68, 'Faria Mahmood Diana', '01711345593', 'Tower; H#4; Block#B; Flat#4B; Banosree Main Road; Banosree Project, infront of Lake, Banosree', '2019-12-17', 9),
(69, 'Diana Ayra', '01758463486', 'Rezwana Majhir khola, Gazipur', '2019-12-17', 9),
(70, 'Umme Salma Upoma', '01744796668', 'Address-200,Amena garden,aamtola Bazar,60 feet road,Flat 6B', '2019-12-17', 9),
(71, 'Umme Salma Upoma', '01744796668', '200,Amena garden,aamtola Bazar,60 feet road,Flat 6B', '2019-12-17', 9),
(72, 'Ela Chowdhury', '01726320897', 'Ali & Noor Real State, Road. 02,House.02.Beside Of Zam Zam Store,Mohammadpur Bus Stand,Dhaka-1207', '2019-12-17', 9),
(73, 'মেসবা উল হাসান রোমান', '01616662552', ' Flat#3B, House#21,Road #2,Dhaka Housing, Adabor, Dhaka', '2019-12-17', 9),
(74, 'Sayma Rafique', '01732842388', 'Sec-6,Block-C,Road-8,House-95/96,97/98,Mirpur-10,Dhaka-1216 (10 no indoor stadium er pase xinxian restaurant ase tar paser road ta 8 number road)', '2019-12-17', 9),
(75, 'Urmi Erika ', '01625282184', 'Sec-7,House-1,Road-25, Uttara ', '2019-12-17', 9),
(76, 'Farzana Kabir', '01717550444', 'Prince Tower,lift-6.F-F/2,Road-1,Adabor', '2019-12-17', 9),
(77, 'Ferdawsy Tabassum Tamanna', '01777008457', '64/B Chayabithy Estern Housing Middle Bashabo Dhaka', '2019-12-17', 9),
(78, 'Hasna Hafiza Simi', '01926243079', 'House- 8/A/1, Road-14 (New) Zenith Tower, Apt# A-, Dhanmondi R/A, Dhaka', '2019-12-17', 9),
(79, 'MD.SABBIR AHMED', '01678648726', 'ex services Bangladesh ltd.\r\n14, (safina centre) 4th floor,gareeb E nawaz avenue,sector 11, uttara,Dhaka 1230', '2019-12-18', 9),
(80, 'Tania Enam Mishu 017187399', '01745685279', '50/6/6 West Hajipara,One Bank-Malibagh ', '2019-12-18', 9),
(81, 'Tania Enam ', '01745685279', '(01718739912)50/6/6 West Hajipara,One Bank-Malibagh ', '2019-12-18', 9),
(82, 'Madhu Mondol ', '01715355379', 'Merits office', '2019-12-18', 9),
(83, 'Nowshin Nusrat Tonmona', '01670153724', 'Flat 1b, 14th mitali housing society east kafrul Dhaka cantonment ', '2019-12-18', 9),
(84, 'Pejon Hasan', '01680281286', 'Mymensingh', '2019-12-18', 9),
(85, 'Tehjib Barat', '01715605595', 'House: 726/4, Road: 10, Adabor', '2019-12-18', 9),
(86, 'Moni Sarkar', '01711574596', 'Brown compound, Barisal sadsr. Barisal', '2019-12-18', 9),
(87, 'Motahar Hossain', '01911545414', 'Khulna', '2019-12-18', 10),
(88, 'Misty Ashrafun', '01628009928', 'H # 4, R# 4/1,Block-B,Section-12, Mirpur', '2019-12-19', 10),
(89, 'Sababa Sabeen', '01711558800', 'Momens Primrose Hall,H# 3 5,R# 14, Baridhara', '2019-12-19', 10),
(90, 'Nasrin Islam', '01770943354', 'Fokir Para, Dinajpur Sadar', '2019-12-19', 10),
(91, 'Tanni Naireen', '01711080661', 'H# 24, R# 27,Sector 7,Uttara', '2019-12-19', 10),
(92, 'Shuab Muhammad Zaber', '01711507384', 'H# 29, R# 2,Block A,Dhaka Real state,Katashur', '2019-12-19', 10),
(93, 'Ibrahim Jahangir ', '01762805245', '46/7 A vagulpor lane company ghat Hazaribag', '2019-12-19', 9),
(94, 'Nilufar Islam', '01723900986', 'Nas Vila, 21/ a Azimpur Shekh Shaheb Bazar,Aamtola road\r\n', '2019-12-19', 9),
(95, 'Shabnam Basera Rishta', '01555014213', 'Ruposhi Pro active village. Building 6,Plot 1/4- B, Road 01, Mirpur 15.', '2019-12-19', 9),
(96, 'Moon Moon ', '01757294424', 'Apt-A-1,H-1,R-3,Baridhara Diplomatic Zone,Dhaka', '2019-12-19', 9),
(97, 'Tabassum Sultana Moutushi', '01310261112', 'Mirpur- 12,Road- 20,House - 18,Block- C', '2019-12-19', 9),
(98, 'Rifat Adnan', '01717299293', 'Monsurabad Housing , Adbor', '2019-12-19', 10),
(99, 'Enamul Ahsan Sohel', '01716978444', 'R# 23,H# 1,Progress Tower,Gulshan 1', '2019-12-19', 10),
(100, 'Samira Moyeen', '01947588109', '', '2019-12-19', 10),
(101, 'Nilufar Nila', '01717339448', 'F-C3,826,East Shewrapara,Mirpur', '2019-12-19', 10),
(102, 'Syeda Tanzila Karim', '01684795338', 'B-A,R#-B,H# 2, 5th Floor, Bonosree', '2019-12-19', 10),
(103, 'Rinku Saif', '01953138187', 'Gabtoli Amin bazar, Momota Petrol Pump', '2019-12-19', 10),
(104, 'Afrin Siddique', '01871006759', '15/F,Jahanara apartment,42-43, Shiddeshweri Sarkular road', '2019-12-19', 10),
(105, 'Adnan Arif Shuvo', '01720000046', '1/A-3,Mirbagh,Hatirjhil Dhaka', '2019-12-19', 10),
(106, 'Rupa Liman', '01881172121', 'Gabtoli bus Stand', '2019-12-19', 10),
(107, 'Irin', '01980414078', 'Union Mostafa Palace, H# 1/1,R# 1, F# A4,Lalmatia', '2019-12-19', 10),
(108, 'XYZ 1', '01818293963', 'H-205, L-6,R-10, Dhanmondi,near of Naz e Noor Hospital', '2019-12-19', 10),
(109, 'Tania Islam', '01911813273', '1/C Green Valley,147/1,Panthapath,(walton Goli)', '2019-12-19', 10),
(110, 'Shahnur Mim', '01908562306', 'Raishbazar, Ali Hardwer,Under Popular Hospital', '2019-12-19', 10),
(111, 'Mohini Rahman', '01757081136', '75, Siddeshweri ', '2019-12-19', 10),
(112, 'Mahmuda Shukhi', '01731760720', 'Dr. Rony.Nabinagar,Savar', '2019-12-19', 9),
(113, 'Kawser Ahmad', '01924800480', 'Level#3, House # 166 (2nd Floor), Road # 3. Mohakhali New DOHS ', '2019-12-19', 9),
(114, 'Mehnaz Sanjana', '01724466646', 'House 207, Road- 8, Block-C, Bashundhara ', '2019-12-19', 9),
(115, 'Afrin Siddique', '01871006759', '15-F Jahanara Apartment, 42-43 Shidheshwari Circular Road, Dhaka', '2019-12-19', 9),
(116, 'Sadnan H.B', '01950959344', '803 komolpur,Bhairob', '2019-12-19', 9),
(117, 'Maksuda Begum', '01713481570', 'Brac Bank Ltd. Anik Tower 6th Floor,220/B Tejgaon I/A,Tejgaon Link Road,Dhaka', '2019-12-19', 9),
(118, 'Noor E Tamanna  kashfi', '01670917114', 'Mohua 435/7 Ghataile Cantonment, Tangail.', '2019-12-19', 9),
(119, 'Shahzia Nowrin Lira ', '01711220012', 'House-31/A Road - 5 Dhanmondi R/A Dhaka.North- West Corner of Dhanmondi 4 no. Play Ground. Appertmemt name:- Amromonjuri. 8th floor of lift.', '2019-12-21', 9),
(120, 'Nadia Sharmin Trisa ', '01752893917', 'House 726/22, road 10, baitul aman housing society, adabor ', '2019-12-21', 9),
(121, 'Sultana Razia ', '01616559593', 'House:694, ibrahimpur ashi dag, kafrul mirpur-14, Dhaka-1206 ', '2019-12-21', 9),
(122, 'Sultana Razia', '01616559593', 'House:694, ibrahimpur ashi dag, kafrul mirpur-14, Dhaka-1206 ', '2019-12-21', 9),
(123, 'Umme Fatema  ', '01784885307', 'Road:33,House:15,Rupnagar R/A Mirpur Dhaka-1216', '2019-12-21', 9),
(124, 'Zephyr Böññâ ', '01769101267', 'Maj emran Staff road 147/10, Dhaka cantonment ', '2019-12-21', 9),
(125, 'Chaldal.com', '01951166080', 'H# 05, R# 01, Sector-6, Uttara, Dhaka-1230', '2019-12-21', 10),
(126, 'Karishma Hossain', '01955282417', 'C/O, S.M.Shoaib Hossain\r\nMahanagar project,Rampura\r\nRoad no : 2Block n : C,House no : 13', '2019-12-21', 9),
(127, 'S.m. Ashrafuzzaman Shojib', '01712977179', 'House#9,Block #B,Road # 7,West dhanmondi housing,Bossila,Moha­mmadpur', '2019-12-21', 9),
(128, 'Sadia Al Kadri', '01789595880', 'Shanti Niketon, 29/2-3, Block-F, Babor Road,Mohammadpur, Dhaka.', '2019-12-21', 9),
(129, 'ABC', '01784885307', 'Road:33,House:15,Rupnagar R/A Mirpur Dhaka-1216', '2019-12-21', 9),
(130, 'Rifat Al Muktadir ', '01717376664', 'House:138,Road:02,Block:E,\r\nSection:11,Kalshi,Pallabi', '2019-12-21', 9),
(131, 'sharmin linta', '01703991141', '125/3,banani officers quarter, beside navy hq, banani, Dhaka', '2019-12-21', 9),
(132, 'ABC-1', '01711082205', '128 Noth kafrul,Near kochukhat', '2019-12-21', 9),
(133, 'Farhana Faisal Tumpa', '01673017001', '37, palash nagar, lalmati, mirpur-11. Dhaka', '2019-12-21', 9),
(134, 'Radin Raihan', '01968878888', '164,Hitech Hospital Building(AR RAIHAN TOURS &TRAVELS),EAST KAFRUL, DHAKA CANTONMENT DHAKA 1206', '2019-12-21', 9),
(135, 'Minar Mustafee', '01775540944', 'Dr.Amanullah Bin Siddik\r\nFlat:1504;Building:20.\r\n Japan Garden City\r\nMohammadpur,Dhaka.', '2019-12-21', 9),
(136, 'Sanjida Nisat', '01789394930', 'Niribili Bhabon 40, 6th floor\r\nNew palton, azimpur', '2019-12-21', 9),
(137, 'Mum & Little One', '01707081295', 'Shongkor, Dhanmondi-1209', '2019-12-21', 10),
(138, 'Mahfuza Mili', '01712184496', 'Salim Mahmud,MARS Solutions Ltd. BDBL Bahban(Level-15),12 kawran Bazar', '2019-12-21', 9),
(139, 'Mum & Little Ones (Uttara)', '01707081293', 'Sec-14, Uttara', '2019-12-21', 10),
(140, 'Mahfuza Mili', '01948897558', '01712184496 -Salim Mahmud,MARS Solutions Ltd. BDBL Bahban(Level-15),12 kawran Bazar', '2019-12-21', 9),
(141, ' Md Fazle Rabbi', '01742429495', 'Punam Cinema Hall\r\nRayerbag Road, Dhaka', '2019-12-21', 9),
(142, 'Nusrat Jahan Tonwi', '01814448829', 'Nusrat 01814448829, Apt A5, House 1/8, Block C, Lalmatia', '2019-12-21', 9),
(143, 'Taskia Sumi', '01940648346', 'Holding :25/2, Block # F, Vogra.(East Side of the Over Bridge,Maleker Bari Bazar) National University, Gazipur (SA poribohon)', '2019-12-21', 9),
(144, 'Abu Sayeed Shamim', '01611258999', '7/B , South Mugdapara Wapdagoli', '2019-12-21', 9),
(145, 'Sharmin Linta', '01703991141', '125/3,banani officers quarter, beside navy hq, banani, dhaka', '2019-12-22', 9),
(146, 'Saam Binte Sabbir', '01734682225', 'Uday Tower, Gulshan-1', '2019-12-22', 9),
(147, 'নূরেজান্নাত সিলভীয়া ', '01922966988', '????????, \r\n???????? ????????????? ??????? \r\n???? ???? ???, ???????? ???,???, ??????', '2019-12-22', 9),
(148, 'সিলভীয়া নূরেজান্নাত', '01922966988', 'Bogra ', '2019-12-22', 9),
(149, 'সিলভীয়া নূরেজান্নাত', '01922966988', 'Bogura Sador', '2019-12-22', 9),
(150, 'Fille De Lombre', '01683391661', '10/3, free school street, kathalbagan (box culvert road)Landmark: lane between hsbc and bangla vision', '2019-12-22', 9),
(151, 'Israt ', '01769119671', 'Bangladesh airforce,Dhaka Cantonment ', '2019-12-22', 9),
(152, 'Erfan Hossain', '01769999659', 'BAF Tejgaon Officer\'s Mess,Dhaka', '2019-12-22', 9),
(153, 'Irin', '01980414078', '93/2 B Monesshore Road,Jigatola,Dhaka', '2019-12-22', 9),
(154, 'Tasfina Islam', '01916038547', 'HS#7, (Lift Er 8),Block-C, Haji Dil Mohammad Avenue, Dhaka Uddan Mohammadpur', '2019-12-22', 9),
(155, 'Zaber IT, Evaly', '01722566555', '01610160201', '2019-12-22', 9),
(156, 'Archi Azad', '01722094357', 'E-68,Professor Para, Sherpur ', '2019-12-22', 9),
(157, 'Sayma Rafique', '01732842388', 'Sec 6,block c, road 8,house 95/96,97/98,Mirpur,Dhaka-1216', '2019-12-22', 9),
(158, 'Nahar Dina', '01689930619', '105 no.nasir uddin sarder lane,koltabazar,Puran Dhaka.', '2019-12-22', 9),
(159, 'Zihan Nur', '01747570647', 'House-35/5,GP CHA,TB Gate,MOhakhali', '2019-12-22', 9),
(160, 'Mothers & Kids World', '01942520741', 'Sector -7, Uttara', '2019-12-22', 9),
(161, 'Dr. Farhana', '01716691369', '01793057424-4C, 170/2 West Agargaon,Vuiya Goli (Shamoli-2)', '2019-12-22', 9),
(162, 'Zannatu Adneen', '01674985873', 'Dr.mou\r\nFlat-4b,3rd building,ka-112/30,sathi nibash(somitir bari), south Badda,Dhaka', '2019-12-23', 9),
(163, 'ToNny TaNia', '01715839633', 'Md Atikul Islam\r\nAdditional SP Sadar circle office,\r\nPolice line,kushtia.', '2019-12-23', 9),
(164, 'Afsana Shanta', '01947550318', '92 arambagh, flat 10/c,Motijheel', '2019-12-23', 9),
(165, 'Aneeka Waleed', '01789109607', 'house 46, road 7, block G, Banani', '2019-12-23', 9),
(166, 'Nilufar Nila', '01717339448', 'Flat- C3, 826,east shewrapara, Mirpur.', '2019-12-23', 9),
(167, 'Miftahul Amin', '01787667615', 'Police plaza Concord Tower-1,13th Floor,Plot-02,Road-144,Gulshan-1', '2019-12-23', 9),
(168, 'Anonya Chowdhury', '01817088751', 'House: 182/B, Flat 4B, Boro moghbazar, Doctor Goli', '2019-12-23', 9),
(169, 'Muslima Akther', '01971143144', '304/1 C Ahammednagor ,paikpara, Mirpur-1,Dhaka', '2019-12-23', 9),
(170, 'Muslima Akther', '01971143144', 'Road:6 Avenue:6 House:446,Mirpur DOHS', '2019-12-23', 9),
(171, 'সাঈদ খান শাওন', '01717585197', 'mirpur 1 \r\nMidile Pikpara \r\npuaran KAzi office road \r\n73 d Lovely poient carnival 2A flat', '2019-12-24', 9),
(172, 'আইভি রহমান ডোরা', '01749129343', 'Flat 5B, 167/1/A Niketan bazar,Tejgaon, Dhaka', '2019-12-24', 9),
(173, 'Karishma Hossain', '01715094876', 'Flat No-A-1(1st Floor) House-38B/1,Road-10A,Dhanmondi Beside Junior Laboratory School)', '2019-12-24', 9),
(174, 'Dina Tahmina', '01710402137', ' S@ifur\'s tangail Branch, Nafi complex(1st floor), Old Bus Stand Road, Tangail sadar, Tangail.', '2019-12-24', 9),
(175, 'Faria Mahmood,Diana', '01711345593', 'Tower; H#4; Block#B; Flat#4B; Banosree Main Road; Banosree Project, infront of Lake, Banosree', '2019-12-24', 9),
(176, 'Farzana Suma', '01729646860', 'Bashundhara Abshik, G block, Road 11, House 1/A, 3rd floor ', '2019-12-24', 9),
(177, 'Musfiq Zaman', '01819535217', 'Hazaribag, Zigatola', '2019-12-24', 9),
(178, 'Lima Mahmud', '01757864337', 'House no 79, Shakertek, Mohammadpur', '2019-12-24', 9),
(179, 'Fatiha Arzumand', '01672727002', 'Capital Mother View, House- 502/1, 503 North Ibrahimpur, Mirpur-14 (Near Police Staff College)', '2019-12-24', 9),
(180, 'Ummey Saba', '01675505417', 'md. Robin.\r\nKishorgonj sodor, Rothkhola.koshorgonj', '2019-12-26', 9),
(181, 'Dr. Shuvo', '01843896533', 'House 12, 2nd Floor, Holy Family Doctors Quarter, Dhaka', '2019-12-26', 9),
(182, 'Lutfur Nahar ', '01798353093', '26/2, Principal Abul Kashem Road, Mirpur-1, Dhaka-1216. ', '2019-12-26', 9),
(183, 'Sharnali Islam', '01717233455', '31/1 chamelibug\r\nShantinagar , 3 tala\r\nShantinagar Mor theke agale left er 1st goli. Moshjid er goli', '2019-12-26', 9),
(184, 'Mahamuda Sarawar', '01819444378', 'AGM,\r\nICT Department,\r\nBangladesh Krishi Bank, (head office),83-85 motijeel C/A. Dhaka', '2019-12-26', 9),
(185, 'Afia Iffat', '01717936521', '166,elite castle\r\nkathal tola mor,Tejkuni Para, Tejgoan', '2019-12-26', 9),
(186, 'Anita Sarker Anonna', '01303567332', '01721101438  B-8,5 Siddeswari,Dhaka ', '2019-12-26', 9),
(187, 'Pan Care Hospital', '01718806947', 'Dhanmondi 27', '2019-12-26', 10),
(188, 'Uma Rani Dash', '01742124460', 'House no 240,  flat ni. 6A, west dhanmondi', '2019-12-26', 9),
(189, 'Rafat Adnan', '01717299293', 'Sundarban Courier, Agrabad Branch Chittagong', '2019-12-26', 9),
(190, 'Ishan ', '01721450545', '31 Shiris Das Lane,10 Tola Building,Pyaridas Road,Banglabazar(Near Beauty Boarding)', '2019-12-28', 9),
(191, 'Nazifa Z Hussain', '01680734137', 'Apt 5A, House 3 , Road 6 , Gulshan-1', '2019-12-28', 9),
(192, 'Tarannum Liza', '01906309064', '\"Tarannum villa\" ,  section- 12, block- D, road- 10, house- 18, pallabi Mirpur, Dhaka -1216.', '2019-12-28', 9),
(193, 'Sadia Tasneem Sharma ', '01847163534', 'House-7,Road-9,Block-A,Mirpur-12', '2019-12-28', 9),
(194, 'Mishela Khan Urmi', '01774334451', '01773334451 Bhuapur tangail', '2019-12-28', 9),
(195, 'Mahdia Toma', ' 0181869901', '58/1 kalabagan 1st Lane', '2019-12-28', 9),
(196, 'Tasnim Maruf', '01673300563', 'Mirpur 1 road 5 house 2 block C', '2019-12-28', 9),
(197, 'Humayon Ahmed', '01911743483', 'Bank Town Savar.', '2019-12-28', 9),
(198, 'Nuri Pathor', '01750699756', '.flat no.2/402.Estern Castle.(opposite of segun bagicha high school) Segun Bagicha Dhaka', '2019-12-28', 9),
(199, 'Lazz Pharma (Gulshan)', '01309265608', 'Gulshan 1', '2019-12-28', 10),
(200, 'Lazz Pharma (Shaymoli)', '01611066007', 'Shaymoli', '2019-12-28', 10),
(201, 'Badhon Sarkar Mumu', '01792611692', '91 No Tatibazar,4th floor,Old Dhaka ', '2019-12-28', 9),
(202, 'MURAD', '01914575586', '(01672708210)11/2 West & Street.Central Road,Dhanmondi,Dhaka', '2019-12-28', 9),
(203, 'MURAD', '01914575586', 'Flate-8B, House 6/2/1/2, Rabi,Hanif before Nabana Garden, kallaynpur(01672708210)', '2019-12-28', 9),
(204, 'Zahidul Islam', '01711992031', 'Labonno Neer- 3rd Floor,147/10/2A ,Lak view R/A, South Pirer Bagh-Mirpur', '2019-12-28', 9),
(205, 'Badas Pharmacy', '01642581654', 'Shegunbagicha', '2019-12-28', 10),
(206, 'Raisul Islam (MD,Kidz)', '01819626030', 'Little Feat,House -5,Road-8,Nikunja,Dhaka', '2019-12-28', 9),
(207, 'Rahnumanuraien Ovi', '01732700431', 'Trishal, Mymensingh ', '2019-12-28', 9),
(208, 'Nusrat Binte Islam', '01724998300', ' Sha-97/A, Officer\'s Tower, Uttar Badda, Dhaka-1212.', '2019-12-29', 9),
(209, 'Mr.Sakib ', '01673112224', '271/B,Anamika Khondoker Villa,Flat 7B, Khilgaon ', '2019-12-29', 9),
(210, 'BANGLADESH SPECIALIZED HOS', '01717702697', '21 SHYAMOLI, DHAKA 1207', '2019-12-29', 10),
(211, 'Rahas E Matanna', '01680076907', 'Flat A6,\r\nHouse 170/A,Delicia Apartment(Behind comfort Hospital)Green Road,Dhaka', '2019-12-29', 9);

-- --------------------------------------------------------

--
-- Table structure for table `disburse_type`
--

CREATE TABLE `disburse_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(26) DEFAULT NULL,
  `discount_upto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disburse_type`
--

INSERT INTO `disburse_type` (`id`, `type_name`, `discount_upto`) VALUES
(1, 'Shop', 10),
(2, 'Super Shop', 25),
(3, 'Home Delivery', 15),
(4, 'Hospital', 10),
(5, 'Dealer Point', 10);

-- --------------------------------------------------------

--
-- Table structure for table `district_govt`
--

CREATE TABLE `district_govt` (
  `id` int(10) UNSIGNED NOT NULL,
  `districtname` varchar(56) NOT NULL,
  `divisionid` int(10) UNSIGNED NOT NULL,
  `govt_district_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `district_govt`
--

INSERT INTO `district_govt` (`id`, `districtname`, `divisionid`, `govt_district_id`) VALUES
(1, 'Bagerhat', 4, NULL),
(2, 'Bandarban', 2, NULL),
(3, 'Barguna', 1, NULL),
(4, 'Barisal', 1, NULL),
(5, 'Bhola', 1, NULL),
(6, 'Bogra', 6, NULL),
(7, 'Brahmanbaria', 2, NULL),
(8, 'Chandpur', 2, NULL),
(9, 'Chapai Nababganj', 6, NULL),
(10, 'Chittagong', 2, NULL),
(11, 'Chuadanga', 4, NULL),
(12, 'Comilla', 2, NULL),
(13, 'Cox\'s Bazar', 2, NULL),
(14, 'Dhaka', 3, NULL),
(15, 'Dinajpur', 7, NULL),
(16, 'Faridpur', 3, NULL),
(17, 'Feni', 2, NULL),
(18, 'Gaibandha', 7, NULL),
(19, 'Gazipur', 3, NULL),
(20, 'Gopalganj', 3, NULL),
(21, 'Habiganj', 8, NULL),
(22, 'Jamalpur', 5, NULL),
(23, 'Jessore', 4, NULL),
(24, 'Jhalokati', 1, NULL),
(25, 'Jhenaidah', 4, NULL),
(26, 'Joypurhat', 6, NULL),
(27, 'Khagrachhari', 2, NULL),
(28, 'Khulna', 4, NULL),
(29, 'Kishoregonj', 3, NULL),
(30, 'Kurigram', 6, NULL),
(31, 'Kushtia', 4, NULL),
(32, 'Lakshmipur', 2, NULL),
(33, 'Lalmonirhat', 7, NULL),
(34, 'Madaripur', 3, NULL),
(35, 'Magura', 4, NULL),
(36, 'Manikganj', 3, NULL),
(37, 'Maulvibazar', 8, NULL),
(38, 'Meherpur', 4, NULL),
(39, 'Munshiganj', 3, NULL),
(40, 'Mymensingh', 5, NULL),
(41, 'Naogaon', 6, NULL),
(42, 'Narail', 4, NULL),
(43, 'Narayanganj', 3, NULL),
(44, 'Narsingdi', 3, NULL),
(45, 'Natore', 6, NULL),
(46, 'Netrakona', 5, NULL),
(47, 'Nilphamari', 7, NULL),
(48, 'Noakhali', 2, NULL),
(49, 'Pabna', 6, NULL),
(50, 'Panchagarh', 7, NULL),
(51, 'Patuakhali', 1, NULL),
(52, 'Pirojpur', 1, NULL),
(53, 'Rajbari', 3, NULL),
(54, 'Rajshahi', 6, NULL),
(55, 'Rangamati', 2, NULL),
(56, 'Rangpur', 7, NULL),
(57, 'Satkhira', 4, NULL),
(58, 'Shariatpur', 3, NULL),
(59, 'Sherpur', 5, NULL),
(60, 'Sirajganj', 6, NULL),
(61, 'Sunamganj', 8, NULL),
(62, 'Sylhet', 8, NULL),
(63, 'Tangail', 3, NULL),
(64, 'Thakurgaon', 7, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `division_zone`
--

CREATE TABLE `division_zone` (
  `id` int(10) NOT NULL,
  `divisionname` varchar(50) NOT NULL,
  `govt_division_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division_zone`
--

INSERT INTO `division_zone` (`id`, `divisionname`, `govt_division_id`) VALUES
(1, 'Barisal', ''),
(2, 'Chittagong', ''),
(3, 'Dhaka', ''),
(4, 'Khulna', ''),
(5, 'Mymensingh', ''),
(6, 'Rajshahi', ''),
(7, 'Rangpur', ''),
(8, 'Sylhet', '');

-- --------------------------------------------------------

--
-- Table structure for table `ledgerposting`
--

CREATE TABLE `ledgerposting` (
  `ledgerPostingId` int(50) NOT NULL,
  `voucherNumber` varchar(50) DEFAULT NULL,
  `ledgerId_dealer` int(11) DEFAULT NULL,
  `ledgerId_customer` int(11) DEFAULT NULL,
  `debit` decimal(18,2) DEFAULT NULL,
  `credit` decimal(18,2) DEFAULT NULL,
  `payment_type` smallint(4) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ledgerposting`
--

INSERT INTO `ledgerposting` (`ledgerPostingId`, `voucherNumber`, `ledgerId_dealer`, `ledgerId_customer`, `debit`, `credit`, `payment_type`, `date`) VALUES
(1, '2547830', 0, 1, NULL, 25500.00, NULL, '2019-12-08'),
(2, '0627189', 0, 3, NULL, 48270.00, NULL, '2019-12-08'),
(3, '1293804', 14, 0, NULL, 968.00, NULL, '2019-12-09'),
(4, '486290', 16, 0, NULL, 1168.00, NULL, '2019-12-11'),
(5, '396025', 17, 0, NULL, 151200.00, NULL, '2019-12-11'),
(6, '625704', 17, 0, NULL, 315680.00, NULL, '2019-12-03'),
(7, '692570', 17, 0, NULL, 30600.00, NULL, '2019-12-04'),
(8, '702516', 16, 0, NULL, 944.00, NULL, '2019-12-03'),
(9, '704681', 14, 0, NULL, 4768.00, NULL, '2019-12-05'),
(10, '706895', 11, 0, NULL, 4560.00, NULL, '2019-12-05'),
(11, '937612', 19, 0, NULL, 560.00, NULL, '2019-12-03'),
(12, '410975', 20, 0, NULL, -29332.00, NULL, '2019-12-03'),
(13, '418072', 20, 0, NULL, 8.00, NULL, '2019-12-10'),
(14, '960578', 11, 0, NULL, 2784.00, NULL, '2019-12-12'),
(15, '428756', 19, 0, NULL, 1408.00, NULL, '2019-12-12'),
(16, '652831', 14, 0, NULL, 151200.00, NULL, '2019-12-12'),
(17, '063524', 0, 6, NULL, 6240.00, NULL, '2019-12-12'),
(18, '562083', 0, 5, NULL, 3120.00, NULL, '2019-12-12'),
(19, '794582', 21, 0, NULL, 2164.00, NULL, '2019-12-12'),
(20, '259364', 0, 7, NULL, 1700.00, NULL, '2019-12-14'),
(21, '804573', 0, 8, NULL, 695.00, NULL, '2019-12-14'),
(22, '650293', 0, 9, NULL, 1700.00, NULL, '2019-12-14'),
(23, '610439', 0, 10, NULL, 1190.00, NULL, '2019-12-14'),
(24, '528931', 0, 11, NULL, 2975.00, NULL, '2019-12-14'),
(25, '753094', 0, 12, NULL, 0.00, NULL, '2019-12-14'),
(26, '764530', 0, 16, NULL, 1955.00, NULL, '2019-12-14'),
(27, '418705', 0, 17, NULL, 3400.00, NULL, '2019-12-14'),
(28, '436095', 0, 18, NULL, 791.00, NULL, '2019-12-14'),
(29, '690381', 0, 19, NULL, 1955.00, NULL, '2019-12-15'),
(30, '328516', 0, 20, NULL, 2105.00, NULL, '2019-12-15'),
(31, '871356', 0, 21, NULL, 1955.00, NULL, '2019-12-15'),
(32, '240153', 0, 22, NULL, 1955.00, NULL, '2019-12-15'),
(33, '863450', 0, 24, NULL, 1800.00, NULL, '2019-12-15'),
(34, '106725', 0, 23, NULL, 26664.00, NULL, '2019-12-15'),
(35, '720364', 0, 23, NULL, 27600.00, NULL, '2019-12-15'),
(36, '573240', 0, 25, NULL, 1515.00, NULL, '2019-12-15'),
(37, '802915', 0, 26, NULL, 1955.00, NULL, '2019-12-15'),
(38, '945862', 0, 30, NULL, 442.00, NULL, '2019-12-16'),
(40, NULL, 17, NULL, 500000.00, NULL, 2, '2019-12-17'),
(41, '975682', 0, 28, NULL, 1700.00, NULL, '2019-12-17'),
(42, '958210', 0, 29, NULL, 1955.00, NULL, '2019-12-17'),
(43, '403957', 0, 31, NULL, 11730.00, NULL, '2019-12-17'),
(44, '631927', 0, 32, NULL, 531.00, NULL, '2019-12-17'),
(45, '453021', 0, 33, NULL, 791.00, NULL, '2019-12-17'),
(46, '407589', 0, 34, NULL, 1896.00, NULL, '2019-12-17'),
(47, '706295', 0, 35, NULL, 2105.00, NULL, '2019-12-17'),
(48, '198723', 0, 36, NULL, 1955.00, NULL, '2019-12-17'),
(49, '957320', 20, 0, NULL, 192.00, NULL, '2019-12-17'),
(50, '053698', 11, 0, NULL, -680.00, NULL, '2019-12-17'),
(51, '796425', 0, 59, NULL, 1110.00, NULL, '2019-12-03'),
(52, '923150', 0, 61, NULL, 1210.00, NULL, '2019-12-03'),
(53, '750641', 29, 0, NULL, 29130.00, NULL, '2019-12-17'),
(54, '137824', 0, 66, NULL, 988.00, NULL, '2019-12-04'),
(55, '278309', 0, 69, NULL, 1800.00, NULL, '2019-12-05'),
(56, '748310', 0, 51, NULL, 3910.00, NULL, '2019-12-18'),
(57, '965720', 0, 78, NULL, 2295.00, NULL, '2019-12-18'),
(58, '021539', 0, 79, NULL, 1955.00, NULL, '2019-12-18'),
(59, '917523', 0, 81, NULL, 1955.00, NULL, '2019-12-18'),
(60, '104956', 14, 0, NULL, 67920.00, NULL, '2019-12-18'),
(61, '954362', 14, 0, NULL, 65484.00, NULL, '2019-12-18'),
(62, '859071', 0, 83, NULL, 1955.00, NULL, '2019-12-18'),
(63, '035964', 17, 0, NULL, 335340.00, NULL, '2019-12-18'),
(64, '764859', 0, 84, NULL, 2105.00, NULL, '2019-12-18'),
(65, '912387', 0, 85, NULL, 161.00, NULL, '2019-12-18'),
(66, '914758', 0, 86, NULL, 4065.00, NULL, '2019-12-18'),
(67, '258037', 30, 0, NULL, 64108.00, NULL, '2019-12-18'),
(68, NULL, 14, NULL, 150000.00, NULL, 2, '2019-12-12'),
(69, NULL, 14, NULL, 65000.00, NULL, 2, '2019-12-18'),
(70, '419237', 0, 88, NULL, 1700.00, NULL, '2019-12-19'),
(71, '069153', 0, 89, NULL, 638.00, NULL, '2019-12-19'),
(72, '418267', 0, 90, NULL, 2105.00, NULL, '2019-12-19'),
(73, '075124', 0, 57, NULL, 3910.00, NULL, '2019-12-19'),
(74, '742018', 0, 92, NULL, 3400.00, NULL, '2019-12-19'),
(75, '927154', 0, 93, NULL, 1275.00, NULL, '2019-12-19'),
(76, '473109', 0, 32, NULL, 1955.00, NULL, '2019-12-19'),
(77, '960278', 0, 95, NULL, 730.00, NULL, '2019-12-19'),
(78, '806713', 0, 96, NULL, 1955.00, NULL, '2019-12-19'),
(79, '593402', 0, 97, NULL, 1955.00, NULL, '2019-12-19'),
(80, '182695', 0, 98, NULL, 1955.00, NULL, '2019-12-08'),
(81, '150437', 0, 112, NULL, 2055.00, NULL, '2019-12-11'),
(82, '741602', 0, 116, NULL, 3885.00, NULL, '2019-12-12'),
(83, '867095', 0, 118, NULL, 2055.00, NULL, '2019-12-14'),
(84, '960145', 0, 119, NULL, 4184.00, NULL, '2019-12-21'),
(85, '815904', 0, 120, NULL, 3910.00, NULL, '2019-12-21'),
(86, '529160', 0, 121, NULL, 2066.00, NULL, '2019-12-21'),
(87, '279864', 0, 123, NULL, 730.00, NULL, '2019-12-21'),
(88, '527648', 0, 124, NULL, 1190.00, NULL, '2019-12-21'),
(89, '960412', 0, 125, NULL, 152800.00, NULL, '2019-12-21'),
(90, '097583', 0, 126, NULL, 1955.00, NULL, '2019-12-21'),
(91, '376580', 0, 127, NULL, 1700.00, NULL, '2019-12-21'),
(92, '509413', 0, 128, NULL, 1700.00, NULL, '2019-12-21'),
(94, '105387', 0, 130, NULL, 1700.00, NULL, '2019-12-21'),
(95, '564231', 0, 131, NULL, 1955.00, NULL, '2019-12-21'),
(96, '915407', 0, 132, NULL, 987.00, NULL, '2019-12-21'),
(97, '901852', 0, 109, NULL, 1955.00, NULL, '2019-12-21'),
(98, '976318', 0, 133, NULL, 1700.00, NULL, '2019-12-21'),
(99, '038274', 0, 63, NULL, 1955.00, NULL, '2019-12-21'),
(100, '953824', 0, 135, NULL, 5865.00, NULL, '2019-12-21'),
(101, '198075', 16, 0, NULL, -364.00, NULL, '2019-12-21'),
(102, '734085', 0, 136, NULL, 2151.00, NULL, '2019-12-21'),
(103, '589326', 0, 137, NULL, 70368.00, NULL, '2019-12-21'),
(105, '206758', 0, 138, NULL, 2635.00, NULL, '2019-12-21'),
(106, '687429', 0, 141, NULL, 1360.00, NULL, '2019-12-21'),
(107, '826540', 0, 139, NULL, 68292.00, NULL, '2019-12-21'),
(108, '624315', 0, 142, NULL, 3400.00, NULL, '2019-12-21'),
(109, '058473', 0, 143, NULL, 3955.00, NULL, '2019-12-21'),
(110, '592438', 0, 144, NULL, 1955.00, NULL, '2019-12-21'),
(111, '586127', 0, 131, NULL, 1955.00, NULL, '2019-12-22'),
(112, '403587', 0, 146, NULL, 3400.00, NULL, '2019-12-22'),
(114, '679538', 20, 0, NULL, 23904.00, NULL, '2019-12-22'),
(116, '803572', 0, 148, NULL, 610.00, NULL, '2019-12-22'),
(117, '483569', 0, 150, NULL, 645.00, NULL, '2019-12-22'),
(118, '650982', 0, 151, NULL, 1190.00, NULL, '2019-12-22'),
(119, '927608', 19, 0, NULL, 100620.00, NULL, '2019-12-22'),
(120, '234589', 0, 152, NULL, 1955.00, NULL, '2019-12-22'),
(122, '392178', 0, 153, NULL, 1955.00, NULL, '2019-12-22'),
(123, '589324', 0, 48, NULL, 1700.00, NULL, '2019-12-22'),
(124, '503241', 0, 155, NULL, 2208.00, NULL, '2019-12-22'),
(125, '396284', 0, 156, NULL, 935.00, NULL, '2019-12-22'),
(126, '310467', 0, 74, NULL, 1700.00, NULL, '2019-12-22'),
(127, '408296', 0, 158, NULL, 169.00, NULL, '2019-12-22'),
(129, '145092', 0, 160, NULL, 25788.00, NULL, '2019-12-22'),
(130, '407869', 0, 6, NULL, 23040.00, NULL, '2019-12-22'),
(132, '512869', 0, 161, NULL, 1955.00, NULL, '2019-12-23'),
(133, '703218', 0, 162, NULL, 1700.00, NULL, '2019-12-23'),
(134, '702958', 0, 163, NULL, 2046.00, NULL, '2019-12-23'),
(135, '214879', 11, 0, NULL, 173160.00, NULL, '2019-12-23'),
(136, '712035', 0, 164, NULL, 645.00, NULL, '2019-12-23'),
(137, '452701', 0, 27, NULL, 835.00, NULL, '2019-12-23'),
(139, '605271', 0, 167, NULL, 1700.00, NULL, '2019-12-23'),
(140, '849762', 0, 159, NULL, 2151.00, NULL, '2019-12-23'),
(141, NULL, 0, NULL, 100000.00, NULL, 2, '2019-12-22'),
(142, NULL, 19, NULL, 100000.00, NULL, 2, '2019-12-22'),
(143, '948130', 0, 64, NULL, 1955.00, NULL, '2019-12-23'),
(144, '941803', 0, 168, NULL, 1955.00, NULL, '2019-12-23'),
(145, '745928', 0, 169, NULL, 1955.00, NULL, '2019-12-24'),
(146, '296417', 0, 3, NULL, 12358.00, NULL, '2019-12-01'),
(147, '456291', 0, 101, NULL, 562.00, NULL, '2019-12-24'),
(148, '370285', 0, 171, NULL, 1940.00, NULL, '2019-12-24'),
(150, '793840', 0, 43, NULL, 3400.00, NULL, '2019-12-24'),
(151, '025478', 0, 173, NULL, 1955.00, NULL, '2019-12-24'),
(152, '542961', 0, 99, NULL, 1700.00, NULL, '2019-12-24'),
(153, '193604', 0, 23, NULL, 46239.00, NULL, '2019-12-24'),
(154, '209156', 0, 174, NULL, 745.00, NULL, '2019-12-24'),
(155, '043869', 0, 67, NULL, 730.00, NULL, '2019-12-24'),
(156, '679324', 0, 172, NULL, 1275.00, NULL, '2019-12-24'),
(157, '596142', 0, 117, NULL, 32.00, NULL, '2019-12-05'),
(158, '713429', 0, 178, NULL, 1700.00, NULL, '2019-12-05'),
(159, '804752', 0, 93, NULL, 1700.00, NULL, '2019-12-26'),
(160, '512468', 0, 180, NULL, 1715.00, NULL, '2019-12-26'),
(161, '596314', 0, 123, NULL, 1360.00, NULL, '2019-12-26'),
(162, '960825', 0, 181, NULL, 1700.00, NULL, '2019-12-26'),
(163, '613579', 0, 182, NULL, 1700.00, NULL, '2019-12-26'),
(164, '946580', 0, 183, NULL, 4327.00, NULL, '2019-12-26'),
(165, '591302', 0, 184, NULL, 203.00, NULL, '2019-12-26'),
(166, '154980', 0, 185, NULL, 645.00, NULL, '2019-12-26'),
(167, '906428', 14, 0, NULL, 97416.00, NULL, '2019-12-26'),
(168, '372806', 11, 0, NULL, 185760.00, NULL, '2019-12-26'),
(169, '073268', 0, 186, NULL, 1190.00, NULL, '2019-12-26'),
(170, '083541', 0, 179, NULL, 1955.00, NULL, '2019-12-26'),
(171, '146257', 0, 187, NULL, 3600.00, NULL, '2019-12-26'),
(172, '924610', 0, 188, NULL, 1955.00, NULL, '2019-12-26'),
(173, '623579', 0, 189, NULL, 1970.00, NULL, '2019-12-26'),
(174, '216430', 0, 190, NULL, 1955.00, NULL, '2019-12-28'),
(175, '408235', 0, 191, NULL, 834.00, NULL, '2019-12-28'),
(176, '903276', 0, 125, NULL, 113200.00, NULL, '2019-12-28'),
(177, '347102', 0, 192, NULL, 374.00, NULL, '2019-12-28'),
(178, '635280', 0, 193, NULL, 1700.00, NULL, '2019-12-28'),
(179, '732456', 0, 194, NULL, 695.00, NULL, '2019-12-28'),
(180, '051967', 0, 195, NULL, 1700.00, NULL, '2019-12-28'),
(181, '409675', 0, 196, NULL, 1700.00, NULL, '2019-12-28'),
(182, '402158', 0, 197, NULL, 1970.00, NULL, '2019-12-28'),
(183, '165379', 0, 198, NULL, 730.00, NULL, '2019-12-28'),
(184, '235496', 0, 36, NULL, 1955.00, NULL, '2019-12-28'),
(185, '246509', 0, 4, NULL, 1998.00, NULL, '2019-12-28'),
(186, '540861', 0, 94, NULL, 1955.00, NULL, '2019-12-29'),
(187, '089435', 16, 0, NULL, 468.00, NULL, '2019-12-28'),
(188, '820964', 0, 199, NULL, 5040.00, NULL, '2019-12-28'),
(189, '813092', 0, 200, NULL, 29340.00, NULL, '2019-12-28'),
(190, '325168', 0, 200, NULL, 24525.00, NULL, '2019-12-28'),
(191, '362874', 0, 200, NULL, 25875.00, NULL, '2019-12-28'),
(192, '850694', 0, 201, NULL, 374.00, NULL, '2019-12-29'),
(193, '869715', 0, 203, NULL, 1955.00, NULL, '2019-12-28'),
(194, '193607', 0, 204, NULL, 1955.00, NULL, '2019-12-29'),
(195, '534618', 0, 207, NULL, 2105.00, NULL, '2019-12-29'),
(196, NULL, 14, NULL, 67920.00, NULL, 1, '2019-12-18'),
(197, NULL, 14, NULL, 100000.00, NULL, 2, '2019-12-26'),
(198, NULL, 11, NULL, 180000.00, NULL, 2, '2019-12-26'),
(199, NULL, 11, NULL, 175000.00, NULL, 2, '2019-12-23'),
(200, NULL, NULL, 5, 3120.00, NULL, 1, '2019-12-12'),
(201, NULL, NULL, 6, 6240.00, NULL, 1, '2019-12-12'),
(202, NULL, NULL, 23, 26664.00, NULL, 1, '2019-12-28'),
(203, NULL, NULL, 23, 27600.00, NULL, 1, '2019-12-21'),
(204, NULL, NULL, 187, 2500.00, NULL, 1, '2019-12-28'),
(205, '706142', 0, 208, NULL, 1700.00, NULL, '2019-12-29'),
(206, '579128', 0, 209, NULL, 1700.00, NULL, '2019-12-29'),
(207, '396542', 0, 210, NULL, 7500.00, NULL, '2019-12-29'),
(208, '270356', 0, 176, NULL, 3988.00, NULL, '2019-12-30'),
(210, '526810', 0, 25, NULL, 1530.00, NULL, '2019-12-30'),
(211, '394751', 0, 211, NULL, 3160.00, NULL, '2019-12-30');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(16) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `message` varchar(156) DEFAULT NULL,
  `date` date NOT NULL,
  `status` smallint(2) DEFAULT 0,
  `replied_text` varchar(256) DEFAULT NULL,
  `replied_by` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `ordermaster_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ordermaster`
--

CREATE TABLE `ordermaster` (
  `id` int(11) NOT NULL,
  `dealer_id` int(4) DEFAULT NULL,
  `order_type` tinyint(4) DEFAULT NULL,
  `customer_id` int(4) DEFAULT NULL,
  `order_by` int(4) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `total_amount` decimal(18,2) DEFAULT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productbatch`
--

CREATE TABLE `productbatch` (
  `id` int(50) NOT NULL,
  `productId` varchar(50) DEFAULT NULL,
  `purchaseRate` decimal(18,2) DEFAULT NULL,
  `salesRate` decimal(18,2) DEFAULT NULL,
  `trade_price` decimal(18,2) DEFAULT NULL,
  `MRP` decimal(18,2) DEFAULT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productbatch`
--

INSERT INTO `productbatch` (`id`, `productId`, `purchaseRate`, `salesRate`, `trade_price`, `MRP`, `created_date`) VALUES
(1, '1', 126.00, 126.00, 140.00, 230.00, '2019-12-11'),
(2, '2', 76.50, 76.50, 85.00, 130.00, '2019-12-03'),
(3, '3', 86.00, 86.00, 95.00, 140.00, '2019-12-14'),
(4, '4', 99.00, 99.00, 110.00, 155.00, '2019-12-14'),
(5, '5', 108.00, 108.00, 120.00, 170.00, '2019-12-14'),
(6, '6', 113.00, 113.00, 125.00, 180.00, '2019-12-14'),
(7, '7', 117.00, 117.00, 130.00, 185.00, '2019-12-14'),
(8, '8', 122.00, 122.00, 135.00, 190.00, '2019-12-14'),
(9, '9', NULL, NULL, NULL, NULL, '2019-11-30'),
(10, '10', 477.00, 477.00, 530.00, 700.00, '2019-12-09'),
(11, '11', 477.00, 477.00, 530.00, 700.00, '2019-12-09'),
(12, '12', 477.00, 477.00, 530.00, 700.00, '2019-12-11'),
(13, '13', 477.00, 477.00, 530.00, 700.00, '2019-12-14'),
(14, '14', 540.00, 540.00, 600.00, 800.00, '2019-12-11'),
(15, '15', 540.00, 540.00, 600.00, 800.00, '2019-12-14'),
(16, '16', NULL, NULL, NULL, NULL, '2019-11-30'),
(17, '17', 1350.00, 1350.00, 1500.00, 2000.00, '2019-12-09'),
(18, '18', 1350.00, 1350.00, 1500.00, 2000.00, '2019-12-09'),
(19, '19', 1350.00, 1350.00, 1500.00, 2000.00, '2019-12-09'),
(20, '20', 1530.00, 1530.00, 1700.00, 2300.00, '2019-12-09'),
(21, '21', 1530.00, 1530.00, 1700.00, 2300.00, '2019-12-09'),
(22, '22', 1530.00, 1530.00, 1700.00, 2300.00, '2019-12-09'),
(23, '23', 1530.00, 1530.00, 1700.00, 2300.00, '2019-12-09'),
(24, '24', NULL, NULL, NULL, NULL, '2019-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `product_name` varchar(55) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `product_name`, `created_date`) VALUES
(1, 'Diaper Pants', '2019-11-05'),
(2, 'Diaper ', '2019-11-05'),
(3, 'Wipes', '2019-11-05'),
(4, 'Tissue', '2019-11-05');

-- --------------------------------------------------------

--
-- Table structure for table `product_sub_list`
--

CREATE TABLE `product_sub_list` (
  `id` int(11) NOT NULL,
  `product_cat` int(11) DEFAULT NULL,
  `sublist_name` varchar(56) DEFAULT NULL,
  `pack_size` varchar(26) DEFAULT NULL,
  `carton_size` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_sub_list`
--

INSERT INTO `product_sub_list` (`id`, `product_cat`, `sublist_name`, `pack_size`, `carton_size`, `created_date`) VALUES
(1, 3, 'Kids Cotton Wet Wipes', '56Pcs', 24, '2019-11-30'),
(2, 2, 'Kids Diapers NB(0-4kg)', '04Pcs', 48, '2019-11-30'),
(3, 2, 'Kids Diapers S(3-6kg)', '04Pcs', 48, '2019-11-30'),
(4, 2, 'Kids Diapers M(5-10kg)', '04Pcs', 48, '2019-11-30'),
(5, 2, 'Kids Diapers L(9-13kg)', '04Pcs', 48, '2019-11-30'),
(6, 1, 'Kids Pants M(5-10kg)', '04Pcs', 36, '2019-11-30'),
(7, 1, 'Kids Pants L(9-14g)', '04Pcs', 36, '2019-11-30'),
(8, 1, 'Kids Pants XL(12-18g)', '04Pcs', 36, '2019-11-30'),
(9, 2, 'Kids Diapers NB(0-4kg)', '12Pcs', 18, '2019-11-30'),
(10, 2, 'Kids Diapers NB(0-4kg)', '25Pcs', 8, '2019-11-30'),
(11, 2, 'Kids Diapers S(3-6kg)', '23Pcs', 8, '2019-11-30'),
(12, 2, 'Kids Diapers M(5-10kg)', '20Pcs', 8, '2019-11-30'),
(13, 2, 'Kids Diapers L(9-13kg)', '18Pcs', 8, '2019-11-30'),
(14, 1, 'Kids Pants M(5-10kg)', '20Pcs', 8, '2019-11-30'),
(15, 1, 'Kids Pants L(9-13kg)', '19Pcs', 8, '2019-11-30'),
(16, 1, 'Kids Pants XL(12-18kg)', '18Pcs', 8, '2019-11-30'),
(17, 2, 'Kids Diapers S(3-6kg)', '68Pcs', 6, '2019-11-30'),
(18, 2, 'Kids Diapers M(5-10kg)', '62Pcs', 6, '2019-11-30'),
(19, 2, 'Kids Diapers L(9-13kg)', '58Pcs', 6, '2019-11-30'),
(20, 1, 'Kids Pants M(5-10kg)', '60Pcs', 6, '2019-11-30'),
(21, 1, 'Kids Pants L(9-14kg)', '58Pcs', 6, '2019-11-30'),
(22, 1, 'Kids Pants XL(12-18kg)', '56Pcs', 6, '2019-11-30'),
(23, 1, 'Kids Pants XXL(16-24kg)', '52Pcs', 8, '2019-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `product_unit`
--

CREATE TABLE `product_unit` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_unit`
--

INSERT INTO `product_unit` (`id`, `unit_name`) VALUES
(1, 'PCS'),
(4, 'CARTOON');

-- --------------------------------------------------------

--
-- Table structure for table `purchasemaster`
--

CREATE TABLE `purchasemaster` (
  `purchaseMasterId` int(50) NOT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchaseInvoiceNo` varchar(55) DEFAULT NULL,
  `vendorId` varchar(50) DEFAULT NULL,
  `prod_group` int(11) DEFAULT NULL,
  `prod_id` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `unit_price` int(24) DEFAULT NULL,
  `sale_price` varchar(11) DEFAULT NULL,
  `trade_price` decimal(18,2) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchasemaster`
--

INSERT INTO `purchasemaster` (`purchaseMasterId`, `purchase_date`, `purchaseInvoiceNo`, `vendorId`, `prod_group`, `prod_id`, `unit`, `unit_price`, `sale_price`, `trade_price`, `mrp`, `amount`, `date`) VALUES
(1, '2019-12-01', '01', 'china', 2, 2, 1, 76, '76', 85.00, 130, 912000.00, '2019-12-08'),
(5, '2019-12-01', '02', 'china', 2, 17, 1, 1350, '1350', 1500.00, 2000, 429300.00, '2019-12-09'),
(6, '2019-12-01', '03', 'china', 2, 11, 1, 477, '477', 530.00, 700, 312912.00, '2019-12-09'),
(7, '2019-12-01', '04', 'china', 2, 18, 1, 1350, '1350', 1500.00, 2000, 899100.00, '2019-12-09'),
(8, '2019-12-01', '05', 'china', 2, 19, 1, 1350, '1350', 1500.00, 2000, 899100.00, '2019-12-09'),
(9, '2019-12-01', '05', 'china', 1, 22, 1, 1530, '1530', 1700.00, 2300, 1037340.00, '2019-12-09'),
(10, '2019-12-01', '06', 'china', 1, 23, 1, 1530, '1530', 1700.00, 2300, 1224000.00, '2019-12-09'),
(11, '2019-12-01', '07', 'china', 1, 21, 1, 1530, '1530', 1700.00, 2300, 1028160.00, '2019-12-09'),
(12, '2019-12-07', 'Khulna', 'china', 2, 10, 1, 477, '477', 530.00, 700, 6678.00, '2019-12-09'),
(13, '2019-12-07', 'Khulna', 'china', 2, 13, 1, 477, '477', 530.00, 700, 22419.00, '2019-12-09'),
(14, '2019-12-07', 'Khulna', 'china', 1, 7, 1, 117, '117', 130.00, 185, 12636.00, '2019-12-09'),
(15, '2019-12-07', 'Khulna', 'china', 1, 8, 1, 122, '122', 135.00, 190, 13176.00, '2019-12-09'),
(16, '2019-12-07', 'Khulna', 'china', 2, 5, 1, 108, '108', 120.00, 170, 15552.00, '2019-12-09'),
(17, '2019-12-01', '10', 'china', 1, 20, 1, 1530, '1530', 1700.00, 2300, 1028160.00, '2019-12-09'),
(18, '2019-12-10', '12', 'china', 3, 1, 1, 126, '126', 140.00, 230, 2866752.00, '2019-12-11'),
(19, '2019-12-11', 'Gulshan', 'china', 2, 12, 1, 477, '477', 530.00, 700, 106848.00, '2019-12-11'),
(20, '2019-12-11', 'Gulshan', 'china', 2, 13, 1, 477, '477', 530.00, 700, 190800.00, '2019-12-11'),
(21, '2019-12-11', 'Gulshan', 'china', 1, 14, 1, 540, '540', 600.00, 800, 73440.00, '2019-12-11'),
(22, '2019-12-14', '11288', 'Local Vendor', 2, 3, 1, 86, '86', 95.00, 140, 172.00, '2019-12-14'),
(23, '2019-12-14', '11288', 'Local Vendor', 2, 4, 1, 99, '99', 110.00, 155, 85140.00, '2019-12-14'),
(24, '2019-12-14', '11288', 'Local Vendor', 2, 5, 1, 108, '108', 120.00, 170, 71064.00, '2019-12-14'),
(25, '2019-12-14', '11288', 'Local Vendor', 1, 6, 1, 113, '113', 125.00, 180, 2599.00, '2019-12-14'),
(26, '2019-12-14', '11288', 'Local Vendor', 1, 7, 1, 117, '117', 130.00, 185, 95589.00, '2019-12-14'),
(27, '2019-12-14', '11288', 'Local Vendor', 1, 8, 1, 122, '122', 135.00, 190, 92476.00, '2019-12-14'),
(28, '2019-12-14', '11288', 'Local Vendor', 2, 13, 1, 477, '477', 530.00, 700, 1908.00, '2019-12-14'),
(29, '2019-12-14', '11288', 'Local Vendor', 1, 15, 1, 540, '540', 600.00, 800, 1620.00, '2019-12-14');

-- --------------------------------------------------------

--
-- Table structure for table `salesmaster`
--

CREATE TABLE `salesmaster` (
  `salesMasterId` int(50) NOT NULL,
  `salesInvoiceNo` varchar(50) DEFAULT NULL,
  `disburse_type` int(4) DEFAULT NULL,
  `disburse_to` int(11) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `total_amount` decimal(18,2) DEFAULT NULL,
  `delivery_charge` decimal(18,2) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `netamount` decimal(18,2) DEFAULT NULL,
  `paid_amount` decimal(18,2) DEFAULT NULL,
  `due_amount` decimal(10,0) DEFAULT NULL,
  `delivered_by` int(50) DEFAULT NULL,
  `delivery_by` varchar(56) DEFAULT NULL,
  `remarks` varchar(128) DEFAULT NULL,
  `print_type` tinyint(4) DEFAULT NULL,
  `delivery_status` tinyint(4) DEFAULT NULL,
  `payment_status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesmaster`
--

INSERT INTO `salesmaster` (`salesMasterId`, `salesInvoiceNo`, `disburse_type`, `disburse_to`, `customer`, `date`, `total_amount`, `delivery_charge`, `discount`, `netamount`, `paid_amount`, `due_amount`, `delivered_by`, `delivery_by`, `remarks`, `print_type`, `delivery_status`, `payment_status`) VALUES
(1, '2547830', 4, 0, 1, '2019-12-08', 25500.00, 0.00, 0, 25500.00, 0.00, 25500, 9, NULL, NULL, 1, 1, 1),
(2, '0627189', 4, 0, 3, '2019-12-08', 48270.00, 0.00, 0, 48270.00, 0.00, 48270, 10, NULL, NULL, 1, 1, 1),
(3, '1293804', 5, 14, 0, '2019-12-09', 400968.00, 0.00, 0, 400968.00, 400000.00, 968, 10, NULL, NULL, 1, 1, 1),
(4, '486290', 5, 16, 0, '2019-12-11', 21168.00, 0.00, 0, 21168.00, 20000.00, 1168, 10, NULL, NULL, 1, 1, 1),
(5, '396025', 5, 17, 0, '2019-12-11', 151200.00, 0.00, 0, 151200.00, 0.00, 151200, 10, NULL, NULL, 1, 1, 1),
(6, '625704', 5, 17, 0, '2019-12-03', 715680.00, 0.00, 0, 715680.00, 400000.00, 315680, 10, NULL, NULL, 1, 1, 1),
(7, '692570', 5, 17, 0, '2019-12-04', 30600.00, 0.00, 0, 30600.00, 0.00, 30600, 10, NULL, NULL, 1, 1, 1),
(8, '702516', 5, 16, 0, '2019-12-03', 100944.00, 0.00, 0, 100944.00, 100000.00, 944, 10, NULL, NULL, 1, 1, 1),
(9, '704681', 5, 14, 0, '2019-12-05', 204768.00, 0.00, 0, 204768.00, 200000.00, 4768, 10, NULL, NULL, 1, 1, 1),
(10, '706895', 5, 11, 0, '2019-12-05', 304560.00, 0.00, 0, 304560.00, 300000.00, 4560, 10, NULL, NULL, 1, 1, 1),
(11, '937612', 5, 19, 0, '2019-12-03', 142560.00, 0.00, 0, 142560.00, 142000.00, 560, 10, NULL, NULL, 1, 1, 1),
(12, '410975', 5, 20, 0, '2019-12-03', 70668.00, 0.00, 0, 70668.00, 100000.00, -29332, 10, NULL, NULL, 1, 1, 1),
(13, '418072', 5, 20, 0, '2019-12-10', 43740.00, 0.00, 0, 43740.00, 43732.00, 8, 10, NULL, NULL, 1, 1, 1),
(14, '960578', 5, 11, 0, '2019-12-12', 206784.00, 0.00, 0, 206784.00, 204000.00, 2784, 10, NULL, NULL, 1, 1, 1),
(15, '428756', 5, 19, 0, '2019-12-12', 51408.00, 0.00, 0, 51408.00, 50000.00, 1408, 10, NULL, NULL, 1, 1, 1),
(16, '652831', 5, 14, 0, '2019-12-12', 151200.00, 0.00, 0, 151200.00, 0.00, 151200, 10, NULL, NULL, 1, 1, 1),
(17, '063524', 4, 0, 6, '2019-12-12', 6240.00, 0.00, 0, 6240.00, 0.00, 6240, 10, NULL, NULL, 1, 1, 1),
(18, '562083', 4, 0, 5, '2019-12-12', 3120.00, 0.00, 0, 3120.00, 0.00, 3120, 10, NULL, NULL, 1, 1, 1),
(19, '794582', 5, 21, 0, '2019-12-12', 502164.00, 0.00, 0, 502164.00, 500000.00, 2164, 10, NULL, NULL, 1, 1, 1),
(20, '259364', 3, 0, 7, '2019-12-14', 2000.00, 0.00, 15, 1700.00, 0.00, 1700, 10, NULL, NULL, 1, 1, 1),
(21, '804573', 3, 0, 8, '2019-12-14', 700.00, 100.00, 15, 695.00, 0.00, 695, 10, NULL, NULL, 1, 1, 1),
(22, '650293', 3, 0, 9, '2019-12-14', 2000.00, 0.00, 15, 1700.00, 0.00, 1700, 10, NULL, NULL, 1, 1, 1),
(23, '610439', 3, 0, 10, '2019-12-14', 1400.00, 0.00, 15, 1190.00, 0.00, 1190, 10, NULL, NULL, 1, 1, 1),
(24, '528931', 3, 0, 11, '2019-12-14', 3500.00, 0.00, 15, 2975.00, 0.00, 2975, 10, NULL, NULL, 1, 1, 1),
(25, '753094', 3, 0, 12, '2019-12-14', 920.00, 0.00, 15, 782.00, 0.00, 0, 10, NULL, NULL, 1, 1, 1),
(26, '764530', 3, 0, 16, '2019-12-14', 2300.00, 0.00, 15, 1955.00, 0.00, 1955, 9, NULL, NULL, 1, 1, 1),
(27, '418705', 3, 0, 17, '2019-12-14', 4000.00, 0.00, 15, 3400.00, 0.00, 3400, 9, NULL, NULL, 1, 1, 1),
(28, '436095', 3, 0, 18, '2019-12-14', 930.00, 0.00, 15, 791.00, 0.00, 791, 9, NULL, NULL, 1, 1, 1),
(29, '690381', 3, 0, 19, '2019-12-15', 2300.00, 0.00, 15, 1955.00, 0.00, 1955, 9, NULL, NULL, 1, 1, 1),
(30, '328516', 3, 0, 20, '2019-12-15', 2300.00, 150.00, 15, 2105.00, 0.00, 2105, 9, NULL, NULL, 1, 1, 1),
(31, '871356', 3, 0, 21, '2019-12-15', 2300.00, 0.00, 15, 1955.00, 0.00, 1955, 9, NULL, NULL, 1, 1, 1),
(32, '240153', 3, 0, 22, '2019-12-15', 2300.00, 0.00, 15, 1955.00, 0.00, 1955, 9, NULL, NULL, 1, 1, 1),
(33, '863450', 3, 0, 24, '2019-12-15', 2000.00, 100.00, 15, 1800.00, 0.00, 1800, 9, NULL, NULL, 1, 1, 1),
(34, '106725', 2, 0, 23, '2019-12-15', 26664.00, 0.00, 0, 26664.00, 0.00, 26664, 10, NULL, NULL, 1, 1, 1),
(35, '720364', 2, 0, 23, '2019-12-15', 27600.00, 0.00, 0, 27600.00, 0.00, 27600, 10, NULL, NULL, 1, 1, 1),
(38, '934271', 3, 0, 25, '2019-12-15', 1530.00, 0.00, 0, 1530.00, 1530.00, 0, 9, NULL, NULL, 1, 1, 1),
(39, '802915', 3, 0, 26, '2019-12-15', 2300.00, 0.00, 15, 1955.00, 0.00, 1955, 9, NULL, NULL, 1, 1, 1),
(40, '179632', 3, 0, 27, '2019-12-14', 5150.00, 0.00, 15, 4378.00, 4378.00, 0, 9, NULL, NULL, 1, 1, 1),
(41, '945862', 3, 0, 30, '2019-12-16', 392.00, 50.00, NULL, 442.00, 0.00, 442, 9, NULL, NULL, NULL, 1, 1),
(42, '975682', 3, 0, 28, '2019-12-17', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, NULL, NULL, NULL, 1, 1),
(43, '958210', 3, 0, 29, '2019-12-17', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(44, '403957', 3, 0, 31, '2019-12-17', 11730.00, 0.00, NULL, 11730.00, 0.00, 11730, 9, NULL, NULL, NULL, 1, 1),
(45, '631927', 3, 0, 32, '2019-12-17', 481.00, 50.00, NULL, 531.00, 0.00, 531, 9, NULL, NULL, NULL, 1, 1),
(46, '453021', 3, 0, 33, '2019-12-17', 791.00, 0.00, NULL, 791.00, 0.00, 791, 9, NULL, NULL, NULL, 1, 1),
(47, '407589', 3, 0, 34, '2019-12-17', 1896.00, 0.00, NULL, 1896.00, 0.00, 1896, 9, NULL, NULL, NULL, 1, 1),
(48, '706295', 3, 0, 35, '2019-12-17', 1955.00, 150.00, NULL, 2105.00, 0.00, 2105, 9, NULL, NULL, NULL, 1, 1),
(49, '198723', 3, 0, 36, '2019-12-17', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(50, '547903', 3, 0, 37, '2019-12-07', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(52, '837602', 3, 0, 38, '2019-12-07', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(53, '670159', 3, 0, 40, '2019-12-02', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(55, '076943', 3, 0, 42, '2019-12-02', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(56, '239465', 3, 0, 44, '2019-12-02', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(57, '295680', 3, 0, 45, '2019-12-02', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(58, '182579', 3, 0, 46, '2019-12-02', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(59, '852714', 3, 0, 47, '2019-12-02', 3910.00, 0.00, NULL, 3910.00, 3910.00, 0, 9, NULL, NULL, NULL, 1, 1),
(60, '957320', 5, 20, 0, '2019-12-17', 24192.00, 0.00, NULL, 24192.00, 24000.00, 192, 10, NULL, NULL, NULL, 1, 1),
(61, '053698', 5, 11, 0, '2019-12-17', 103320.00, 0.00, NULL, 103320.00, 104000.00, -680, 10, NULL, NULL, NULL, 1, 1),
(62, '893427', 3, 0, 48, '2019-12-02', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(63, '471258', 3, 0, 49, '2019-12-02', 3400.00, 0.00, NULL, 3400.00, 3400.00, 0, 9, NULL, NULL, NULL, 1, 1),
(64, '431267', 3, 0, 50, '2019-12-02', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(65, '356198', 3, 0, 51, '2019-12-03', 3910.00, 0.00, NULL, 3910.00, 3910.00, 0, 9, NULL, NULL, NULL, 1, 1),
(66, '210384', 3, 0, 52, '2019-12-03', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(67, '725168', 3, 0, 53, '2019-12-03', 3400.00, 0.00, NULL, 3400.00, 3400.00, 0, 9, NULL, NULL, NULL, 1, 1),
(68, '326594', 3, 0, 54, '2019-12-03', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(69, '193084', 3, 0, 55, '2019-12-03', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(70, '421396', 3, 0, 19, '2019-12-03', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(71, '495870', 3, 0, 57, '2019-12-03', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(72, '746859', 3, 0, 58, '2019-12-03', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(73, '796425', 3, 0, 59, '2019-12-03', 1110.00, 0.00, NULL, 1110.00, 0.00, 1110, 9, NULL, NULL, NULL, 1, 1),
(74, '923150', 3, 0, 61, '2019-12-03', 1110.00, 100.00, NULL, 1210.00, 0.00, 1210, 9, NULL, NULL, NULL, 1, 1),
(75, '925384', 3, 0, 28, '2019-12-04', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(76, '358706', 3, 0, 63, '2019-12-04', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(77, '489632', 3, 0, 64, '2019-12-04', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(78, '750641', 5, 29, 0, '2019-12-17', 29130.00, 0.00, NULL, 29130.00, 0.00, 29130, 10, NULL, NULL, NULL, 1, 1),
(79, '231970', 3, 0, 65, '2019-12-04', 5100.00, 0.00, NULL, 5100.00, 5100.00, 0, 9, NULL, NULL, NULL, 1, 1),
(80, '137824', 3, 0, 66, '2019-12-04', 888.00, 100.00, NULL, 988.00, 0.00, 988, 9, NULL, NULL, NULL, 1, 1),
(81, '180352', 3, 0, 67, '2019-12-04', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(82, '278309', 3, 0, 69, '2019-12-05', 1700.00, 100.00, NULL, 1800.00, 0.00, 1800, 9, NULL, NULL, NULL, 1, 1),
(83, '914203', 3, 0, 70, '2019-12-07', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(84, '450916', 3, 0, 72, '2019-12-07', 3910.00, 0.00, NULL, 3910.00, 3910.00, 0, 9, NULL, NULL, NULL, 1, 1),
(85, '825016', 3, 0, 73, '2019-12-07', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(86, '742190', 3, 0, 74, '2019-12-07', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(87, '234716', 3, 0, 75, '2019-12-07', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, NULL, NULL, NULL, 1, 1),
(88, '120873', 3, 0, 76, '2019-12-07', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(89, '768539', 3, 0, 77, '2019-12-07', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(90, '748310', 3, 0, 51, '2019-12-18', 3910.00, 0.00, NULL, 3910.00, 0.00, 3910, 9, NULL, NULL, NULL, 1, 1),
(91, '965720', 3, 0, 78, '2019-12-18', 2295.00, 0.00, NULL, 2295.00, 0.00, 2295, 9, NULL, NULL, NULL, 1, 1),
(92, '021539', 3, 0, 79, '2019-12-18', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(93, '917523', 3, 0, 81, '2019-12-18', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(95, '954362', 5, 14, 0, '2019-12-18', 65484.00, 0.00, NULL, 65484.00, 0.00, 65484, 10, NULL, NULL, NULL, 1, 1),
(96, '127965', 3, 0, 82, '2019-12-18', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(97, '859071', 3, 0, 83, '2019-12-18', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(98, '035964', 5, 17, 0, '2019-12-18', 335340.00, 0.00, NULL, 335340.00, 0.00, 335340, 10, NULL, NULL, NULL, 1, 1),
(99, '764859', 3, 0, 84, '2019-12-18', 1955.00, 150.00, NULL, 2105.00, 0.00, 2105, 9, NULL, NULL, NULL, 1, 1),
(100, '912387', 3, 0, 85, '2019-12-18', 111.00, 50.00, NULL, 161.00, 0.00, 161, 9, NULL, NULL, NULL, 1, 1),
(101, '914758', 3, 0, 86, '2019-12-18', 3915.00, 150.00, NULL, 4065.00, 0.00, 4065, 9, NULL, NULL, NULL, 1, 1),
(102, '258037', 5, 30, 0, '2019-12-18', 64108.00, 0.00, NULL, 64108.00, 0.00, 64108, 10, NULL, NULL, NULL, 1, 1),
(103, '634902', 3, 0, 87, '2019-12-18', 2862.00, 0.00, NULL, 2862.00, 2862.00, 0, 10, NULL, NULL, NULL, 1, 1),
(104, '419237', 3, 0, 88, '2019-12-19', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 10, NULL, NULL, NULL, 1, 1),
(105, '069153', 3, 0, 89, '2019-12-19', 588.00, 50.00, NULL, 638.00, 0.00, 638, 10, NULL, NULL, NULL, 1, 1),
(106, '418267', 3, 0, 90, '2019-12-19', 1955.00, 150.00, NULL, 2105.00, 0.00, 2105, 10, NULL, NULL, NULL, 1, 1),
(107, '075124', 3, 0, 57, '2019-12-19', 3910.00, 0.00, NULL, 3910.00, 0.00, 3910, 10, NULL, NULL, NULL, 1, 1),
(108, '742018', 3, 0, 92, '2019-12-19', 3400.00, 0.00, NULL, 3400.00, 0.00, 3400, 10, NULL, NULL, NULL, 1, 1),
(109, '927154', 3, 0, 93, '2019-12-19', 1275.00, 0.00, NULL, 1275.00, 0.00, 1275, 9, NULL, NULL, NULL, 1, 1),
(110, '473109', 3, 0, 32, '2019-12-19', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(111, '960278', 3, 0, 95, '2019-12-19', 680.00, 50.00, NULL, 730.00, 0.00, 730, 9, NULL, NULL, NULL, 1, 1),
(112, '806713', 3, 0, 96, '2019-12-19', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(113, '593402', 3, 0, 97, '2019-12-19', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(114, '182695', 3, 0, 98, '2019-12-08', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 10, NULL, NULL, NULL, 1, 1),
(115, '753049', 3, 0, 2, '2019-12-08', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(116, '852610', 3, 0, 99, '2019-12-09', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(117, '051629', 3, 0, 100, '2019-12-09', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 10, NULL, NULL, NULL, 1, 1),
(118, '284539', 3, 0, 101, '2019-12-10', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(119, '850634', 3, 0, 41, '2019-12-10', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 10, NULL, NULL, NULL, 1, 1),
(120, '361095', 3, 0, 4, '2019-12-10', 5100.00, 0.00, NULL, 5100.00, 5100.00, 0, 10, NULL, NULL, NULL, 1, 1),
(121, '780952', 3, 0, 102, '2019-12-10', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(122, '064972', 3, 0, 103, '2019-12-10', 785.00, 0.00, NULL, 785.00, 785.00, 0, 10, NULL, NULL, NULL, 1, 1),
(123, '326910', 3, 0, 104, '2019-12-10', 595.00, 0.00, NULL, 595.00, 595.00, 0, 10, NULL, NULL, NULL, 1, 1),
(124, '081695', 3, 0, 105, '2019-12-10', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(125, '260784', 3, 0, 106, '2019-12-10', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(126, '275603', 3, 0, 107, '2019-12-11', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 10, NULL, NULL, NULL, 1, 1),
(127, '720685', 3, 0, 108, '2019-12-11', 1950.00, 0.00, NULL, 1950.00, 1950.00, 0, 10, NULL, NULL, NULL, 1, 1),
(128, '428371', 3, 0, 109, '2019-12-11', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 10, NULL, NULL, NULL, 1, 1),
(129, '408951', 3, 0, 110, '2019-12-11', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(130, '073912', 3, 0, 111, '2019-12-11', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 10, NULL, NULL, NULL, 1, 1),
(131, '150437', 3, 0, 112, '2019-12-11', 1955.00, 100.00, NULL, 2055.00, 0.00, 2055, 9, NULL, NULL, NULL, 1, 1),
(132, '906251', 3, 0, 113, '2019-12-01', 1850.00, 0.00, NULL, 1850.00, 1850.00, 0, 9, NULL, NULL, NULL, 1, 1),
(133, '521690', 3, 0, 114, '2019-12-12', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, NULL, NULL, NULL, 1, 1),
(134, '519203', 3, 0, 104, '2019-12-12', 975.00, 0.00, NULL, 975.00, 975.00, 0, 9, NULL, NULL, NULL, 1, 1),
(135, '741602', 3, 0, 116, '2019-12-12', 3735.00, 150.00, NULL, 3885.00, 0.00, 3885, 9, NULL, NULL, NULL, 1, 1),
(136, '013967', 3, 0, 117, '2019-12-12', 2380.00, 0.00, NULL, 2380.00, 2380.00, 0, 9, NULL, NULL, NULL, 1, 1),
(137, '867095', 3, 0, 118, '2019-12-14', 1955.00, 100.00, NULL, 2055.00, 0.00, 2055, 9, NULL, NULL, NULL, 1, 1),
(138, '960145', 3, 0, 119, '2019-12-21', 4184.00, 0.00, NULL, 4184.00, 0.00, 4184, 9, NULL, NULL, NULL, 1, 1),
(139, '815904', 3, 0, 120, '2019-12-21', 3910.00, 0.00, NULL, 3910.00, 0.00, 3910, 9, NULL, NULL, NULL, 1, 1),
(140, '529160', 3, 0, 121, '2019-12-21', 2066.00, 0.00, NULL, 2066.00, 0.00, 2066, 9, NULL, NULL, NULL, 1, 1),
(141, '279864', 3, 0, 123, '2019-12-21', 680.00, 50.00, NULL, 730.00, 0.00, 730, 9, NULL, NULL, NULL, 1, 1),
(142, '527648', 3, 0, 124, '2019-12-21', 1190.00, 0.00, NULL, 1190.00, 0.00, 1190, 9, NULL, NULL, NULL, 1, 1),
(143, '960412', 1, 0, 125, '2019-12-21', 152800.00, 0.00, NULL, 152800.00, 0.00, 152800, 10, NULL, NULL, NULL, 1, 1),
(144, '097583', 3, 0, 126, '2019-12-21', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(145, '376580', 3, 0, 127, '2019-12-21', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, NULL, NULL, NULL, 1, 1),
(146, '509413', 3, 0, 128, '2019-12-21', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, NULL, NULL, NULL, 1, 1),
(148, '105387', 3, 0, 130, '2019-12-21', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, NULL, NULL, NULL, 1, 1),
(149, '564231', 3, 0, 131, '2019-12-21', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(150, '915407', 3, 0, 132, '2019-12-21', 987.00, 0.00, NULL, 987.00, 0.00, 987, 9, NULL, NULL, NULL, 1, 1),
(151, '901852', 3, 0, 109, '2019-12-21', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(152, '976318', 3, 0, 133, '2019-12-21', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, NULL, NULL, NULL, 1, 1),
(153, '038274', 3, 0, 63, '2019-12-21', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(154, '953824', 3, 0, 135, '2019-12-21', 5865.00, 0.00, NULL, 5865.00, 0.00, 5865, 9, NULL, NULL, NULL, 1, 1),
(155, '198075', 5, 16, 0, '2019-12-21', 39636.00, 0.00, NULL, 39636.00, 40000.00, -364, 10, NULL, NULL, NULL, 1, 1),
(156, '734085', 3, 0, 136, '2019-12-21', 2151.00, 0.00, NULL, 2151.00, 0.00, 2151, 9, NULL, NULL, NULL, 1, 1),
(157, '589326', 2, 0, 137, '2019-12-21', 70368.00, 0.00, NULL, 70368.00, 0.00, 70368, 10, NULL, NULL, NULL, 1, 1),
(159, '206758', 3, 0, 138, '2019-12-21', 2635.00, 0.00, NULL, 2635.00, 0.00, 2635, 9, NULL, NULL, NULL, 1, 1),
(160, '687429', 3, 0, 141, '2019-12-21', 1360.00, 0.00, NULL, 1360.00, 0.00, 1360, 9, NULL, NULL, NULL, 1, 1),
(161, '826540', 2, 0, 139, '2019-12-21', 68292.00, 0.00, NULL, 68292.00, 0.00, 68292, 10, NULL, NULL, NULL, 1, 1),
(162, '624315', 3, 0, 142, '2019-12-21', 3400.00, 0.00, NULL, 3400.00, 0.00, 3400, 9, NULL, NULL, NULL, 1, 1),
(163, '058473', 3, 0, 143, '2019-12-21', 3655.00, 300.00, NULL, 3955.00, 0.00, 3955, 9, NULL, NULL, NULL, 1, 1),
(164, '592438', 3, 0, 144, '2019-12-21', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(165, '586127', 3, 0, 131, '2019-12-22', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, NULL, NULL, NULL, 1, 1),
(166, '403587', 3, 0, 146, '2019-12-22', 3400.00, 0.00, NULL, 3400.00, 0.00, 3400, 9, NULL, NULL, NULL, 1, 1),
(168, '679538', 5, 20, 0, '2019-12-22', 23904.00, 0.00, NULL, 23904.00, 0.00, 23904, 10, NULL, NULL, NULL, 1, 1),
(170, '803572', 3, 0, 148, '2019-12-22', 595.00, 150.00, NULL, 610.00, 0.00, 610, 9, NULL, NULL, NULL, 1, 1),
(171, '483569', 3, 0, 150, '2019-12-22', 595.00, 50.00, NULL, 645.00, 0.00, 645, 9, NULL, NULL, NULL, 1, 1),
(172, '650982', 3, 0, 151, '2019-12-22', 1190.00, 0.00, NULL, 1190.00, 0.00, 1190, 9, NULL, NULL, NULL, 1, 1),
(173, '927608', 5, 19, 0, '2019-12-22', 100620.00, 0.00, NULL, 100620.00, 0.00, 100620, 10, '', '', NULL, 1, 1),
(174, '234589', 3, 0, 152, '2019-12-22', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(176, '392178', 3, 0, 153, '2019-12-22', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(177, '589324', 3, 0, 48, '2019-12-22', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, 1, 1),
(178, '503241', 3, 0, 155, '2019-12-22', 2208.00, 0.00, NULL, 2208.00, 0.00, 2208, 9, '', '', NULL, 1, 1),
(179, '396284', 3, 0, 156, '2019-12-22', 785.00, 150.00, NULL, 935.00, 0.00, 935, 9, '', '', NULL, 1, 1),
(180, '310467', 3, 0, 74, '2019-12-22', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, 1, 1),
(181, '408296', 3, 0, 158, '2019-12-22', 119.00, 50.00, NULL, 169.00, 0.00, 169, 9, '', '', NULL, 1, 1),
(183, '145092', 2, 0, 160, '2019-12-22', 25788.00, 0.00, NULL, 25788.00, 0.00, 25788, 9, '', '', NULL, 1, 1),
(184, '407869', 4, 0, 6, '2019-12-22', 23040.00, 0.00, NULL, 23040.00, 0.00, 23040, 10, '', '', NULL, 1, 1),
(186, '512869', 3, 0, 161, '2019-12-23', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(187, '703218', 3, 0, 162, '2019-12-23', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, 1, 1),
(188, '702958', 3, 0, 163, '2019-12-23', 1896.00, 150.00, NULL, 2046.00, 0.00, 2046, 9, '', '', NULL, 1, 1),
(189, '214879', 5, 11, 0, '2019-12-23', 173160.00, 0.00, NULL, 173160.00, 0.00, 173160, 9, '', '', NULL, 1, 1),
(190, '712035', 3, 0, 164, '2019-12-23', 595.00, 50.00, NULL, 645.00, 0.00, 645, 9, '', '', NULL, 1, 1),
(191, '452701', 3, 0, 27, '2019-12-23', 785.00, 50.00, NULL, 835.00, 0.00, 835, 9, '', '', NULL, 1, 1),
(193, '605271', 3, 0, 167, '2019-12-23', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, 1, 1),
(194, '849762', 3, 0, 159, '2019-12-23', 2151.00, 0.00, NULL, 2151.00, 0.00, 2151, 9, '', '', NULL, 1, 1),
(195, '948130', 3, 0, 64, '2019-12-23', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(196, '941803', 3, 0, 168, '2019-12-23', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(197, '745928', 3, 0, 169, '2019-12-24', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(198, '296417', 4, 0, 3, '2019-12-01', 12357.50, 0.00, NULL, 12358.00, 0.00, 12358, 10, '', '', NULL, 1, 1),
(199, '456291', 3, 0, 101, '2019-12-24', 512.00, 50.00, NULL, 562.00, 0.00, 562, 9, '', '', NULL, 1, 1),
(200, '370285', 3, 0, 171, '2019-12-24', 1940.00, 0.00, NULL, 1940.00, 0.00, 1940, 9, '', '', NULL, 1, 1),
(202, '793840', 3, 0, 43, '2019-12-24', 3400.00, 0.00, NULL, 3400.00, 0.00, 3400, 9, '', '', NULL, 1, 1),
(203, '025478', 3, 0, 173, '2019-12-24', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, 1, 1),
(204, '542961', 3, 0, 99, '2019-12-24', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, 1, 1),
(205, '193604', 2, 0, 23, '2019-12-24', 46239.00, 0.00, NULL, 46239.00, 0.00, 46239, 9, '', '', NULL, 1, 1),
(206, '209156', 3, 0, 174, '2019-12-24', 595.00, 150.00, NULL, 745.00, 0.00, 745, 9, '', '', NULL, 1, 1),
(207, '043869', 3, 0, 67, '2019-12-24', 680.00, 50.00, NULL, 730.00, 0.00, 730, 9, '', '', NULL, 1, 1),
(208, '679324', 3, 0, 172, '2019-12-24', 1275.00, 0.00, NULL, 1275.00, 0.00, 1275, 9, '', '', NULL, 1, 1),
(209, '693874', 3, 0, 175, '2019-12-05', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, '', '', NULL, NULL, NULL),
(210, '402836', 3, 0, 176, '2019-12-05', 5100.00, 0.00, NULL, 5100.00, 5100.00, 0, 9, '', '', NULL, NULL, NULL),
(211, '321506', 3, 0, 22, '2019-12-05', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, '', '', NULL, NULL, NULL),
(212, '251976', 3, 0, 177, '2019-12-05', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, '', '', NULL, NULL, NULL),
(213, '596142', 3, 0, 117, '2019-12-05', 1332.00, 0.00, NULL, 1332.00, 1300.00, 32, 9, '', '', NULL, NULL, NULL),
(215, '713429', 3, 0, 178, '2019-12-05', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(216, '714853', 3, 0, 27, '2019-12-05', 1700.00, 0.00, NULL, 1700.00, 1700.00, 0, 9, '', '', NULL, NULL, NULL),
(217, '362180', 3, 0, 179, '2019-12-08', 1955.00, 0.00, NULL, 1955.00, 1955.00, 0, 9, '', '', NULL, NULL, NULL),
(218, '804752', 3, 0, 93, '2019-12-26', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(219, '512468', 3, 0, 180, '2019-12-26', 1700.00, 150.00, NULL, 1715.00, 0.00, 1715, 9, '', '', NULL, NULL, NULL),
(220, '596314', 3, 0, 123, '2019-12-26', 1360.00, 0.00, NULL, 1360.00, 0.00, 1360, 9, '', '', NULL, NULL, NULL),
(221, '960825', 3, 0, 181, '2019-12-26', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(222, '613579', 3, 0, 182, '2019-12-26', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(223, '946580', 3, 0, 183, '2019-12-26', 4327.00, 0.00, NULL, 4327.00, 0.00, 4327, 9, '', '', NULL, NULL, NULL),
(224, '591302', 3, 0, 184, '2019-12-26', 153.00, 50.00, NULL, 203.00, 0.00, 203, 9, '', '', NULL, NULL, NULL),
(225, '154980', 3, 0, 185, '2019-12-26', 595.00, 50.00, NULL, 645.00, 0.00, 645, 9, '', '', NULL, NULL, NULL),
(226, '906428', 5, 14, 0, '2019-12-26', 97416.00, 0.00, NULL, 97416.00, 0.00, 97416, 10, '', '', NULL, NULL, NULL),
(227, '372806', 5, 11, 0, '2019-12-26', 185760.00, 0.00, NULL, 185760.00, 0.00, 185760, 10, '', '', NULL, NULL, NULL),
(228, '073268', 3, 0, 186, '2019-12-26', 1190.00, 0.00, NULL, 1190.00, 0.00, 1190, 9, '', '', NULL, NULL, NULL),
(229, '083541', 3, 0, 179, '2019-12-26', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(230, '146257', 4, 0, 187, '2019-12-26', 3600.00, 0.00, NULL, 3600.00, 0.00, 3600, 10, '', '', NULL, NULL, NULL),
(231, '924610', 3, 0, 188, '2019-12-26', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(232, '623579', 3, 0, 189, '2019-12-26', 1955.00, 150.00, NULL, 1970.00, 0.00, 1970, 9, '', '', NULL, NULL, NULL),
(233, '639578', 3, 0, 66, '2019-12-28', 1190.00, 150.00, NULL, 1340.00, 1340.00, 0, 9, '', '', NULL, NULL, NULL),
(234, '216430', 3, 0, 190, '2019-12-28', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(235, '408235', 3, 0, 191, '2019-12-28', 784.00, 50.00, NULL, 834.00, 0.00, 834, 9, '', '', NULL, NULL, NULL),
(236, '903276', 1, 0, 125, '2019-12-28', 113200.00, 0.00, NULL, 113200.00, 0.00, 113200, 10, '', '', NULL, NULL, NULL),
(237, '347102', 3, 0, 192, '2019-12-28', 324.00, 50.00, NULL, 374.00, 0.00, 374, 9, '', '', NULL, NULL, NULL),
(238, '635280', 3, 0, 193, '2019-12-28', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(239, '732456', 3, 0, 194, '2019-12-28', 680.00, 150.00, NULL, 695.00, 0.00, 695, 9, '', '', NULL, NULL, NULL),
(240, '051967', 3, 0, 195, '2019-12-28', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(241, '409675', 3, 0, 196, '2019-12-28', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(242, '402158', 3, 0, 197, '2019-12-28', 1955.00, 150.00, NULL, 1970.00, 0.00, 1970, 9, '', '', NULL, NULL, NULL),
(243, '165379', 3, 0, 198, '2019-12-28', 680.00, 50.00, NULL, 730.00, 0.00, 730, 9, '', '', NULL, NULL, NULL),
(244, '235496', 3, 0, 36, '2019-12-28', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(245, '246509', 3, 0, 4, '2019-12-28', 1998.00, 0.00, NULL, 1998.00, 0.00, 1998, 9, '', '', NULL, NULL, NULL),
(246, '540861', 3, 0, 94, '2019-12-29', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(247, '089435', 5, 16, 0, '2019-12-28', 45468.00, 0.00, NULL, 45468.00, 45000.00, 468, 10, '', '', NULL, NULL, NULL),
(248, '820964', 2, 0, 199, '2019-12-28', 5040.00, 0.00, NULL, 5040.00, 0.00, 5040, 10, '', '', NULL, NULL, NULL),
(249, '813092', 2, 0, 200, '2019-12-28', 29340.00, 0.00, NULL, 29340.00, 0.00, 29340, 10, '', '', NULL, NULL, NULL),
(250, '325168', 2, 0, 200, '2019-12-28', 24525.00, 0.00, NULL, 24525.00, 0.00, 24525, 10, '', '', NULL, NULL, NULL),
(251, '362874', 2, 0, 200, '2019-12-28', 25875.00, 0.00, NULL, 25875.00, 0.00, 25875, 10, '', '', NULL, NULL, NULL),
(252, '850694', 3, 0, 201, '2019-12-29', 324.00, 50.00, NULL, 374.00, 0.00, 374, 9, '', '', NULL, NULL, NULL),
(253, '869715', 3, 0, 203, '2019-12-28', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(254, '193607', 3, 0, 204, '2019-12-29', 1955.00, 0.00, NULL, 1955.00, 0.00, 1955, 9, '', '', NULL, NULL, NULL),
(255, '370621', 4, 0, 205, '2019-12-06', 20400.00, 0.00, NULL, 20400.00, 20400.00, 0, 10, '', '', NULL, NULL, NULL),
(256, '742359', 4, 0, 6, '2019-12-05', 18000.00, 0.00, NULL, 18000.00, 18000.00, 0, 10, '', '', NULL, NULL, NULL),
(258, '534618', 3, 0, 207, '2019-12-29', 1955.00, 150.00, NULL, 2105.00, 0.00, 2105, 9, '', '', NULL, NULL, NULL),
(259, '706142', 3, 0, 208, '2019-12-29', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(260, '579128', 3, 0, 209, '2019-12-29', 1700.00, 0.00, NULL, 1700.00, 0.00, 1700, 9, '', '', NULL, NULL, NULL),
(261, '396542', 4, 0, 210, '2019-12-29', 7500.00, 0.00, NULL, 7500.00, 0.00, 7500, 10, '', '', NULL, NULL, NULL),
(262, '270356', 3, 0, 176, '2019-12-30', 3988.00, 0.00, NULL, 3988.00, 0.00, 3988, 9, '', '', NULL, NULL, NULL),
(264, '526810', 3, 0, 25, '2019-12-30', 1530.00, 0.00, NULL, 1530.00, 0.00, 1530, 9, '', '', NULL, NULL, NULL),
(265, '394751', 3, 0, 211, '2019-12-30', 3160.00, 0.00, NULL, 3160.00, 0.00, 3160, 9, '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(26) DEFAULT NULL,
  `color` varchar(26) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_name`, `color`) VALUES
(1, 'Pending', ' #f5eb98 '),
(2, 'Processing', '#f4d681'),
(3, 'Delivered', '#62f767'),
(4, 'Cancel', '#f73f69');

-- --------------------------------------------------------

--
-- Table structure for table `stockposting`
--

CREATE TABLE `stockposting` (
  `serialNumber` int(50) NOT NULL,
  `purchaseMasterId` int(11) DEFAULT NULL,
  `disburseMasterId` int(11) DEFAULT NULL,
  `purchaseInvoiceNo` varchar(50) DEFAULT NULL,
  `disburseInvoiceNo` varchar(36) DEFAULT NULL,
  `productBatchId` varchar(50) DEFAULT NULL,
  `inwardQuantity` decimal(18,2) DEFAULT NULL,
  `outwardQuantity` decimal(18,2) DEFAULT NULL,
  `unitId` varchar(50) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `destination_type` int(4) DEFAULT NULL,
  `destinationId` int(50) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `stock_type` tinyint(2) DEFAULT NULL,
  `discountrate` decimal(18,2) DEFAULT NULL,
  `discounts` varchar(26) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockposting`
--

INSERT INTO `stockposting` (`serialNumber`, `purchaseMasterId`, `disburseMasterId`, `purchaseInvoiceNo`, `disburseInvoiceNo`, `productBatchId`, `inwardQuantity`, `outwardQuantity`, `unitId`, `rate`, `destination_type`, `destinationId`, `customerId`, `date`, `stock_type`, `discountrate`, `discounts`) VALUES
(1, 1, NULL, '01', NULL, '2', 12000.00, NULL, '1', 76.00, NULL, 1, NULL, '2019-12-08 05:06:55', 1, NULL, NULL),
(3, NULL, 1, NULL, '2547830', '2', 0.00, 300.00, '1', 85.00, 4, 0, 1, '2019-12-08 05:18:44', 2, NULL, NULL),
(4, NULL, 2, NULL, '0627189', '2', 0.00, 750.00, '1', 64.36, 4, 0, 3, '2019-12-08 05:46:19', 2, NULL, NULL),
(6, 5, NULL, '02', NULL, '17', 318.00, NULL, '1', 1350.00, NULL, 10, NULL, '2019-12-09 02:25:36', 1, NULL, NULL),
(7, 6, NULL, '03', NULL, '11', 656.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-09 02:28:48', 1, NULL, NULL),
(8, 7, NULL, '04', NULL, '18', 666.00, NULL, '1', 1350.00, NULL, 10, NULL, '2019-12-09 02:37:45', 1, NULL, NULL),
(9, 8, NULL, '05', NULL, '19', 666.00, NULL, '1', 1350.00, NULL, 10, NULL, '2019-12-09 02:39:31', 1, NULL, NULL),
(10, 9, NULL, '05', NULL, '22', 678.00, NULL, '1', 1530.00, NULL, 10, NULL, '2019-12-09 02:42:38', 1, NULL, NULL),
(11, 10, NULL, '06', NULL, '23', 800.00, NULL, '1', 1530.00, NULL, 10, NULL, '2019-12-09 02:44:02', 1, NULL, NULL),
(12, 11, NULL, '07', NULL, '21', 672.00, NULL, '1', 1530.00, NULL, 10, NULL, '2019-12-09 02:47:22', 1, NULL, NULL),
(13, 12, NULL, 'Khulna', NULL, '10', 14.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-09 02:51:11', 1, NULL, NULL),
(14, 13, NULL, 'Khulna', NULL, '13', 47.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-09 02:59:02', 1, NULL, NULL),
(15, 14, NULL, 'Khulna', NULL, '7', 108.00, NULL, '1', 117.00, NULL, 10, NULL, '2019-12-09 03:02:49', 1, NULL, NULL),
(16, 15, NULL, 'Khulna', NULL, '8', 108.00, NULL, '1', 122.00, NULL, 10, NULL, '2019-12-09 03:05:21', 1, NULL, NULL),
(17, 16, NULL, 'Khulna', NULL, '5', 144.00, NULL, '1', 108.00, NULL, 10, NULL, '2019-12-09 03:07:12', 1, NULL, NULL),
(18, 17, NULL, '10', NULL, '20', 672.00, NULL, '1', 1530.00, NULL, 10, NULL, '2019-12-09 03:18:55', 1, NULL, NULL),
(19, NULL, 3, NULL, '1293804', '2', 0.00, 240.00, '1', 76.50, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(20, NULL, 3, NULL, '1293804', '11', 0.00, 64.00, '1', 477.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(21, NULL, 3, NULL, '1293804', '17', 0.00, 24.00, '1', 1350.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(22, NULL, 3, NULL, '1293804', '18', 0.00, 30.00, '1', 1350.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(23, NULL, 3, NULL, '1293804', '19', 0.00, 30.00, '1', 1350.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(24, NULL, 3, NULL, '1293804', '20', 0.00, 30.00, '1', 1530.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(25, NULL, 3, NULL, '1293804', '21', 0.00, 36.00, '1', 1530.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(26, NULL, 3, NULL, '1293804', '22', 0.00, 42.00, '1', 1530.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(27, NULL, 3, NULL, '1293804', '23', 0.00, 48.00, '1', 1530.00, 5, 14, 0, '2019-12-09 04:57:03', 2, NULL, NULL),
(28, 18, NULL, '12', NULL, '1', 22752.00, NULL, '1', 126.00, NULL, 10, NULL, '2019-12-11 12:36:06', 1, NULL, NULL),
(29, NULL, 4, NULL, '486290', '1', 0.00, 168.00, '1', 126.00, 5, 16, 0, '2019-12-11 12:43:55', 2, NULL, NULL),
(30, NULL, 5, NULL, '396025', '1', 0.00, 1200.00, '1', 126.00, 5, 17, 0, '2019-12-11 02:22:01', 2, NULL, NULL),
(31, NULL, 6, NULL, '625704', '2', 0.00, 480.00, '1', 76.50, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(32, NULL, 6, NULL, '625704', '11', 0.00, 80.00, '1', 477.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(33, NULL, 6, NULL, '625704', '17', 0.00, 60.00, '1', 1350.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(34, NULL, 6, NULL, '625704', '18', 0.00, 60.00, '1', 1350.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(35, NULL, 6, NULL, '625704', '19', 0.00, 60.00, '1', 1350.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(36, NULL, 6, NULL, '625704', '20', 0.00, 60.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(37, NULL, 6, NULL, '625704', '21', 0.00, 60.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(38, NULL, 6, NULL, '625704', '22', 0.00, 60.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(39, NULL, 6, NULL, '625704', '23', 0.00, 80.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:02:25', 2, NULL, NULL),
(40, NULL, 7, NULL, '692570', '21', 0.00, 6.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:05:08', 2, NULL, NULL),
(41, NULL, 7, NULL, '692570', '22', 0.00, 6.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:05:08', 2, NULL, NULL),
(42, NULL, 7, NULL, '692570', '23', 0.00, 8.00, '1', 1530.00, 5, 17, 0, '2019-12-11 03:05:08', 2, NULL, NULL),
(43, NULL, 8, NULL, '702516', '2', 0.00, 432.00, '1', 76.50, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(44, NULL, 8, NULL, '702516', '11', 0.00, 8.00, '1', 477.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(45, NULL, 8, NULL, '702516', '17', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(46, NULL, 8, NULL, '702516', '18', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(47, NULL, 8, NULL, '702516', '19', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(48, NULL, 8, NULL, '702516', '20', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(49, NULL, 8, NULL, '702516', '21', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(50, NULL, 8, NULL, '702516', '22', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(51, NULL, 8, NULL, '702516', '23', 0.00, 8.00, '1', 1530.00, 5, 16, 0, '2019-12-11 03:16:24', 2, NULL, NULL),
(52, NULL, 9, NULL, '704681', '11', 0.00, 24.00, '1', 477.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(53, NULL, 9, NULL, '704681', '17', 0.00, 12.00, '1', 1350.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(54, NULL, 9, NULL, '704681', '18', 0.00, 18.00, '1', 1350.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(55, NULL, 9, NULL, '704681', '19', 0.00, 18.00, '1', 1350.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(56, NULL, 9, NULL, '704681', '20', 0.00, 18.00, '1', 1530.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(57, NULL, 9, NULL, '704681', '21', 0.00, 18.00, '1', 1530.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(58, NULL, 9, NULL, '704681', '22', 0.00, 24.00, '1', 1530.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(59, NULL, 9, NULL, '704681', '23', 0.00, 24.00, '1', 1530.00, 5, 14, 0, '2019-12-11 03:35:06', 2, NULL, NULL),
(60, NULL, 10, NULL, '706895', '17', 0.00, 18.00, '1', 1350.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(61, NULL, 10, NULL, '706895', '18', 0.00, 24.00, '1', 1350.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(62, NULL, 10, NULL, '706895', '20', 0.00, 30.00, '1', 1530.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(63, NULL, 10, NULL, '706895', '21', 0.00, 30.00, '1', 1530.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(64, NULL, 10, NULL, '706895', '22', 0.00, 54.00, '1', 1530.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(65, NULL, 10, NULL, '706895', '23', 0.00, 48.00, '1', 1530.00, 5, 11, 0, '2019-12-11 03:40:21', 2, NULL, NULL),
(66, NULL, 11, NULL, '937612', '17', 0.00, 12.00, '1', 1350.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(67, NULL, 11, NULL, '937612', '18', 0.00, 12.00, '1', 1350.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(68, NULL, 11, NULL, '937612', '20', 0.00, 12.00, '1', 1530.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(69, NULL, 11, NULL, '937612', '21', 0.00, 12.00, '1', 1530.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(70, NULL, 11, NULL, '937612', '22', 0.00, 24.00, '1', 1530.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(71, NULL, 11, NULL, '937612', '23', 0.00, 24.00, '1', 1530.00, 5, 19, 0, '2019-12-11 03:54:00', 2, NULL, NULL),
(72, NULL, 12, NULL, '410975', '2', 0.00, 192.00, '1', 76.50, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(73, NULL, 12, NULL, '410975', '17', 0.00, 6.00, '1', 1350.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(74, NULL, 12, NULL, '410975', '18', 0.00, 6.00, '1', 1350.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(75, NULL, 12, NULL, '410975', '20', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(76, NULL, 12, NULL, '410975', '21', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(77, NULL, 12, NULL, '410975', '22', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(78, NULL, 12, NULL, '410975', '23', 0.00, 8.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:00:54', 2, NULL, NULL),
(79, NULL, 13, NULL, '418072', '17', 0.00, 6.00, '1', 1350.00, 5, 20, 0, '2019-12-11 04:05:28', 2, NULL, NULL),
(80, NULL, 13, NULL, '418072', '18', 0.00, 6.00, '1', 1350.00, 5, 20, 0, '2019-12-11 04:05:28', 2, NULL, NULL),
(81, NULL, 13, NULL, '418072', '20', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:05:28', 2, NULL, NULL),
(82, NULL, 13, NULL, '418072', '21', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:05:28', 2, NULL, NULL),
(83, NULL, 13, NULL, '418072', '22', 0.00, 6.00, '1', 1530.00, 5, 20, 0, '2019-12-11 04:05:28', 2, NULL, NULL),
(84, 19, NULL, 'Gulshan', NULL, '12', 224.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-11 05:02:58', 1, NULL, NULL),
(85, 20, NULL, 'Gulshan', NULL, '13', 400.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-11 05:04:09', 1, NULL, NULL),
(86, 21, NULL, 'Gulshan', NULL, '14', 136.00, NULL, '1', 540.00, NULL, 10, NULL, '2019-12-11 05:12:18', 1, NULL, NULL),
(87, NULL, 14, NULL, '960578', '1', 0.00, 720.00, '1', 126.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(88, NULL, 14, NULL, '960578', '11', 0.00, 32.00, '1', 477.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(89, NULL, 14, NULL, '960578', '17', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(90, NULL, 14, NULL, '960578', '18', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(91, NULL, 14, NULL, '960578', '19', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(92, NULL, 14, NULL, '960578', '20', 0.00, 6.00, '1', 1530.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(93, NULL, 14, NULL, '960578', '21', 0.00, 6.00, '1', 1530.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(94, NULL, 14, NULL, '960578', '22', 0.00, 6.00, '1', 1530.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(95, NULL, 14, NULL, '960578', '23', 0.00, 32.00, '1', 1530.00, 5, 11, 0, '2019-12-12 10:36:54', 2, NULL, NULL),
(96, NULL, 15, NULL, '428756', '1', 0.00, 408.00, '1', 126.00, 5, 19, 0, '2019-12-12 10:42:26', 2, NULL, NULL),
(97, NULL, 16, NULL, '652831', '1', 0.00, 1200.00, '1', 126.00, 5, 14, 0, '2019-12-12 12:02:08', 2, NULL, NULL),
(98, NULL, 17, NULL, '063524', '1', 0.00, 48.00, '1', 130.00, 4, 0, 6, '2019-12-12 12:09:56', 2, NULL, NULL),
(99, NULL, 18, NULL, '562083', '1', 0.00, 24.00, '1', 130.00, 4, 0, 5, '2019-12-12 12:26:42', 2, NULL, NULL),
(100, NULL, 19, NULL, '794582', '1', 0.00, 696.00, '1', 126.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(101, NULL, 19, NULL, '794582', '2', 0.00, 240.00, '1', 76.50, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(102, NULL, 19, NULL, '794582', '11', 0.00, 24.00, '1', 477.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(103, NULL, 19, NULL, '794582', '17', 0.00, 30.00, '1', 1350.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(104, NULL, 19, NULL, '794582', '18', 0.00, 30.00, '1', 1350.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(105, NULL, 19, NULL, '794582', '19', 0.00, 30.00, '1', 1350.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(106, NULL, 19, NULL, '794582', '20', 0.00, 30.00, '1', 1530.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(107, NULL, 19, NULL, '794582', '21', 0.00, 30.00, '1', 1530.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(108, NULL, 19, NULL, '794582', '22', 0.00, 48.00, '1', 1530.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(109, NULL, 19, NULL, '794582', '23', 0.00, 64.00, '1', 1530.00, 5, 21, 0, '2019-12-12 01:35:29', 2, NULL, NULL),
(110, NULL, 20, NULL, '259364', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 7, '2019-12-14 10:11:53', 2, NULL, NULL),
(111, NULL, 21, NULL, '804573', '11', 0.00, 1.00, '1', 700.00, 3, 0, 8, '2019-12-14 10:40:44', 2, NULL, NULL),
(112, NULL, 22, NULL, '650293', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 9, '2019-12-14 10:55:23', 2, NULL, NULL),
(113, NULL, 23, NULL, '610439', '11', 0.00, 2.00, '1', 700.00, 3, 0, 10, '2019-12-14 11:09:05', 2, NULL, NULL),
(114, NULL, 24, NULL, '528931', '13', 0.00, 5.00, '1', 700.00, 3, 0, 11, '2019-12-14 11:36:34', 2, NULL, NULL),
(115, 22, NULL, '11288', NULL, '3', 2.00, NULL, '1', 86.00, NULL, 10, NULL, '2019-12-14 01:18:29', 1, NULL, NULL),
(116, 23, NULL, '11288', NULL, '4', 860.00, NULL, '1', 99.00, NULL, 10, NULL, '2019-12-14 01:20:28', 1, NULL, NULL),
(117, 24, NULL, '11288', NULL, '5', 658.00, NULL, '1', 108.00, NULL, 10, NULL, '2019-12-14 01:21:57', 1, NULL, NULL),
(118, 25, NULL, '11288', NULL, '6', 23.00, NULL, '1', 113.00, NULL, 10, NULL, '2019-12-14 01:23:46', 1, NULL, NULL),
(119, 26, NULL, '11288', NULL, '7', 817.00, NULL, '1', 117.00, NULL, 10, NULL, '2019-12-14 01:25:03', 1, NULL, NULL),
(120, 27, NULL, '11288', NULL, '8', 758.00, NULL, '1', 122.00, NULL, 10, NULL, '2019-12-14 01:26:51', 1, NULL, NULL),
(121, 28, NULL, '11288', NULL, '13', 4.00, NULL, '1', 477.00, NULL, 10, NULL, '2019-12-14 01:28:03', 1, NULL, NULL),
(122, 29, NULL, '11288', NULL, '15', 3.00, NULL, '1', 540.00, NULL, 10, NULL, '2019-12-14 01:29:06', 1, NULL, NULL),
(123, NULL, 25, NULL, '753094', '1', 0.00, 4.00, '1', 230.00, 3, 0, 12, '2019-12-14 02:10:01', 2, NULL, NULL),
(124, NULL, 26, NULL, '764530', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 16, '2019-12-14 05:04:18', 2, NULL, NULL),
(125, NULL, 27, NULL, '418705', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 17, '2019-12-14 05:15:17', 2, NULL, NULL),
(126, NULL, 28, NULL, '436095', '1', 0.00, 1.00, '1', 230.00, 3, 0, 18, '2019-12-14 05:24:56', 2, NULL, NULL),
(127, NULL, 28, NULL, '436095', '10', 0.00, 1.00, '1', 700.00, 3, 0, 18, '2019-12-14 05:24:56', 2, NULL, NULL),
(128, NULL, 29, NULL, '690381', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 19, '2019-12-15 10:26:52', 2, NULL, NULL),
(129, NULL, 30, NULL, '328516', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 20, '2019-12-15 10:33:32', 2, NULL, NULL),
(130, NULL, 31, NULL, '871356', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 21, '2019-12-15 11:02:15', 2, NULL, NULL),
(131, NULL, 32, NULL, '240153', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 22, '2019-12-15 11:04:38', 2, NULL, NULL),
(132, NULL, 33, NULL, '863450', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 24, '2019-12-15 11:38:31', 2, NULL, NULL),
(133, NULL, 34, NULL, '106725', '1', 0.00, 24.00, '1', 161.00, 2, 0, 23, '2019-12-15 11:44:09', 2, NULL, NULL),
(134, NULL, 34, NULL, '106725', '18', 0.00, 6.00, '1', 1500.00, 2, 0, 23, '2019-12-15 11:44:09', 2, NULL, NULL),
(135, NULL, 34, NULL, '106725', '23', 0.00, 8.00, '1', 1725.00, 2, 0, 23, '2019-12-15 11:44:09', 2, NULL, NULL),
(136, NULL, 35, NULL, '720364', '20', 0.00, 5.00, '1', 1725.00, 2, 0, 23, '2019-12-15 11:45:37', 2, NULL, NULL),
(137, NULL, 35, NULL, '720364', '21', 0.00, 5.00, '1', 1725.00, 2, 0, 23, '2019-12-15 11:45:37', 2, NULL, NULL),
(138, NULL, 35, NULL, '720364', '22', 0.00, 6.00, '1', 1725.00, 2, 0, 23, '2019-12-15 11:45:37', 2, NULL, NULL),
(141, NULL, 38, NULL, '934271', '22', 0.00, 1.00, '1', 1530.00, 3, 0, 25, '2019-12-15 01:20:19', 2, NULL, NULL),
(142, NULL, 39, NULL, '802915', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 26, '2019-12-15 02:44:39', 2, NULL, NULL),
(143, NULL, 40, NULL, '179632', '1', 0.00, 5.00, '1', 230.00, 3, 0, 27, '2019-12-15 03:17:13', 2, NULL, NULL),
(144, NULL, 40, NULL, '179632', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 27, '2019-12-15 03:17:13', 2, NULL, NULL),
(145, NULL, 41, NULL, '945862', '1', 0.00, 2.00, '1', 230.00, 3, 0, 30, '2019-12-17 10:37:11', 2, 196.00, '15'),
(146, NULL, 42, NULL, '975682', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 28, '2019-12-17 10:50:51', 2, 1700.00, '15'),
(147, NULL, 43, NULL, '958210', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 29, '2019-12-17 10:52:29', 2, 1955.00, '15'),
(148, NULL, 44, NULL, '403957', '21', 0.00, 3.00, '1', 2300.00, 3, 0, 31, '2019-12-17 10:57:03', 2, 1955.00, '15'),
(149, NULL, 44, NULL, '403957', '22', 0.00, 3.00, '1', 2300.00, 3, 0, 31, '2019-12-17 10:57:03', 2, 1955.00, '15'),
(150, NULL, 44, NULL, '403957', '1', 0.00, 1.00, '1', 230.00, 3, 0, 31, '2019-12-17 10:57:03', 2, 0.00, '100'),
(151, NULL, 45, NULL, '631927', '1', 0.00, 1.00, '1', 230.00, 3, 0, 32, '2019-12-17 11:07:29', 2, 196.00, '15'),
(152, NULL, 45, NULL, '631927', '4', 0.00, 1.00, '1', 155.00, 3, 0, 32, '2019-12-17 11:07:29', 2, 132.00, '15'),
(153, NULL, 45, NULL, '631927', '6', 0.00, 1.00, '1', 180.00, 3, 0, 32, '2019-12-17 11:07:29', 2, 153.00, '15'),
(154, NULL, 46, NULL, '453021', '10', 0.00, 1.00, '1', 700.00, 3, 0, 33, '2019-12-17 11:12:34', 2, 595.00, '15'),
(155, NULL, 46, NULL, '453021', '1', 0.00, 1.00, '1', 230.00, 3, 0, 33, '2019-12-17 11:12:34', 2, 196.00, '15'),
(156, NULL, 47, NULL, '407589', '1', 0.00, 1.00, '1', 230.00, 3, 0, 34, '2019-12-17 11:16:35', 2, 196.00, '15'),
(157, NULL, 47, NULL, '407589', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 34, '2019-12-17 11:16:35', 2, 1700.00, '15'),
(158, NULL, 48, NULL, '706295', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 35, '2019-12-17 11:19:59', 2, 1955.00, '15'),
(159, NULL, 49, NULL, '198723', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 36, '2019-12-17 02:02:42', 2, 1955.00, '15'),
(160, NULL, 50, NULL, '547903', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 37, '2019-12-17 02:14:24', 2, 1955.00, '15'),
(162, NULL, 52, NULL, '837602', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 38, '2019-12-17 02:30:57', 2, 1955.00, '15'),
(163, NULL, 53, NULL, '670159', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 40, '2019-12-17 02:37:15', 2, 1700.00, '15'),
(165, NULL, 55, NULL, '076943', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 42, '2019-12-17 02:50:13', 2, 1955.00, '15'),
(166, NULL, 56, NULL, '239465', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 44, '2019-12-17 02:55:59', 2, 1700.00, '15'),
(167, NULL, 57, NULL, '295680', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 45, '2019-12-17 03:04:05', 2, 1955.00, '15'),
(168, NULL, 58, NULL, '182579', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 46, '2019-12-17 03:07:19', 2, 1700.00, '15'),
(169, NULL, 59, NULL, '852714', '23', 0.00, 2.00, '1', 2300.00, 3, 0, 47, '2019-12-17 03:15:42', 2, 1955.00, '15'),
(170, NULL, 60, NULL, '957320', '1', 0.00, 192.00, '1', 126.00, 5, 20, 0, '2019-12-17 03:25:12', 2, 126.00, '0'),
(171, NULL, 61, NULL, '053698', '1', 0.00, 480.00, '1', 126.00, 5, 11, 0, '2019-12-17 03:28:44', 2, 126.00, '0'),
(172, NULL, 61, NULL, '053698', '22', 0.00, 12.00, '1', 1530.00, 5, 11, 0, '2019-12-17 03:28:44', 2, 1530.00, '0'),
(173, NULL, 61, NULL, '053698', '23', 0.00, 16.00, '1', 1530.00, 5, 11, 0, '2019-12-17 03:28:44', 2, 1530.00, '0'),
(174, NULL, 62, NULL, '893427', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 48, '2019-12-17 03:34:11', 2, 1700.00, '15'),
(175, NULL, 63, NULL, '471258', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 49, '2019-12-17 03:38:27', 2, 1700.00, '15'),
(176, NULL, 63, NULL, '471258', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 49, '2019-12-17 03:38:27', 2, 1700.00, '15'),
(177, NULL, 64, NULL, '431267', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 50, '2019-12-17 03:42:30', 2, 1955.00, '15'),
(178, NULL, 65, NULL, '356198', '23', 0.00, 2.00, '1', 2300.00, 3, 0, 51, '2019-12-17 03:44:58', 2, 1955.00, '15'),
(179, NULL, 66, NULL, '210384', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 52, '2019-12-17 03:47:55', 2, 1700.00, '15'),
(180, NULL, 67, NULL, '725168', '18', 0.00, 2.00, '1', 2000.00, 3, 0, 53, '2019-12-17 03:51:29', 2, 1700.00, '15'),
(181, NULL, 68, NULL, '326594', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 54, '2019-12-17 03:56:24', 2, 1955.00, '15'),
(182, NULL, 69, NULL, '193084', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 55, '2019-12-17 03:59:27', 2, 1955.00, '15'),
(183, NULL, 70, NULL, '421396', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 19, '2019-12-17 04:03:51', 2, 1700.00, '15'),
(184, NULL, 71, NULL, '495870', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 57, '2019-12-17 04:06:02', 2, 1955.00, '15'),
(185, NULL, 72, NULL, '746859', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 58, '2019-12-17 04:08:59', 2, 1955.00, '15'),
(186, NULL, 73, NULL, '796425', '2', 0.00, 10.00, '1', 130.00, 3, 0, 59, '2019-12-17 04:15:00', 2, 111.00, '15'),
(187, NULL, 74, NULL, '923150', '2', 0.00, 10.00, '1', 130.00, 3, 0, 61, '2019-12-17 04:18:57', 2, 111.00, '15'),
(188, NULL, 75, NULL, '925384', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 28, '2019-12-17 04:26:05', 2, 1700.00, '15'),
(189, NULL, 76, NULL, '358706', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 63, '2019-12-17 04:28:31', 2, 1700.00, '15'),
(190, NULL, 77, NULL, '489632', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 64, '2019-12-17 04:32:33', 2, 1955.00, '15'),
(191, NULL, 78, NULL, '750641', '1', 0.00, 120.00, '1', 126.00, 5, 29, 0, '2019-12-17 04:38:59', 2, 119.00, '5.6'),
(192, NULL, 78, NULL, '750641', '11', 0.00, 8.00, '1', 477.00, 5, 29, 0, '2019-12-17 04:38:59', 2, 450.00, '5.7'),
(193, NULL, 78, NULL, '750641', '13', 0.00, 8.00, '1', 477.00, 5, 29, 0, '2019-12-17 04:38:59', 2, 450.00, '5.7'),
(194, NULL, 78, NULL, '750641', '18', 0.00, 6.00, '1', 1350.00, 5, 29, 0, '2019-12-17 04:38:59', 2, 1275.00, '5.6'),
(195, NULL, 79, NULL, '231970', '19', 0.00, 3.00, '1', 2000.00, 3, 0, 65, '2019-12-17 04:57:42', 2, 1700.00, '15'),
(196, NULL, 80, NULL, '137824', '2', 0.00, 8.00, '1', 130.00, 3, 0, 66, '2019-12-17 05:04:18', 2, 111.00, '15'),
(197, NULL, 81, NULL, '180352', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 67, '2019-12-17 05:10:08', 2, 1955.00, '15'),
(198, NULL, 82, NULL, '278309', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 69, '2019-12-17 05:50:22', 2, 1700.00, '15'),
(199, NULL, 83, NULL, '914203', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 70, '2019-12-17 05:58:34', 2, 1700.00, '15'),
(200, NULL, 84, NULL, '450916', '20', 0.00, 2.00, '1', 2300.00, 3, 0, 72, '2019-12-17 06:01:26', 2, 1955.00, '15'),
(201, NULL, 85, NULL, '825016', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 73, '2019-12-17 06:03:51', 2, 1700.00, '15'),
(202, NULL, 86, NULL, '742190', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 74, '2019-12-17 06:06:09', 2, 1700.00, '15'),
(203, NULL, 87, NULL, '234716', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 75, '2019-12-17 06:09:23', 2, 1955.00, '15'),
(204, NULL, 88, NULL, '120873', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 76, '2019-12-17 06:13:06', 2, 1700.00, '15'),
(205, NULL, 89, NULL, '768539', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 77, '2019-12-17 06:19:03', 2, 1700.00, '15'),
(206, NULL, 90, NULL, '748310', '23', 0.00, 2.00, '1', 2300.00, 3, 0, 51, '2019-12-17 06:38:56', 2, 1955.00, '15'),
(207, NULL, 91, NULL, '965720', '13', 0.00, 1.00, '1', 700.00, 3, 0, 78, '2019-12-17 06:46:55', 2, 595.00, '15'),
(208, NULL, 91, NULL, '965720', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 78, '2019-12-17 06:46:55', 2, 1700.00, '15'),
(209, NULL, 92, NULL, '021539', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 79, '2019-12-18 10:24:41', 2, 1955.00, '15'),
(210, NULL, 93, NULL, '917523', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 81, '2019-12-18 10:36:39', 2, 1955.00, '15'),
(216, NULL, 95, NULL, '954362', '7', 0.00, 36.00, '1', 117.00, 5, 14, 0, '2019-12-18 11:29:51', 2, 117.00, '0'),
(217, NULL, 95, NULL, '954362', '8', 0.00, 36.00, '1', 122.00, 5, 14, 0, '2019-12-18 11:29:51', 2, 122.00, '0'),
(218, NULL, 95, NULL, '954362', '12', 0.00, 40.00, '1', 477.00, 5, 14, 0, '2019-12-18 11:29:51', 2, 477.00, '0'),
(219, NULL, 95, NULL, '954362', '14', 0.00, 40.00, '1', 540.00, 5, 14, 0, '2019-12-18 11:29:51', 2, 540.00, '0'),
(220, NULL, 95, NULL, '954362', '18', 0.00, 12.00, '1', 1350.00, 5, 14, 0, '2019-12-18 11:29:51', 2, 1350.00, '0'),
(221, NULL, 96, NULL, '127965', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 82, '2019-12-18 12:34:48', 2, 1700.00, '26.1'),
(222, NULL, 97, NULL, '859071', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 83, '2019-12-18 12:43:06', 2, 1955.00, '15'),
(223, NULL, 98, NULL, '035964', '1', 0.00, 1200.00, '1', 126.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 126.00, '0'),
(224, NULL, 98, NULL, '035964', '18', 0.00, 30.00, '1', 1350.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 1350.00, '0'),
(225, NULL, 98, NULL, '035964', '19', 0.00, 18.00, '1', 1350.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 1350.00, '0'),
(226, NULL, 98, NULL, '035964', '20', 0.00, 24.00, '1', 1530.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 1530.00, '0'),
(227, NULL, 98, NULL, '035964', '21', 0.00, 30.00, '1', 1530.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 1530.00, '0'),
(228, NULL, 98, NULL, '035964', '22', 0.00, 24.00, '1', 1530.00, 5, 17, 0, '2019-12-18 01:22:56', 2, 1530.00, '0'),
(229, NULL, 99, NULL, '764859', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 84, '2019-12-18 02:11:33', 2, 1955.00, '15'),
(230, NULL, 100, NULL, '912387', '2', 0.00, 1.00, '1', 130.00, 3, 0, 85, '2019-12-18 02:22:20', 2, 111.00, '15'),
(231, NULL, 101, NULL, '914758', '1', 0.00, 10.00, '1', 230.00, 3, 0, 86, '2019-12-18 02:33:43', 2, 196.00, '15'),
(232, NULL, 101, NULL, '914758', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 86, '2019-12-18 02:33:43', 2, 1955.00, '15'),
(233, NULL, 102, NULL, '258037', '1', 0.00, 120.00, '1', 126.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 120.00, '5'),
(234, NULL, 102, NULL, '258037', '23', 0.00, 8.00, '1', 1530.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 1454.00, '5'),
(235, NULL, 102, NULL, '258037', '22', 0.00, 6.00, '1', 1530.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 1454.00, '5'),
(236, NULL, 102, NULL, '258037', '21', 0.00, 6.00, '1', 1530.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 1454.00, '5'),
(237, NULL, 102, NULL, '258037', '20', 0.00, 6.00, '1', 1530.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 1454.00, '5'),
(238, NULL, 102, NULL, '258037', '11', 0.00, 8.00, '1', 477.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 453.00, '5'),
(239, NULL, 102, NULL, '258037', '14', 0.00, 8.00, '1', 540.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 513.00, '5'),
(240, NULL, 102, NULL, '258037', '8', 0.00, 36.00, '1', 122.00, 5, 30, 0, '2019-12-18 03:13:51', 2, 116.00, '5'),
(241, NULL, 103, NULL, '634902', '13', 0.00, 6.00, '1', 700.00, 3, 0, 87, '2019-12-18 03:22:00', 2, 477.00, '31.9'),
(242, NULL, 104, NULL, '419237', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 88, '2019-12-19 11:04:19', 2, 1700.00, '15'),
(243, NULL, 105, NULL, '069153', '1', 0.00, 3.00, '1', 230.00, 3, 0, 89, '2019-12-19 11:08:02', 2, 196.00, '15'),
(244, NULL, 106, NULL, '418267', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 90, '2019-12-19 11:12:26', 2, 1955.00, '15'),
(245, NULL, 107, NULL, '075124', '21', 0.00, 2.00, '1', 2300.00, 3, 0, 57, '2019-12-19 11:30:55', 2, 1955.00, '15'),
(246, NULL, 108, NULL, '742018', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 92, '2019-12-19 11:34:34', 2, 1700.00, '15'),
(247, NULL, 109, NULL, '927154', '11', 0.00, 1.00, '1', 700.00, 3, 0, 93, '2019-12-19 12:10:41', 2, 595.00, '15'),
(248, NULL, 109, NULL, '927154', '15', 0.00, 1.00, '1', 800.00, 3, 0, 93, '2019-12-19 12:10:41', 2, 680.00, '15'),
(249, NULL, 110, NULL, '473109', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 32, '2019-12-19 12:44:12', 2, 1955.00, '15'),
(250, NULL, 111, NULL, '960278', '15', 0.00, 1.00, '1', 800.00, 3, 0, 95, '2019-12-19 01:38:12', 2, 680.00, '15'),
(251, NULL, 112, NULL, '806713', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 96, '2019-12-19 02:14:09', 2, 1955.00, '15'),
(252, NULL, 113, NULL, '593402', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 97, '2019-12-19 02:16:30', 2, 1955.00, '15'),
(253, NULL, 114, NULL, '182695', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 98, '2019-12-19 02:59:27', 2, 1955.00, '15'),
(254, NULL, 115, NULL, '753049', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 2, '2019-12-19 03:05:03', 2, 1700.00, '15'),
(255, NULL, 116, NULL, '852610', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 99, '2019-12-19 03:07:51', 2, 1700.00, '15'),
(256, NULL, 117, NULL, '051629', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 100, '2019-12-19 03:10:28', 2, 1955.00, '15'),
(257, NULL, 118, NULL, '284539', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 101, '2019-12-19 03:14:07', 2, 1700.00, '15'),
(258, NULL, 119, NULL, '850634', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 41, '2019-12-19 03:17:21', 2, 1955.00, '15'),
(259, NULL, 120, NULL, '361095', '19', 0.00, 3.00, '1', 2000.00, 3, 0, 4, '2019-12-19 03:18:26', 2, 1700.00, '15'),
(260, NULL, 121, NULL, '780952', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 102, '2019-12-19 03:27:53', 2, 1700.00, '15'),
(261, NULL, 122, NULL, '064972', '7', 0.00, 5.00, '1', 185.00, 3, 0, 103, '2019-12-19 03:42:42', 2, 157.00, '15'),
(262, NULL, 123, NULL, '326910', '10', 0.00, 1.00, '1', 700.00, 3, 0, 104, '2019-12-19 03:46:04', 2, 595.00, '15'),
(263, NULL, 124, NULL, '081695', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 105, '2019-12-19 03:48:51', 2, 1700.00, '15'),
(264, NULL, 125, NULL, '260784', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 106, '2019-12-19 03:57:37', 2, 1700.00, '15'),
(265, NULL, 126, NULL, '275603', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 107, '2019-12-19 04:00:13', 2, 1955.00, '15'),
(266, NULL, 127, NULL, '720685', '1', 0.00, 10.00, '1', 230.00, 3, 0, 108, '2019-12-19 04:11:44', 2, 195.00, '15.2'),
(267, NULL, 128, NULL, '428371', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 109, '2019-12-19 04:16:36', 2, 1955.00, '15'),
(268, NULL, 129, NULL, '408951', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 110, '2019-12-19 04:22:28', 2, 1700.00, '15'),
(269, NULL, 130, NULL, '073912', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 111, '2019-12-19 04:25:26', 2, 1700.00, '15'),
(270, NULL, 131, NULL, '150437', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 112, '2019-12-19 04:32:26', 2, 1955.00, '15'),
(271, NULL, 132, NULL, '906251', '1', 0.00, 10.00, '1', 230.00, 3, 0, 113, '2019-12-19 05:11:57', 2, 185.00, '19.6'),
(272, NULL, 133, NULL, '521690', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 114, '2019-12-19 05:16:25', 2, 1700.00, '15'),
(273, NULL, 134, NULL, '519203', '1', 0.00, 5.00, '1', 230.00, 3, 0, 104, '2019-12-19 05:19:28', 2, 195.00, '15.2'),
(274, NULL, 135, NULL, '741602', '1', 0.00, 10.00, '1', 230.00, 3, 0, 116, '2019-12-19 05:28:10', 2, 195.00, '15.2'),
(275, NULL, 135, NULL, '741602', '10', 0.00, 3.00, '1', 700.00, 3, 0, 116, '2019-12-19 05:28:10', 2, 595.00, '15'),
(276, NULL, 136, NULL, '013967', '10', 0.00, 4.00, '1', 700.00, 3, 0, 117, '2019-12-19 05:30:21', 2, 595.00, '15'),
(277, NULL, 137, NULL, '867095', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 118, '2019-12-19 05:54:20', 2, 1955.00, '15'),
(278, NULL, 138, NULL, '960145', '1', 0.00, 4.00, '1', 230.00, 3, 0, 119, '2019-12-21 10:23:03', 2, 196.00, '15'),
(279, NULL, 138, NULL, '960145', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 119, '2019-12-21 10:23:03', 2, 1700.00, '15'),
(280, NULL, 138, NULL, '960145', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 119, '2019-12-21 10:23:03', 2, 1700.00, '15'),
(281, NULL, 139, NULL, '815904', '22', 0.00, 2.00, '1', 2300.00, 3, 0, 120, '2019-12-21 10:28:20', 2, 1955.00, '15'),
(282, NULL, 140, NULL, '529160', '1', 0.00, 1.00, '1', 230.00, 3, 0, 121, '2019-12-21 10:38:16', 2, 196.00, '15'),
(283, NULL, 140, NULL, '529160', '13', 0.00, 2.00, '1', 700.00, 3, 0, 121, '2019-12-21 10:38:16', 2, 595.00, '15'),
(284, NULL, 140, NULL, '529160', '15', 0.00, 1.00, '1', 800.00, 3, 0, 121, '2019-12-21 10:38:16', 2, 680.00, '15'),
(285, NULL, 141, NULL, '279864', '14', 0.00, 1.00, '1', 800.00, 3, 0, 123, '2019-12-21 10:40:27', 2, 680.00, '15'),
(286, NULL, 142, NULL, '527648', '12', 0.00, 2.00, '1', 700.00, 3, 0, 124, '2019-12-21 10:44:40', 2, 595.00, '15'),
(287, NULL, 143, NULL, '960412', '1', 0.00, 54.00, '1', 140.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 140.00, '0'),
(288, NULL, 143, NULL, '960412', '13', 0.00, 56.00, '1', 530.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 530.00, '0'),
(289, NULL, 143, NULL, '960412', '12', 0.00, 56.00, '1', 530.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 530.00, '0'),
(290, NULL, 143, NULL, '960412', '14', 0.00, 16.00, '1', 600.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 600.00, '0'),
(291, NULL, 143, NULL, '960412', '11', 0.00, 16.00, '1', 530.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 530.00, '0'),
(292, NULL, 143, NULL, '960412', '17', 0.00, 6.00, '1', 1500.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1500.00, '0'),
(293, NULL, 143, NULL, '960412', '18', 0.00, 6.00, '1', 1500.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1500.00, '0'),
(294, NULL, 143, NULL, '960412', '19', 0.00, 6.00, '1', 1500.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1500.00, '0'),
(295, NULL, 143, NULL, '960412', '20', 0.00, 6.00, '1', 1700.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1700.00, '0'),
(296, NULL, 143, NULL, '960412', '21', 0.00, 6.00, '1', 1700.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1700.00, '0'),
(297, NULL, 143, NULL, '960412', '22', 0.00, 6.00, '1', 1700.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1700.00, '0'),
(298, NULL, 143, NULL, '960412', '23', 0.00, 6.00, '1', 1700.00, 1, 0, 125, '2019-12-21 10:59:26', 2, 1700.00, '0'),
(299, NULL, 144, NULL, '097583', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 126, '2019-12-21 11:09:21', 2, 1955.00, '15'),
(300, NULL, 145, NULL, '376580', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 127, '2019-12-21 11:18:54', 2, 1700.00, '15'),
(301, NULL, 146, NULL, '509413', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 128, '2019-12-21 11:21:41', 2, 1700.00, '15'),
(303, NULL, 148, NULL, '105387', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 130, '2019-12-21 11:25:40', 2, 1700.00, '15'),
(304, NULL, 149, NULL, '564231', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 131, '2019-12-21 11:27:21', 2, 1955.00, '15'),
(305, NULL, 150, NULL, '915407', '1', 0.00, 2.00, '1', 230.00, 3, 0, 132, '2019-12-21 11:44:04', 2, 196.00, '15'),
(306, NULL, 150, NULL, '915407', '10', 0.00, 1.00, '1', 700.00, 3, 0, 132, '2019-12-21 11:44:04', 2, 595.00, '15'),
(307, NULL, 151, NULL, '901852', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 109, '2019-12-21 11:51:35', 2, 1955.00, '15'),
(308, NULL, 152, NULL, '976318', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 133, '2019-12-21 12:02:22', 2, 1700.00, '15'),
(309, NULL, 153, NULL, '038274', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 63, '2019-12-21 12:17:46', 2, 1955.00, '15'),
(310, NULL, 154, NULL, '953824', '23', 0.00, 3.00, '1', 2300.00, 3, 0, 135, '2019-12-21 12:29:09', 2, 1955.00, '15'),
(311, NULL, 155, NULL, '198075', '1', 0.00, 96.00, '1', 126.00, 5, 16, 0, '2019-12-21 12:30:35', 2, 126.00, '0'),
(312, NULL, 155, NULL, '198075', '20', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-21 12:30:35', 2, 1530.00, '0'),
(313, NULL, 155, NULL, '198075', '21', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-21 12:30:35', 2, 1530.00, '0'),
(314, NULL, 155, NULL, '198075', '22', 0.00, 6.00, '1', 1530.00, 5, 16, 0, '2019-12-21 12:30:35', 2, 1530.00, '0'),
(315, NULL, 156, NULL, '734085', '1', 0.00, 1.00, '1', 230.00, 3, 0, 136, '2019-12-21 12:40:20', 2, 196.00, '15'),
(316, NULL, 156, NULL, '734085', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 136, '2019-12-21 12:40:20', 2, 1955.00, '15'),
(317, NULL, 157, NULL, '589326', '18', 0.00, 6.00, '1', 2000.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1560.00, '22'),
(318, NULL, 157, NULL, '589326', '19', 0.00, 6.00, '1', 2000.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1560.00, '22'),
(319, NULL, 157, NULL, '589326', '20', 0.00, 6.00, '1', 2300.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1794.00, '22'),
(320, NULL, 157, NULL, '589326', '21', 0.00, 6.00, '1', 2300.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1794.00, '22'),
(321, NULL, 157, NULL, '589326', '22', 0.00, 6.00, '1', 2300.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1794.00, '22'),
(322, NULL, 157, NULL, '589326', '23', 0.00, 6.00, '1', 2300.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 1794.00, '22'),
(323, NULL, 157, NULL, '589326', '1', 0.00, 48.00, '1', 230.00, 2, 0, 137, '2019-12-21 01:10:12', 2, 179.00, '22'),
(332, NULL, 159, NULL, '206758', '14', 0.00, 1.00, '1', 800.00, 3, 0, 138, '2019-12-21 01:22:47', 2, 680.00, '15'),
(333, NULL, 159, NULL, '206758', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 138, '2019-12-21 01:22:47', 2, 1955.00, '15'),
(334, NULL, 160, NULL, '687429', '14', 0.00, 2.00, '1', 800.00, 3, 0, 141, '2019-12-21 01:36:09', 2, 680.00, '15'),
(335, NULL, 161, NULL, '826540', '1', 0.00, 12.00, '1', 230.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 179.00, '22'),
(336, NULL, 161, NULL, '826540', '17', 0.00, 6.00, '1', 2000.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1560.00, '22'),
(337, NULL, 161, NULL, '826540', '18', 0.00, 6.00, '1', 2000.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1560.00, '22'),
(338, NULL, 161, NULL, '826540', '19', 0.00, 12.00, '1', 2000.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1560.00, '22'),
(339, NULL, 161, NULL, '826540', '20', 0.00, 4.00, '1', 2300.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1794.00, '22'),
(340, NULL, 161, NULL, '826540', '21', 0.00, 4.00, '1', 2300.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1794.00, '22'),
(341, NULL, 161, NULL, '826540', '22', 0.00, 4.00, '1', 2300.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1794.00, '22'),
(342, NULL, 161, NULL, '826540', '23', 0.00, 4.00, '1', 2300.00, 2, 0, 139, '2019-12-21 01:41:41', 2, 1794.00, '22'),
(343, NULL, 162, NULL, '624315', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 142, '2019-12-21 01:57:03', 2, 1700.00, '15'),
(344, NULL, 163, NULL, '058473', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 143, '2019-12-21 02:45:42', 2, 1700.00, '15'),
(345, NULL, 163, NULL, '058473', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 143, '2019-12-21 02:45:42', 2, 1955.00, '15'),
(346, NULL, 164, NULL, '592438', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 144, '2019-12-21 03:45:45', 2, 1955.00, '15'),
(347, NULL, 165, NULL, '586127', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 131, '2019-12-22 10:36:53', 2, 1955.00, '15'),
(348, NULL, 166, NULL, '403587', '17', 0.00, 2.00, '1', 2000.00, 3, 0, 146, '2019-12-22 10:41:26', 2, 1700.00, '15'),
(350, NULL, 168, NULL, '679538', '11', 0.00, 16.00, '1', 477.00, 5, 20, 0, '2019-12-22 10:50:56', 2, 477.00, '0'),
(351, NULL, 168, NULL, '679538', '12', 0.00, 16.00, '1', 477.00, 5, 20, 0, '2019-12-22 10:50:56', 2, 477.00, '0'),
(352, NULL, 168, NULL, '679538', '14', 0.00, 16.00, '1', 540.00, 5, 20, 0, '2019-12-22 10:50:56', 2, 540.00, '0'),
(354, NULL, 170, NULL, '803572', '12', 0.00, 1.00, '1', 700.00, 3, 0, 148, '2019-12-22 10:59:25', 2, 595.00, '15'),
(355, NULL, 171, NULL, '483569', '13', 0.00, 1.00, '1', 700.00, 3, 0, 150, '2019-12-22 11:11:13', 2, 595.00, '15'),
(356, NULL, 172, NULL, '650982', '10', 0.00, 2.00, '1', 700.00, 3, 0, 151, '2019-12-22 11:19:27', 2, 595.00, '15'),
(357, NULL, 173, NULL, '927608', '1', 0.00, 120.00, '1', 126.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 126.00, '0'),
(358, NULL, 173, NULL, '927608', '18', 0.00, 12.00, '1', 1350.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1350.00, '0'),
(359, NULL, 173, NULL, '927608', '19', 0.00, 6.00, '1', 1350.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1350.00, '0'),
(360, NULL, 173, NULL, '927608', '20', 0.00, 12.00, '1', 1530.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1530.00, '0'),
(361, NULL, 173, NULL, '927608', '21', 0.00, 12.00, '1', 1530.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1530.00, '0'),
(362, NULL, 173, NULL, '927608', '23', 0.00, 12.00, '1', 1530.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1530.00, '0'),
(363, NULL, 173, NULL, '927608', '22', 0.00, 4.00, '1', 1530.00, 5, 19, 0, '2019-12-22 11:27:58', 2, 1530.00, '0'),
(364, NULL, 174, NULL, '234589', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 152, '2019-12-22 11:32:07', 2, 1955.00, '15'),
(366, NULL, 176, NULL, '392178', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 153, '2019-12-22 11:42:49', 2, 1955.00, '15'),
(367, NULL, 177, NULL, '589324', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 48, '2019-12-22 11:49:17', 2, 1700.00, '15'),
(368, NULL, 178, NULL, '503241', '1', 0.00, 2.00, '1', 230.00, 3, 0, 155, '2019-12-22 12:07:04', 2, 184.00, '20'),
(369, NULL, 178, NULL, '503241', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 155, '2019-12-22 12:07:04', 2, 1840.00, '20'),
(370, NULL, 179, NULL, '396284', '7', 0.00, 5.00, '1', 185.00, 3, 0, 156, '2019-12-22 12:35:12', 2, 157.00, '15'),
(371, NULL, 180, NULL, '310467', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 74, '2019-12-22 12:48:07', 2, 1700.00, '15'),
(372, NULL, 181, NULL, '408296', '3', 0.00, 1.00, '1', 140.00, 3, 0, 158, '2019-12-22 01:38:57', 2, 119.00, '15'),
(374, NULL, 183, NULL, '145092', '17', 0.00, 2.00, '1', 2000.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1560.00, '22'),
(375, NULL, 183, NULL, '145092', '18', 0.00, 2.00, '1', 2000.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1560.00, '22'),
(376, NULL, 183, NULL, '145092', '19', 0.00, 2.00, '1', 2000.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1560.00, '22'),
(377, NULL, 183, NULL, '145092', '20', 0.00, 2.00, '1', 2300.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1794.00, '22'),
(378, NULL, 183, NULL, '145092', '21', 0.00, 2.00, '1', 2300.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1794.00, '22'),
(379, NULL, 183, NULL, '145092', '22', 0.00, 2.00, '1', 2300.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1794.00, '22'),
(380, NULL, 183, NULL, '145092', '23', 0.00, 2.00, '1', 2300.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 1794.00, '22'),
(381, NULL, 183, NULL, '145092', '1', 0.00, 12.00, '1', 230.00, 2, 0, 160, '2019-12-22 03:06:06', 2, 173.00, '25'),
(382, NULL, 184, NULL, '407869', '5', 0.00, 24.00, '1', 120.00, 4, 0, 6, '2019-12-22 03:29:09', 2, 120.00, '0'),
(383, NULL, 184, NULL, '407869', '4', 0.00, 24.00, '1', 110.00, 4, 0, 6, '2019-12-22 03:29:09', 2, 110.00, '0'),
(384, NULL, 184, NULL, '407869', '2', 0.00, 192.00, '1', 85.00, 4, 0, 6, '2019-12-22 03:29:09', 2, 75.00, '11.8'),
(385, NULL, 184, NULL, '407869', '1', 0.00, 24.00, '1', 140.00, 4, 0, 6, '2019-12-22 03:29:09', 2, 130.00, '7.1'),
(388, NULL, 186, NULL, '512869', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 161, '2019-12-22 06:12:38', 2, 1955.00, '15'),
(389, NULL, 187, NULL, '703218', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 162, '2019-12-23 10:18:59', 2, 1700.00, '15'),
(390, NULL, 188, NULL, '702958', '1', 0.00, 1.00, '1', 230.00, 3, 0, 163, '2019-12-23 10:38:10', 2, 196.00, '15'),
(391, NULL, 188, NULL, '702958', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 163, '2019-12-23 10:38:10', 2, 1700.00, '15'),
(392, NULL, 189, NULL, '214879', '17', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1350.00, '0'),
(393, NULL, 189, NULL, '214879', '18', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1350.00, '0'),
(394, NULL, 189, NULL, '214879', '19', 0.00, 12.00, '1', 1350.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1350.00, '0'),
(395, NULL, 189, NULL, '214879', '20', 0.00, 18.00, '1', 1530.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1530.00, '0'),
(396, NULL, 189, NULL, '214879', '21', 0.00, 18.00, '1', 1530.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1530.00, '0'),
(397, NULL, 189, NULL, '214879', '22', 0.00, 24.00, '1', 1530.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1530.00, '0'),
(398, NULL, 189, NULL, '214879', '23', 0.00, 32.00, '1', 1530.00, 5, 11, 0, '2019-12-23 10:45:49', 2, 1530.00, '0'),
(399, NULL, 190, NULL, '712035', '12', 0.00, 1.00, '1', 700.00, 3, 0, 164, '2019-12-23 10:52:56', 2, 595.00, '15'),
(400, NULL, 191, NULL, '452701', '7', 0.00, 5.00, '1', 185.00, 3, 0, 27, '2019-12-23 11:10:40', 2, 157.00, '15'),
(402, NULL, 193, NULL, '605271', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 167, '2019-12-23 11:34:50', 2, 1700.00, '15'),
(403, NULL, 194, NULL, '849762', '1', 0.00, 1.00, '1', 230.00, 3, 0, 159, '2019-12-23 11:41:04', 2, 196.00, '15'),
(404, NULL, 194, NULL, '849762', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 159, '2019-12-23 11:41:04', 2, 1955.00, '15'),
(405, NULL, 195, NULL, '948130', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 64, '2019-12-23 12:36:32', 2, 1955.00, '15'),
(406, NULL, 196, NULL, '941803', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 168, '2019-12-23 03:32:52', 2, 1955.00, '15'),
(407, NULL, 197, NULL, '745928', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 169, '2019-12-23 03:47:12', 2, 1955.00, '15'),
(408, NULL, 198, NULL, '296417', '2', 0.00, 192.00, '1', 85.00, 4, 0, 3, '2019-12-23 05:57:24', 2, 64.36, '24.3'),
(409, NULL, 199, NULL, '456291', '2', 0.00, 2.00, '1', 130.00, 3, 0, 101, '2019-12-24 10:15:38', 2, 111.00, '15'),
(410, NULL, 199, NULL, '456291', '5', 0.00, 2.00, '1', 170.00, 3, 0, 101, '2019-12-24 10:15:38', 2, 145.00, '15'),
(411, NULL, 200, NULL, '370285', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 171, '2019-12-24 10:28:49', 2, 1940.00, '15.7'),
(413, NULL, 202, NULL, '793840', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 43, '2019-12-24 10:57:53', 2, 1700.00, '15'),
(414, NULL, 203, NULL, '025478', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 173, '2019-12-24 11:34:06', 2, 1955.00, '15'),
(415, NULL, 204, NULL, '542961', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 99, '2019-12-24 11:54:25', 2, 1700.00, '15'),
(416, NULL, 205, NULL, '193604', '19', 0.00, 8.00, '1', 2000.00, 2, 0, 23, '2019-12-24 12:41:11', 2, 1500.00, '25'),
(417, NULL, 205, NULL, '193604', '18', 0.00, 3.00, '1', 2000.00, 2, 0, 23, '2019-12-24 12:41:11', 2, 1500.00, '25'),
(418, NULL, 205, NULL, '193604', '20', 0.00, 3.00, '1', 2300.00, 2, 0, 23, '2019-12-24 12:41:11', 2, 1725.00, '25'),
(419, NULL, 205, NULL, '193604', '23', 0.00, 12.00, '1', 2300.00, 2, 0, 23, '2019-12-24 12:41:11', 2, 1725.00, '25'),
(420, NULL, 205, NULL, '193604', '1', 0.00, 24.00, '1', 230.00, 2, 0, 23, '2019-12-24 12:41:11', 2, 161.00, '30'),
(421, NULL, 206, NULL, '209156', '10', 0.00, 1.00, '1', 700.00, 3, 0, 174, '2019-12-24 12:58:36', 2, 595.00, '15'),
(422, NULL, 207, NULL, '043869', '14', 0.00, 1.00, '1', 800.00, 3, 0, 67, '2019-12-24 01:07:56', 2, 680.00, '15'),
(423, NULL, 208, NULL, '679324', '12', 0.00, 1.00, '1', 700.00, 3, 0, 172, '2019-12-24 01:12:33', 2, 595.00, '15'),
(424, NULL, 208, NULL, '679324', '14', 0.00, 1.00, '1', 800.00, 3, 0, 172, '2019-12-24 01:12:33', 2, 680.00, '15'),
(425, NULL, 209, NULL, '693874', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 175, '2019-12-24 04:10:42', 2, 1955.00, '15'),
(426, NULL, 210, NULL, '402836', '19', 0.00, 3.00, '1', 2000.00, 3, 0, 176, '2019-12-24 04:13:58', 2, 1700.00, '15'),
(427, NULL, 211, NULL, '321506', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 22, '2019-12-24 04:15:28', 2, 1955.00, '15'),
(428, NULL, 212, NULL, '251976', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 177, '2019-12-24 04:18:15', 2, 1955.00, '15'),
(429, NULL, 213, NULL, '596142', '2', 0.00, 12.00, '1', 130.00, 3, 0, 117, '2019-12-24 04:20:47', 2, 111.00, '15'),
(431, NULL, 215, NULL, '713429', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 178, '2019-12-24 04:25:25', 2, 1700.00, '15'),
(432, NULL, 216, NULL, '714853', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 27, '2019-12-24 04:29:03', 2, 1700.00, '15'),
(433, NULL, 217, NULL, '362180', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 179, '2019-12-24 05:00:06', 2, 1955.00, '15'),
(434, NULL, 218, NULL, '804752', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 93, '2019-12-26 10:15:16', 2, 1700.00, '15'),
(435, NULL, 219, NULL, '512468', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 180, '2019-12-26 10:21:11', 2, 1700.00, '15'),
(436, NULL, 220, NULL, '596314', '14', 0.00, 2.00, '1', 800.00, 3, 0, 123, '2019-12-26 10:49:41', 2, 680.00, '15'),
(437, NULL, 221, NULL, '960825', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 181, '2019-12-26 10:55:27', 2, 1700.00, '15'),
(438, NULL, 222, NULL, '613579', '17', 0.00, 1.00, '1', 2000.00, 3, 0, 182, '2019-12-26 11:04:51', 2, 1700.00, '15'),
(439, NULL, 223, NULL, '946580', '1', 0.00, 2.00, '1', 230.00, 3, 0, 183, '2019-12-26 11:14:36', 2, 196.00, '15'),
(440, NULL, 223, NULL, '946580', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 183, '2019-12-26 11:14:36', 2, 1980.00, '1'),
(441, NULL, 223, NULL, '946580', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 183, '2019-12-26 11:14:36', 2, 1955.00, '15'),
(442, NULL, 224, NULL, '591302', '6', 0.00, 1.00, '1', 180.00, 3, 0, 184, '2019-12-26 11:19:40', 2, 153.00, '15'),
(443, NULL, 225, NULL, '154980', '12', 0.00, 1.00, '1', 700.00, 3, 0, 185, '2019-12-26 11:26:39', 2, 595.00, '15'),
(444, NULL, 226, NULL, '906428', '17', 0.00, 18.00, '1', 1350.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 1350.00, '0'),
(445, NULL, 226, NULL, '906428', '19', 0.00, 6.00, '1', 1350.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 1350.00, '0'),
(446, NULL, 226, NULL, '906428', '20', 0.00, 18.00, '1', 1530.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 1530.00, '0'),
(447, NULL, 226, NULL, '906428', '21', 0.00, 12.00, '1', 1530.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 1530.00, '0'),
(448, NULL, 226, NULL, '906428', '22', 0.00, 6.00, '1', 1530.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 1530.00, '0'),
(449, NULL, 226, NULL, '906428', '4', 0.00, 48.00, '1', 99.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 99.00, '0'),
(450, NULL, 226, NULL, '906428', '5', 0.00, 48.00, '1', 108.00, 5, 14, 0, '2019-12-26 11:33:38', 2, 108.00, '0'),
(451, NULL, 227, NULL, '372806', '1', 0.00, 480.00, '1', 126.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 126.00, '0'),
(452, NULL, 227, NULL, '372806', '18', 0.00, 6.00, '1', 1350.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 1350.00, '0'),
(453, NULL, 227, NULL, '372806', '19', 0.00, 12.00, '1', 1350.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 1350.00, '0'),
(454, NULL, 227, NULL, '372806', '20', 0.00, 18.00, '1', 1530.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 1530.00, '0'),
(455, NULL, 227, NULL, '372806', '21', 0.00, 24.00, '1', 1530.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 1530.00, '0'),
(456, NULL, 227, NULL, '372806', '22', 0.00, 24.00, '1', 1530.00, 5, 11, 0, '2019-12-26 11:39:27', 2, 1530.00, '0'),
(457, NULL, 228, NULL, '073268', '11', 0.00, 2.00, '1', 700.00, 3, 0, 186, '2019-12-26 11:49:33', 2, 595.00, '15'),
(458, NULL, 229, NULL, '083541', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 179, '2019-12-26 12:10:35', 2, 1955.00, '15'),
(459, NULL, 230, NULL, '146257', '2', 0.00, 48.00, '1', 85.00, 4, 0, 187, '2019-12-26 12:58:59', 2, 75.00, '11.8'),
(460, NULL, 231, NULL, '924610', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 188, '2019-12-26 01:34:04', 2, 1955.00, '15'),
(461, NULL, 232, NULL, '623579', '23', 0.00, 1.00, '1', 2300.00, 3, 0, 189, '2019-12-26 03:02:40', 2, 1955.00, '15'),
(462, NULL, 233, NULL, '639578', '11', 0.00, 2.00, '1', 700.00, 3, 0, 66, '2019-12-26 04:51:02', 2, 595.00, '15'),
(463, NULL, 234, NULL, '216430', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 190, '2019-12-28 10:04:55', 2, 1955.00, '15'),
(464, NULL, 235, NULL, '408235', '1', 0.00, 4.00, '1', 230.00, 3, 0, 191, '2019-12-28 10:20:48', 2, 196.00, '15'),
(465, NULL, 236, NULL, '903276', '13', 0.00, 30.00, '1', 530.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 530.00, '0'),
(466, NULL, 236, NULL, '903276', '23', 0.00, 30.00, '1', 1700.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 1700.00, '0'),
(467, NULL, 236, NULL, '903276', '14', 0.00, 15.00, '1', 600.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 600.00, '0');
INSERT INTO `stockposting` (`serialNumber`, `purchaseMasterId`, `disburseMasterId`, `purchaseInvoiceNo`, `disburseInvoiceNo`, `productBatchId`, `inwardQuantity`, `outwardQuantity`, `unitId`, `rate`, `destination_type`, `destinationId`, `customerId`, `date`, `stock_type`, `discountrate`, `discounts`) VALUES
(468, NULL, 236, NULL, '903276', '1', 0.00, 12.00, '1', 140.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 140.00, '0'),
(469, NULL, 236, NULL, '903276', '12', 0.00, 8.00, '1', 530.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 530.00, '0'),
(470, NULL, 236, NULL, '903276', '11', 0.00, 6.00, '1', 530.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 530.00, '0'),
(471, NULL, 236, NULL, '903276', '18', 0.00, 6.00, '1', 1500.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 1500.00, '0'),
(472, NULL, 236, NULL, '903276', '19', 0.00, 6.00, '1', 1500.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 1500.00, '0'),
(473, NULL, 236, NULL, '903276', '21', 0.00, 6.00, '1', 1700.00, 1, 0, 125, '2019-12-28 10:31:44', 2, 1700.00, '0'),
(474, NULL, 237, NULL, '347102', '8', 0.00, 2.00, '1', 190.00, 3, 0, 192, '2019-12-28 10:39:37', 2, 162.00, '15'),
(475, NULL, 238, NULL, '635280', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 193, '2019-12-28 10:54:23', 2, 1700.00, '15'),
(476, NULL, 239, NULL, '732456', '14', 0.00, 1.00, '1', 800.00, 3, 0, 194, '2019-12-28 11:06:46', 2, 680.00, '15'),
(477, NULL, 240, NULL, '051967', '19', 0.00, 1.00, '1', 2000.00, 3, 0, 195, '2019-12-28 11:11:56', 2, 1700.00, '15'),
(478, NULL, 241, NULL, '409675', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 196, '2019-12-28 11:16:57', 2, 1700.00, '15'),
(479, NULL, 242, NULL, '402158', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 197, '2019-12-28 11:22:21', 2, 1955.00, '15'),
(480, NULL, 243, NULL, '165379', '14', 0.00, 1.00, '1', 800.00, 3, 0, 198, '2019-12-28 11:35:36', 2, 680.00, '15'),
(481, NULL, 244, NULL, '235496', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 36, '2019-12-28 11:44:13', 2, 1955.00, '15'),
(482, NULL, 245, NULL, '246509', '1', 0.00, 10.00, '1', 230.00, 3, 0, 4, '2019-12-28 12:10:50', 2, 185.00, '19.6'),
(483, NULL, 245, NULL, '246509', '7', 0.00, 1.00, '1', 185.00, 3, 0, 4, '2019-12-28 12:10:50', 2, 148.00, '20'),
(484, NULL, 246, NULL, '540861', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 94, '2019-12-28 12:18:31', 2, 1955.00, '15'),
(485, NULL, 247, NULL, '089435', '17', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-28 12:19:52', 2, 1350.00, '0'),
(486, NULL, 247, NULL, '089435', '18', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-28 12:19:52', 2, 1350.00, '0'),
(487, NULL, 247, NULL, '089435', '19', 0.00, 6.00, '1', 1350.00, 5, 16, 0, '2019-12-28 12:19:52', 2, 1350.00, '0'),
(488, NULL, 247, NULL, '089435', '1', 0.00, 168.00, '1', 126.00, 5, 16, 0, '2019-12-28 12:19:52', 2, 126.00, '0'),
(489, NULL, 248, NULL, '820964', '1', 0.00, 24.00, '1', 230.00, 2, 0, 199, '2019-12-28 12:36:39', 2, 161.00, '30'),
(490, NULL, 248, NULL, '820964', '2', 0.00, 12.00, '1', 130.00, 2, 0, 199, '2019-12-28 12:36:39', 2, 98.00, '25'),
(491, NULL, 249, NULL, '813092', '1', 0.00, 24.00, '1', 230.00, 2, 0, 200, '2019-12-28 12:47:10', 2, 161.00, '30'),
(492, NULL, 249, NULL, '813092', '2', 0.00, 12.00, '1', 130.00, 2, 0, 200, '2019-12-28 12:47:10', 2, 98.00, '25'),
(493, NULL, 249, NULL, '813092', '12', 0.00, 12.00, '1', 700.00, 2, 0, 200, '2019-12-28 12:47:10', 2, 525.00, '25'),
(494, NULL, 249, NULL, '813092', '17', 0.00, 6.00, '1', 2000.00, 2, 0, 200, '2019-12-28 12:47:10', 2, 1500.00, '25'),
(495, NULL, 249, NULL, '813092', '18', 0.00, 6.00, '1', 2000.00, 2, 0, 200, '2019-12-28 12:47:10', 2, 1500.00, '25'),
(496, NULL, 250, NULL, '325168', '19', 0.00, 6.00, '1', 2000.00, 2, 0, 200, '2019-12-28 12:49:41', 2, 1500.00, '25'),
(497, NULL, 250, NULL, '325168', '20', 0.00, 6.00, '1', 2300.00, 2, 0, 200, '2019-12-28 12:49:41', 2, 1725.00, '25'),
(498, NULL, 250, NULL, '325168', '21', 0.00, 3.00, '1', 2300.00, 2, 0, 200, '2019-12-28 12:49:41', 2, 1725.00, '25'),
(499, NULL, 251, NULL, '362874', '21', 0.00, 3.00, '1', 2300.00, 2, 0, 200, '2019-12-28 12:51:02', 2, 1725.00, '25'),
(500, NULL, 251, NULL, '362874', '22', 0.00, 6.00, '1', 2300.00, 2, 0, 200, '2019-12-28 12:51:02', 2, 1725.00, '25'),
(501, NULL, 251, NULL, '362874', '23', 0.00, 6.00, '1', 2300.00, 2, 0, 200, '2019-12-28 12:51:02', 2, 1725.00, '25'),
(502, NULL, 252, NULL, '850694', '8', 0.00, 2.00, '1', 190.00, 3, 0, 201, '2019-12-28 01:07:49', 2, 162.00, '15'),
(503, NULL, 253, NULL, '869715', '20', 0.00, 1.00, '1', 2300.00, 3, 0, 203, '2019-12-28 01:25:10', 2, 1955.00, '15'),
(504, NULL, 254, NULL, '193607', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 204, '2019-12-28 01:48:48', 2, 1955.00, '15'),
(505, NULL, 255, NULL, '370621', '2', 0.00, 240.00, '1', 85.00, 4, 0, 205, '2019-12-28 04:08:47', 2, 85.00, '0'),
(506, NULL, 256, NULL, '742359', '2', 0.00, 240.00, '1', 85.00, 4, 0, 6, '2019-12-28 04:11:07', 2, 75.00, '11.8'),
(509, NULL, 258, NULL, '534618', '21', 0.00, 1.00, '1', 2300.00, 3, 0, 207, '2019-12-28 05:32:53', 2, 1955.00, '15'),
(510, NULL, 259, NULL, '706142', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 208, '2019-12-29 10:07:36', 2, 1700.00, '15'),
(511, NULL, 260, NULL, '579128', '18', 0.00, 1.00, '1', 2000.00, 3, 0, 209, '2019-12-29 10:34:28', 2, 1700.00, '15'),
(512, NULL, 261, NULL, '396542', '2', 0.00, 100.00, '1', 85.00, 4, 0, 210, '2019-12-29 11:16:18', 2, 75.00, '11.8'),
(513, NULL, 262, NULL, '270356', '1', 0.00, 3.00, '1', 230.00, 3, 0, 176, '2019-12-29 04:06:06', 2, 196.00, '15'),
(514, NULL, 262, NULL, '270356', '19', 0.00, 2.00, '1', 2000.00, 3, 0, 176, '2019-12-29 04:06:06', 2, 1700.00, '15'),
(516, NULL, 264, NULL, '526810', '22', 0.00, 1.00, '1', 2300.00, 3, 0, 25, '2019-12-29 04:15:20', 2, 1530.00, '33.5'),
(517, NULL, 265, NULL, '394751', '1', 0.00, 10.00, '1', 230.00, 3, 0, 211, '2019-12-29 04:36:31', 2, 196.00, '15'),
(518, NULL, 265, NULL, '394751', '2', 0.00, 12.00, '1', 130.00, 3, 0, 211, '2019-12-29 04:36:31', 2, 100.00, '23.1');

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE `system` (
  `id` int(11) NOT NULL,
  `app_name` varchar(64) DEFAULT NULL,
  `app_title` varchar(64) DEFAULT NULL,
  `address` varchar(156) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(56) NOT NULL,
  `logo_path` varchar(156) DEFAULT NULL,
  `updated_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`id`, `app_name`, `app_title`, `address`, `phone`, `email`, `logo_path`, `updated_time`) VALUES
(1, 'Little Feat', 'Little feat inventory System', '8/5 nikunja,Dhaka', 1686799560, 'info@littelefeetinventory.com', 'dia_kidz-283x179.png', '2019-12-12 10:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE `userrole` (
  `id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`id`, `role_name`, `description`) VALUES
(1, 'Admin', 'admin'),
(2, 'Sales Manager', 'Sales Manager'),
(3, 'Dealer ', 'Dealer'),
(4, 'SR', 'SR'),
(5, 'Accounts', 'Accounts');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(56) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `present_address` varchar(156) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `dealer_point_name` varchar(155) DEFAULT NULL,
  `dealerid` int(11) DEFAULT NULL,
  `zonename` int(11) DEFAULT NULL,
  `status` smallint(2) DEFAULT 0,
  `image_path` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `password`, `name`, `email`, `phone`, `present_address`, `role`, `dealer_point_name`, `dealerid`, `zonename`, `status`, `image_path`, `created_date`, `created_by`) VALUES
(1, 'shahed', '1234', 'admin', 'cseshahed@gmail.com', '01686799560', '345564564654                                                                                                                                            ', 1, NULL, NULL, NULL, 1, 'bbcs.png', '2019-12-09 04:28:18', 0),
(9, 'Asad Uz zaman Titumir ', 'titumir123456', 'Titumir', 'aztitumir@gmail.com', '01613329289', '', 1, '', 0, 0, 1, 'default.jpg', '2019-12-19 07:11:46', 1),
(10, 'Md. Saeeduzzaman', 'saeed123456', 'saeed', 'saeed.vini@gmail.com', '01722106707', '', 2, '', 0, NULL, 1, 'IMG_20190915_150632.jpg', '2019-12-08 05:58:27', 1),
(11, 'Md.Robin', 'robin1234', 'robin', 'akheruzzaman.sarker@gmail.com', '01792818181', '', 3, 'Maks Distribution', 0, 3, 1, 'default.jpg', '2019-12-08 01:05:31', 1),
(12, 'Md.Shahdat Hossan', 'shaoun1234', 'shaoun', 'shaoun@gmail.com', '01715777774', '', 2, '', 0, NULL, 1, 'default.jpg', '2019-12-20 11:43:42', 1),
(13, 'Abdul Muiez', 'Probook@83', 'muiez', 'mdmuiez@gmail.com', '01713034486', '', 1, '', 0, 0, 1, 'default.jpg', '2019-12-20 11:42:28', 10),
(14, 'Amirul Abedin Shakil', 'shakil123456', 'shakil', '', '01734634435', 'Mirpur -1', 3, 'Family Enterprise', 0, 3, 1, 'default.jpg', '2019-12-09 05:31:48', 10),
(15, 'Md.Raisul Islam', 'raisul1234', 'raisul', 'raisul.rbt@gmail.com', '01819626030', '', 1, '', 0, 0, 1, 'default.jpg', '2019-12-10 04:21:58', 1),
(16, 'Md Abdur Rashid', 'rashid123456', 'rashid', '', '01718954151', 'ka,144/1,moddho para, khilkhet,Dhaka', 3, 'Uttara Trade Center', 0, 3, 1, 'default.jpg', '2019-12-11 12:39:42', 10),
(17, 'Saiful Islam', 'noyon123456', 'noyon', '', '01914939632', 'Link Road, Gulshan, Dhaka', 3, 'Shahaba Traders', 0, 3, 1, 'default.jpg', '2019-12-11 02:19:16', 10),
(18, 'Asiful islam', '01851323766', 'asif0185', 'asifulislam0185@gmail.com', '01851323766', '', 1, '', 0, 0, 1, '20190613_141048-2.jpg', '2019-12-11 03:45:11', 1),
(19, 'Dina', 'dina123456', 'dina', 'niroz.agency@gmail.com', '01712058420', '', 3, 'Niroz agency', 0, 3, 1, 'default.jpg', '2019-12-11 03:51:51', 10),
(20, 'Md Sayem Chowdhuri', 'saem123456', 'sayem', '', '01915698903', '', 3, 'Alfee Enterprise', 0, 3, 1, 'default.jpg', '2019-12-11 03:57:12', 10),
(21, 'Md Abdul Halim', 'halim123456', 'halim', 'halim@gmail.com', '01755683684', '3rd floor,Samimabad Vaban,Bongabandhu Avenue, Dhaka-1200', 3, 'Alif Trading', 0, 3, 1, 'default.jpg', '2019-12-12 12:35:40', 10),
(22, 'Md Helal Uddin', 'helal123456', 'helal', 'helaluddinlaw1987@gmail.com', '01915500504', '', 4, '', 17, 0, 1, 'default.jpg', '2019-12-15 03:37:48', 10),
(23, 'Md Sohel Rana', 'sohel123456', 'sohel', 'sohelwv@gmail.com', '01701092981', '', 4, '', 14, 0, 1, 'default.jpg', '2019-12-15 06:34:52', 10),
(24, 'Md Yasir Arafat', 'arafat123456', 'arafat', '', '01677589825', '', 4, '', 20, 0, 1, 'default.jpg', '2019-12-15 03:54:10', 10),
(25, 'Md Abu Taher Shipu', 'shipu123456', 'shipu', '', '01307768640', '', 4, '', 19, 0, 1, 'default.jpg', '2019-12-15 03:55:51', 10),
(26, 'Md Enamul Khan', 'enamul123456', 'enamul', '', '01822503294', '', 4, '', 11, 0, 1, 'default.jpg', '2019-12-15 03:59:21', 10),
(27, 'Albinus Simsung', 'alvi123456', 'alvi', '', '01701040027', '', 4, '', 11, 0, 1, 'default.jpg', '2019-12-15 04:14:59', 10),
(28, 'Md Ibrahim Hossain', 'ibrahim123456', 'ibrahim', '', '01838942407', '', 4, '', 16, 0, 1, 'default.jpg', '2019-12-15 04:17:48', 10),
(29, 'Md arif Hossain', 'arif123456', 'arif', '', '01778999005', '', 3, 'Jinaidah', 0, 6, 1, 'default.jpg', '2019-12-17 04:17:38', 10),
(30, 'Ruhul Amin', 'ruhul123456', 'ruhul', '', '01718443107', 'SS Road, Shirajgonj', 3, 'Alauddin Store', 0, 0, 1, 'default.jpg', '2019-12-18 02:39:21', 10),
(31, 'Nasrin Parvin', 'nimni4321', 'nimni', 'nasrin.nimni@gmail.com', '01918322807', 'Dhaka', 5, '', 0, 0, 1, 'default.jpg', '2019-12-24 03:36:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` int(50) NOT NULL,
  `vendorName` varchar(50) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `emailId` varchar(50) DEFAULT NULL,
  `mobileNumber` varchar(50) DEFAULT NULL,
  `created_by` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendorId`, `vendorName`, `address`, `country`, `emailId`, `mobileNumber`, `created_by`) VALUES
(1, 'china', '300/B, BLOCK-A, ROAD-14', 'china', 'jhrjthrjkth', '746578', 1),
(2, 'Local Vendor', 'Nikunja', 'Bangladesh', 'vendor@gmail.com', '12345678', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_cost`
--
ALTER TABLE `additional_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `allblood_group`
--
ALTER TABLE `allblood_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan`
--
ALTER TABLE `challan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challan_details`
--
ALTER TABLE `challan_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cost_category`
--
ALTER TABLE `cost_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disburse_type`
--
ALTER TABLE `disburse_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district_govt`
--
ALTER TABLE `district_govt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division_zone`
--
ALTER TABLE `division_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledgerposting`
--
ALTER TABLE `ledgerposting`
  ADD PRIMARY KEY (`ledgerPostingId`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordermaster`
--
ALTER TABLE `ordermaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productbatch`
--
ALTER TABLE `productbatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sub_list`
--
ALTER TABLE `product_sub_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_unit`
--
ALTER TABLE `product_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchasemaster`
--
ALTER TABLE `purchasemaster`
  ADD PRIMARY KEY (`purchaseMasterId`);

--
-- Indexes for table `salesmaster`
--
ALTER TABLE `salesmaster`
  ADD PRIMARY KEY (`salesMasterId`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockposting`
--
ALTER TABLE `stockposting`
  ADD PRIMARY KEY (`serialNumber`);

--
-- Indexes for table `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_cost`
--
ALTER TABLE `additional_cost`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `allblood_group`
--
ALTER TABLE `allblood_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `challan`
--
ALTER TABLE `challan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `challan_details`
--
ALTER TABLE `challan_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cost_category`
--
ALTER TABLE `cost_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `disburse_type`
--
ALTER TABLE `disburse_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `district_govt`
--
ALTER TABLE `district_govt`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `division_zone`
--
ALTER TABLE `division_zone`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ledgerposting`
--
ALTER TABLE `ledgerposting`
  MODIFY `ledgerPostingId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ordermaster`
--
ALTER TABLE `ordermaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productbatch`
--
ALTER TABLE `productbatch`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_sub_list`
--
ALTER TABLE `product_sub_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `product_unit`
--
ALTER TABLE `product_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchasemaster`
--
ALTER TABLE `purchasemaster`
  MODIFY `purchaseMasterId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `salesmaster`
--
ALTER TABLE `salesmaster`
  MODIFY `salesMasterId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stockposting`
--
ALTER TABLE `stockposting`
  MODIFY `serialNumber` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=519;

--
-- AUTO_INCREMENT for table `system`
--
ALTER TABLE `system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `userrole`
--
ALTER TABLE `userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendorId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
