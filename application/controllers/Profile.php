<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
    }

    public function index() {

        if (in_array($this->session->userdata('user_role'), array(1, 2, 3,4,5))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'My Profile';
            $data['active_menu'] = 'profile';
            $data['sub_menu'] = '';
            $data['role'] = $this->session->userdata('user_role');

            $userid = $this->session->userdata('user_id');
            $data['user_data'] = $this->common->profileView($userid);


            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('admin/profile', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

    public function dealer_info() {

        if (in_array($this->session->userdata('user_role'), array(1, 2, 3,4,5))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Dealer Info';
            $data['active_menu'] = 'admin';
            $data['sub_menu'] = 'dealer';

            $data['all_dealer'] = $this->db->query("SELECT * FROM users where role='3'")->result();

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('admin/dealer_info', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

    public function edit() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3,4,5))) :
            $id = $this->input->post('user_id');
            $post['full_name'] = $this->input->post('full_name');
            $post['name'] = $this->input->post('name');
            $post['email'] = $this->input->post('email');
            $post['phone'] = $this->input->post('phone');
            $post['present_address'] = $this->input->post('present_address');

            $update = $this->common->update('users', $id, $post);
            if ($update):
                $this->session->set_userdata('add', 'Profile update Successfully');
            else:
                $this->session->set_userdata('notadd', 'Failed to Profile update');
            endif;

            redirect('profile');
        else :
            redirect('auth');
        endif;
    }

    function updatePassword() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3,4,5))):

            $userid = $this->session->userdata('user_id');
            $currentpass = $this->input->post('cpassword');
            $newpass = $this->input->post('npassword');

            $queryCheckPwd = $this->common->checkPassword($userid, 'users', $currentpass);

            if ($queryCheckPwd == true) {
                $userdata = array(
                    'password' => $newpass
                );
                $this->db->where('id', $userid);
                $status = $this->db->update('users', $userdata);
                if ($status):
                    $this->session->set_userdata('add', 'Password updated successfully !!!');
                else :
                    $this->session->set_userdata('notadd', 'Password updated fail. Try again !!!');
                endif;
                redirect('profile');
            } else {
                $this->session->set_userdata('notadd', 'Password do not match with your existing password, Please try again !!!');
                redirect('profile');
            }
        else:
            redirect('auth');
        endif;
    }

    function updateImage() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3,4,5))):

            $userpin = $this->session->userdata('user_id');
            $target_dir = "assets/images/uploads/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imgFile = $_FILES['fileToUpload']['name'];
            if (empty($imgFile)) {
                $post['image_path'] = 'default.jpg';
            } elseif (!empty($imgFile)) {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                    $post['image_path'] = basename($_FILES["fileToUpload"]["name"]);
                else:
                    $data['error'] = "Sorry, there was an error uploading your file";
                endif;
            }
            $update = $this->common->updateImage($userpin, $post);

            if ($update):
                $this->session->set_userdata('add', 'Image Upload Successfully');
            else:
                $this->session->set_userdata('notadd', 'Failed to Image Upload');
            endif;

            redirect('profile');
        else :
            redirect('auth');
        endif;
    }

}
