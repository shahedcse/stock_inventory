<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common');
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3])) {
            redirect('auth');
        }
    }

    public function purchase_list() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Purchase List';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'purchase';
        $data['purchase_list'] = $this->common->viewAll('purchasemaster');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/purchase_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function disburse_list() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Disburse List';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'disburse';
        $data['disburse_list'] = $this->common->viewAll('salesmaster');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/disburse_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function home_delivery() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Home Delivery List';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'homedelivery';
        $data['home_list'] = $this->db->query("SELECT * FROM salesmaster WHERE disburse_type='3'")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/homedelivery_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function delevery_status() {
        $masterid = $this->input->post('master_id2');
        $masterdata = [
            'delivery_status' => 1,
            'payment_status' => $this->input->post('payment_status')
        ];
        $this->db->where('salesMasterId', $masterid);
        $status = $this->db->update('salesmaster', $masterdata);
        if ($status):
            $this->session->set_userdata('add', 'Status Changed successfully');
        else:
            $this->session->set_userdata('notadd', 'Status Chnaged Failed');
        endif;
        redirect('Inventory/home_delivery');
    }

    public function add_disburse() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Add Disburse';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'disburse';
        $data['productlist'] = $this->common->viewAll('product_category');
        $data['subproductlist'] = $this->common->viewAll('product_sub_list');
        $data['unitlist'] = $this->common->viewAll('product_unit');
        $data['disburse_list'] = $this->common->viewAll('disburse_type');
        $data['customer_list'] = $this->common->viewAll('customer');
        $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3'")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/add_disburse', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_purchase() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Add Purchase';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'purchase';
        $data['productlist'] = $this->common->viewAll('product_category');
        $data['subproductlist'] = $this->common->viewAll('product_sub_list');
        $data['unitlist'] = $this->common->viewAll('product_unit');
        $data['vendorlist'] = $this->db->query("SELECT * FROM vendor order by vendorName ASC")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/add_purchase', $data);
        $this->load->view('common/footer', $data);
    }

    public function edit_purchase() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Edit Purchase';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'purchase';
        $masterid = $this->input->get('id');
        $data['allinfo'] = $this->db->query("SELECT purchasemaster.*,stockposting.* FROM purchasemaster JOIN  stockposting ON stockposting.purchaseMasterId=purchasemaster.purchaseMasterId WHERE purchasemaster.purchaseMasterId='$masterid' ")->row();
        $data['productlist'] = $this->common->viewAll('product_category');
        $data['subproductlist'] = $this->common->viewAll('product_sub_list');
        $data['unitlist'] = $this->common->viewAll('product_unit');
        $data['vendorlist'] = $this->db->query("SELECT * FROM vendor order by vendorName ASC")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/edit_purchase', $data);
        $this->load->view('common/footer', $data);
    }

    public function purchase_insert() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $p_date = $this->input->post('purchase_date');
            $date = date("Y-m-d", strtotime($p_date));
            $user_id = $this->session->userdata('user_id');
            $productid = $this->input->post('prod_sublist');
            $masterdata = [
                'purchase_date' => $date,
                'purchaseInvoiceNo' => $this->input->post('purchaseInvoiceNo'),
                'vendorId' => $this->input->post('purchase_from'),
                'prod_group' => $this->input->post('prod_cat'),
                'prod_id' => $productid,
                'unit' => $this->input->post('unit'),
                'unit_price' => $this->input->post('unit_price'),
                'sale_price' => $this->input->post('sale_price'),
                'trade_price' => $this->input->post('trade_price'),
                'mrp' => $this->input->post('mrp'),
                'amount' => $this->input->post('total_price'),
                'date' => date('Y-m-d')
            ];
            $this->db->insert('purchasemaster', $masterdata);
            $masterid = $this->db->query("SELECT MAX(purchaseMasterId) AS max FROM purchasemaster ")->row()->max;

            $saledata = array(
                'purchaseRate' => $this->input->post('unit_price'),
                'salesRate' => $this->input->post('sale_price'),
                'trade_price' => $this->input->post('trade_price'),
                'MRP' => $this->input->post('mrp'),
                'created_date' => date('Y-m-d')
            );
            $this->db->where('productId', $productid);
            $this->db->update('productbatch', $saledata);
            $stockdata = [
                'purchaseMasterId' => $masterid,
                'purchaseInvoiceNo' => $this->input->post('purchaseInvoiceNo'),
                'productBatchId' => $this->input->post('prod_sublist'),
                'inwardQuantity' => $this->input->post('quantity'),
                'unitId' => $this->input->post('unit'),
                'rate' => $this->input->post('unit_price'),
                'destinationId' => $user_id,
                'stock_type' => 1,
                'date' => date('Y-m-d h:i:s')
            ];
            $status = $this->db->insert('stockposting', $stockdata);
            if ($status):
                $this->session->set_userdata('add', 'Purrchase add successfully');
            else:
                $this->session->set_userdata('notadd', 'Failed to Purrchase add');
            endif;
            redirect('Inventory/purchase_list');
        else :
            redirect('auth');
        endif;
    }

    function get_sub() {
        $prod_cat = $this->input->post('prod_cat');
        $subQr = $this->db->query("SELECT * FROM  product_sub_list WHERE product_cat = '$prod_cat'");
        $sublistlist = $subQr->result();
        if (sizeof($sublistlist)):
            echo '<option value=""> --Select-- </option>';
            foreach ($sublistlist as $datarow):
                echo '<option value="' . $datarow->id . '">' . $datarow->id . '_' . $datarow->sublist_name . '-' . $datarow->pack_size . '</option>';
            endforeach;
        else:
            echo '<option value=""> No Product available </option>';
        endif;
    }

    public function getlist() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :

            $id = $this->input->get('prod_cat');
            $listname = $this->common->fetchResult('product_sub_list', $id, 'product_cat');
            echo json_encode($listname);

        else :
            redirect('auth');
        endif;
    }

    public function unit_name() {
        $product_id = $this->input->post("product_id");

        //query  unit  name from table
        $queryunit = $this->db->query("SELECT  unitId  FROM  stockposting WHERE productBatchId='$product_id'");
        // $data['unitinfo'] = $queryunit->result();
        $row1 = $queryunit->row_array();
        $unitId = $row1['unitId'];

        //query  unit  name from table
        $queryunitname = $this->db->query("SELECT  unit_name  FROM  product_unit WHERE id='$unitId'");
        $row2 = $queryunitname->row_array();
        $unitName = $row2['unit_name'];
        echo $unitId . "," . $unitName;
    }

    public function getBusinessofName() {
        $ledgerId = $this->input->post("ledgerId");

        //query  unit  name from table
        $queryunit = $this->db->query("SELECT  nameOfBusiness  FROM  accountledger WHERE ledgerId='$ledgerId'");
        // $data['unitinfo'] = $queryunit->result();
        $row1 = $queryunit->row_array();
        $unitId = $row1['nameOfBusiness'];

        echo $unitId;
    }

    //query qty range for product
    public function product_qty() {
        $product_id = $this->input->post("product_id");
        //  $sdate = "2018-01-01 00:00:00"; #$this->sessiondata['mindate'];
        // $edate = $this->sessiondata['maxdate'];
        if (!empty($product_id)) {
            $stockinqr = $this->db->query("SELECT SUM(inwardQuantity) AS qty FROM stockposting WHERE productBatchId='$product_id' AND stock_type='1'")->row()->qty;
            $stockoutqr = $this->db->query("SELECT SUM(outwardQuantity) AS qty1 FROM stockposting WHERE productBatchId='$product_id'")->row()->qty1;
            $stock = ($stockinqr - $stockoutqr);
            //  $quantity = $this->db->query("select sum(stockposting.inwardQuantity -stockposting.outwardQuantity) as totalqty from stockposting  where stockposting.productBatchId = '$product_id'");
            echo $stock;
        }
    }

    public function add_view_table() {
        $product_name = $this->input->post('product_name');
        $unit = $this->input->post('unit');
        $count = $this->input->post('count');
        //query  product  name from table
        $queryp = $this->db->query("SELECT  *  FROM  product_sub_list WHERE id='$product_name'");
        $row1 = $queryp->row_array();
        $productName = $row1['sublist_name'];

        //query  unit  name from table
        $queryunit = $this->db->query("SELECT  *  FROM  product_unit WHERE id='$unit'");
        $row2 = $queryunit->row_array();
        $unitName = $row2['unit_name'];

        //Net amount per product
        $qty = $this->input->post('qty');
        $rate = $this->input->post('rate');
        $discounts = $this->input->post('discounts');
        $discountrate = $this->input->post('discountrate');

        $qtyrate = $qty * $discountrate;
        $grandtotal = $qtyrate;  //total amount

        $rowdata = '<tr id="row' . $count . '"> <td> <a onclick=deleteRowData(' . $count . ');  href="#"><i class="material-icons">add_to_queue</i></a> </td>
                    <td>' . $productName . '<input name="product_name' . $count . '" id="product_name' . $count . '" type="hidden" value="' . $this->input->post('product_name') . '"/></td>
                    <td id="click" ><span>' . $this->input->post('qty') . '</span><input name="qty' . $count . '"   id="qty' . $count . '" type="hidden" value="' . $this->input->post('qty') . '"/></td>
                    <td>' . $unitName . '<input name="unit' . $count . '" id="unit' . $count . '" type="hidden" value="' . $this->input->post('unit') . '"/></td>
                 <td><span>' . $this->input->post('rate') . '</span><input name="rate' . $count . '" id="rate' . $count . '" type="hidden"  class="edit_input"value="' . $this->input->post('rate') . '"/></td>        
<td>' . $discounts . '<input name="discounts' . $count . '" id="discounts' . $count . '" type="hidden" value="' . $this->input->post('discounts') . '"/></td>
                                <td>' . $discountrate . '<input name="discountrate' . $count . '" id="discountrate' . $count . '" type="hidden" value="' . $this->input->post('discountrate') . '"/></td>'
                . '<td><span id="product_amount' . $count . '">' . $grandtotal . '</span></td>
               </tr>';


        $outputdata = array(
            'rowdata' => $rowdata,
            'printdataoffice' => '',
            'printdatacustomer' => ''
        );

        echo json_encode($outputdata);
    }

    //query unit id for product
    public function product_salerate() {
        $product_id = $this->input->post("product_id");
        $type = $this->input->post("cat_type");
        if ($type == 5):
            $field = 'salesRate';
        elseif ($type == 1 || $type == 4):
            $field = 'trade_price';
        else:
            $field = 'MRP';
        endif;

        //query  unit  name from table
        $queryproductbatch = $this->db->query("SELECT  $field  FROM  productbatch  WHERE productId='$product_id'");
        $row = $queryproductbatch->row_array();
        $salesRate = floor($row[$field]);
        echo $salesRate;
    }

    public function disburse_add() {
        $data['base_url'] = $this->config->item('base_url');
        $count = $this->input->post('count_product');
        $d_date = $this->input->post('invoice_date');
        $invoicedate = date("Y-m-d", strtotime($d_date));
        $invoice = substr(str_shuffle("0123456789"), 0, 6);
        $data1 = array(
            'salesInvoiceNo' => $invoice,
            'disburse_type' => $this->input->post('disburse_type'),
            'customer' => $this->input->post('customer_name'),
            'disburse_to' => $this->input->post('disburse_to'),
            'date' => $invoicedate,
            'total_amount' => $this->input->post('total_amout'),
            'delivery_charge' => $this->input->post('delivery_charge'),
            'netamount' => $this->input->post('netamount'),
            'paid_amount' => $this->input->post('paid_amount'),
            'due_amount' => $this->input->post('due_amount'),
            'delivered_by' => $this->session->userdata('user_id'),
            'delivery_by' => $this->input->post('delivery_by'),
            'remarks' => $this->input->post('remarks')
        );
        $this->db->insert('salesmaster', $data1);
        $dueamount = $this->input->post('due_amount');
        if (!empty($dueamount)):
            $data2 = array(
                'voucherNumber' => $invoice,
                'ledgerId_dealer' => $this->input->post('disburse_to'),
                'ledgerId_customer' => $this->input->post('customer_name'),
                'credit' => $this->input->post('due_amount'),
                'date' => $invoicedate
            );
            $this->db->insert('ledgerposting', $data2);
        endif;

        $query1 = $this->db->query("SELECT MAX(salesMasterId) FROM salesmaster ");
        $row1 = $query1->row_array();
        $salesMasterId = $row1['MAX(salesMasterId)'];

        for ($i = 1; $i <= $count; $i++) {
            $product_name = $this->input->post('product_name' . $i);
            if ($product_name != ""):
                $query2 = $this->db->query("SELECT productBatchId, rate FROM stockposting WHERE productBatchId='$product_name'");
                if ($query2->num_rows() > 0) {
                    $row2 = $query2->row_array();
                    $productBatchId = $row2['productBatchId'];
                } else {
                    $productBatchId = "";
                }

                $data4[] = array(
                    'disburseMasterId' => $salesMasterId,
                    'productBatchId' => $productBatchId,
                    'disburseInvoiceNo' => $invoice,
                    'inwardQuantity' => 0,
                    'outwardQuantity' => $this->input->post('qty' . $i),
                    'unitId' => $this->input->post('unit' . $i),
                    'rate' => $this->input->post('rate' . $i),
                    'discountrate' => $this->input->post('discountrate' . $i),
                    'discounts' => $this->input->post('discounts' . $i),
                    'date' => date('Y-m-d h:i:s'),
                    'stock_type' => 2,
                    'destination_type' => $this->input->post('disburse_type'),
                    'destinationId' => $this->input->post('disburse_to'),
                    'customerId' => $this->input->post('customer_name')
                );

            endif;
        }
        $status = $this->db->insert_batch('stockposting', $data4);
        if ($status):
            $this->session->set_userdata('add', 'Disbursed add successfully');
        else:
            $this->session->set_userdata('notadd', 'Failed to Disbursed add');
        endif;
        redirect('Inventory/vouchar_print?salesMasterId=' . $salesMasterId);
    }

    function vouchar_print() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Print Vouchar';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'disburse';
        $id = $this->input->get('salesMasterId');
        $data['allinfo'] = $this->db->query("SELECT salesmaster.* FROM salesmaster WHERE salesMasterId='$id'")->row();
        $data['disburse_info'] = $this->db->query("SELECT stockposting.*,product_sub_list.sublist_name,product_sub_list.pack_size,product_unit.unit_name FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId JOIN product_unit ON product_unit.id=stockposting.unitId WHERE disburseMasterId='$id' ")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/print_vouchar', $data);
        $this->load->view('common/footer', $data);
    }

    function discountlimit() {
        $userrole = $this->session->userdata('user_role');
        $typeid = $this->input->POST('disburse_type');
        $discount = $this->input->POST('discount');
        if ($userrole == '1'):
            $discountlimit = '30';
        elseif ($userrole == '2'):
            $discountlimit = $this->db->query("SELECT discount_upto FROM disburse_type WHERE id='$typeid'")->row()->discount_upto;
        endif;
        if ($discount > $discountlimit):
            echo 'false';
        else:
            echo 'true';
        endif;
    }

    function challan_list() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Challan List';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'challan';
        $data['challan_list'] = $this->common->viewAll('challan');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/challan_list', $data);
        $this->load->view('common/footer', $data);
    }

    function add_challan() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Create Challan';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'challan';

        $data['product_list'] = $this->db->query("SELECT * FROM product_sub_list order by id ASC")->result();
        $data['subproductlist'] = $this->common->viewAll('product_sub_list');
        $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3' ")->result();
        $data['customerall'] = $this->db->query("SELECT * FROM customer  ")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/add_challan', $data);
        $this->load->view('common/footer', $data);
    }

    function challan_insert() {
        $expire_date = $this->input->post('expire_date');
        $exdate = date("Y-m-d", strtotime($expire_date));
        $user_id = $this->session->userdata('user_id');
        $productid = $this->input->post('productname');
        $challandata = [
            'invoiceno' => $this->input->post('invoiceno'),
            'parchase' => $this->input->post('parchase'),
            'order_date' => $this->input->post('order_date'),
            'challan_type' => $this->input->post('challan_type'),
            'dealerid' => $this->input->post('dealerid'),
            'customerid' => $this->input->post('customerid'),
            'create_date' => date('Y-m-d'),
            'created_by' => $user_id
        ];
        $this->db->insert('challan', $challandata);
        $maxid = $this->db->query("SELECT MAX(id) AS max FROM challan ")->row()->max;
        $productid = $this->input->post('productid');

        foreach ($productid as $i => $prod):
            $detailsData = [
                'challanmaster' => $maxid,
                'productid' => $prod,
                'qty' => $this->input->post('qty')[$i],
                'batch' => $this->input->post('batch')[$i],
                'exdate' => $this->input->post('exdate')[$i]
            ];

            $status = $this->db
                    ->insert('challan_details', $detailsData);
        endforeach;
        $this->db->query("DELETE FROM `challan_details` WHERE `qty`='0'");
        redirect('Inventory/challan_print?challanid=' . $maxid);
    }

    function challan_print() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Print Challan';
        $data['active_menu'] = 'inventory';
        $data['sub_menu'] = 'challan';
        $id = $this->input->get('challanid');
        $data['allinfo'] = $this->db->query("SELECT challan.* FROM challan WHERE id='$id'")->row();
        $data['challan_info'] = $this->db->query("SELECT challan_details.*,product_sub_list.sublist_name,product_sub_list.pack_size FROM challan_details JOIN product_sub_list ON product_sub_list.id=challan_details.productid WHERE challanmaster='$id' ")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('inventory/print_challan', $data);
        $this->load->view('common/footer', $data);
    }

    function delete_invoice() {
        $masterid = $this->input->post('master_id');

        $voucharno = $this->db->query("SELECT salesInvoiceNo FROM salesmaster WHERE salesMasterId='$masterid'")->row()->salesInvoiceNo;

        $this->db->where('voucherNumber', $voucharno);
        $this->db->delete('ledgerposting');

        $this->db->where('salesMasterId', $masterid);
        $this->db->delete('salesmaster');

        $this->db->where('disburseMasterId', $masterid);
        $status = $this->db->delete('stockposting');
        if ($status):
            $this->session->set_userdata('add', 'Disbursed Delete successfully');
        else:
            $this->session->set_userdata('notadd', 'Failed to Delete,try again');
        endif;
        redirect('Inventory/disburse_list');
    }

}
