<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('user_role') != NULL && $this->session->userdata('user_pin') != NULL) {
            redirect('dashboard');
        } else {
            $data['title'] = 'User Login';
            // $this->load->view('common/header', $data);
            $this->load->view('login', $data);
            $this->load->view('common/footer', $data);
        }
    }

}
