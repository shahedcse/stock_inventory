<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    /**
     * Accounts constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('common');

        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 4, 5])) {
            redirect('auth');
        }
    }

    public function order_list() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 4])) {
            redirect('auth');
        }
        $data['title'] = 'Order List';
        $data['active_menu'] = 'SR';
        $data['sub_menu'] = 'orderlist';
        $data['userrole'] = $this->session->userdata('user_role');
        $userid = $this->session->userdata('user_id');
        $userrole = $data['userrole'];
        if ($userrole == 3 || $userrole == 4):
            $data['order_details'] = $this->db->query("SELECT `ordermaster`.*,users.full_name,customer.name,status.status_name,status.color FROM ordermaster JOIN users ON users.id=ordermaster.dealer_id JOIN customer ON customer.id=ordermaster.customer_id JOIN status ON status.id=ordermaster.status WHERE  ordermaster.order_by='$userid' AND ordermaster.status!='4'  order by ordermaster.status ASC")->result();
        else:
            $data['order_details'] = $this->db->query("SELECT `ordermaster`.*,users.full_name,customer.name,status.status_name,status.color FROM ordermaster JOIN users ON users.id=ordermaster.dealer_id JOIN customer ON customer.id=ordermaster.customer_id JOIN status ON status.id=ordermaster.status AND ordermaster.status!='4' order by ordermaster.status ASC ")->result();
        endif;
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('order/order_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function order_listcancel() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 4])) {
            redirect('auth');
        }
        $data['title'] = 'Cancel Order List';
        $data['active_menu'] = 'SR';
        $data['sub_menu'] = 'cancelorder';
        $data['userrole'] = $this->session->userdata('user_role');
        $userid = $this->session->userdata('user_id');
        $userrole = $data['userrole'];
        if ($userrole == 3 || $userrole == 4):
            $data['order_details'] = $this->db->query("SELECT `ordermaster`.*,users.full_name,customer.name,status.status_name,status.color FROM ordermaster JOIN users ON users.id=ordermaster.dealer_id JOIN customer ON customer.id=ordermaster.customer_id JOIN status ON status.id=ordermaster.status WHERE ordermaster.status='4' AND  ordermaster.order_by='$userid'")->result();
        else:
            $data['order_details'] = $this->db->query("SELECT `ordermaster`.*,users.full_name,customer.name,status.status_name,status.color FROM ordermaster JOIN users ON users.id=ordermaster.dealer_id JOIN customer ON customer.id=ordermaster.customer_id JOIN status ON status.id=ordermaster.status WHERE ordermaster.status='4' ")->result();
        endif;
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('order/cancelorder_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function neworder() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 4])) {
            redirect('auth');
        }
        $data['title'] = 'New Order';
        $data['active_menu'] = 'SR';
        $data['sub_menu'] = 'neworder';
        $data['userrole'] = $this->session->userdata('user_role');
        $data['customer_list'] = $this->common->viewAll('customer');
        $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3'")->result();
        $data['product_list'] = $this->db->query("SELECT * FROM product_sub_list order by id ASC")->result();


        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('order/neworder', $data);
        $this->load->view('common/footer', $data);
    }

    function order_insert() {
        $productid = $this->input->post('productid');
        $qty = $this->input->post('qty');

        $orderData = [
            'dealer_id' => $this->input->post('dealer_id'),
            'order_type' => $this->input->post('order_type'),
            'customer_id' => $this->input->post('customer_name'),
            'order_by' => $this->session->userdata('user_id'),
            'status' => 1,
            'total_amount' => $this->input->post('total_amount'),
            'created_date' => date('Y-m-d h:i:s')
        ];

        $this->db
                ->insert('ordermaster', $orderData);
        $maxidqr = $this->db->query("SELECT MAX(id) AS MAX FROM ordermaster ");
        $maxid = $maxidqr->row()->MAX;

        foreach ($productid as $i => $prod):
            $detailsData = [
                'ordermaster_id' => $maxid,
                'product_id' => $prod,
                'quantity' => $qty[$i],
                'total' => $this->input->post('total')[$i]
            ];

            $status = $this->db
                    ->insert('orderdetails', $detailsData);
        endforeach;
        $this->db->query("DELETE FROM `orderdetails` WHERE `quantity`='0'");
        if ($status):
            $this->session->set_userdata('add', 'Order request added Successfully');
        else:
            $this->session->set_userdata('notadd', 'Order request added failed');
        endif;
        redirect('Order/order_list');
    }

    function vouchar_create() {


        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 4])) {
            redirect('auth');
        }
        $data['title'] = 'Order View';
        $data['active_menu'] = 'SR';
        $data['sub_menu'] = 'orderlist';
        $data['userrole'] = $this->session->userdata('user_role');
        $orderid = $this->input->get('orderid');

        $data['orderdmaster'] = $this->db->query("SELECT * FROM ordermaster  WHERE id='$orderid'")->row();
        $data['orderdetails'] = $this->db->query("SELECT orderdetails.*,product_sub_list.sublist_name  FROM orderdetails  JOIN product_sub_list ON product_sub_list.id=orderdetails.product_id WHERE ordermaster_id='$orderid' AND quantity !=''")->result();

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('order/order_preview', $data);
        $this->load->view('common/footer', $data);
    }

    function order_cancel() {
        $id = $this->input->post('order');
        $cancedata = array(
            'status' => 4
        );
        $this->db->where('id', $id);
        $status = $this->db->update('ordermaster', $cancedata);
        if ($status):
            $this->session->set_userdata('add', 'Order Cancel Successfully');
        else:
            $this->session->set_userdata('notadd', 'Order Cancel failed');
        endif;
        redirect('Order/order_list');
    }

    function order_delivered() {
        $id = $this->input->post('order1');
        $cancedata = array(
            'status' => 3
        );
        $this->db->where('id', $id);
        $status = $this->db->update('ordermaster', $cancedata);
        if ($status):
            $this->session->set_userdata('add', 'Order Delivered Successfully');
        else:
            $this->session->set_userdata('notadd', 'Order Delivered failed');
        endif;
        redirect('Order/order_list');
    }

}
