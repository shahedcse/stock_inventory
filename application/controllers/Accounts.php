<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

    /**
     * Accounts constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('common');

        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 5])) {
            redirect('auth');
        }
    }

    public function additionalCost() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 5])) {
            redirect('auth');
        }

        $data['title'] = 'Additional Costs';
        $data['active_menu'] = 'accounts';
        $data['sub_menu'] = 'ad_cost';
        $data['costs'] = [];
        $data['category'] = $this->db->query("SELECT * FROM cost_category order by category_name")->result();

        $data['costs'] = $this->db->query("select substr(date, 1, 7) yr_mon, count(*) num_cost ,SUM(amount) AS total from additional_cost group by yr_mon order by yr_mon DESC ")->result();
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('accounts/additionalCost', $data);
        $this->load->view('common/footer', $data);
    }

    public function cost_details() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 5])) {
            redirect('auth');
        }

        $data['title'] = 'Costs Details';
        $data['active_menu'] = 'accounts';
        $data['sub_menu'] = 'ad_cost';
        $data['costs'] = [];
        $month = $this->input->get('month');
        $data['category'] = $this->db->query("SELECT * FROM cost_category order by category_name")->result();

        $data['costs'] = $this->db->query("select * FROM additional_cost where date LIKE '%$month%' order by date desc ")->result();


        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('accounts/additionalCost_details', $data);
        $this->load->view('common/footer', $data);
    }

    public function cost_category() {
        if (!in_array($this->session->userdata('user_role'), [1, 2, 5])) {
            redirect('auth');
        }

        $data['title'] = 'Costs Category';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'costcat';
        $data['costlist'] = $this->common->viewAll('cost_category');
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('accounts/cost_category', $data);
        $this->load->view('common/footer', $data);
    }

    public function newCost() {
        if (!in_array($this->session->userdata('user_role'), [1, 5])) {
            redirect('auth');
        }

        $date = $this->input->post('dateOfCost');
        $dateOfCost = date("Y-m-d", strtotime($date));

        $purpose = $this->input->post('addPurpose');
        $amount = $this->input->post('addAmount');

        $data = [
            'date' => $dateOfCost,
            'purpose' => $purpose,
            'amount' => $amount,
            'remarks' => $this->input->post('remarks')
        ];

        $status = $this->db
                ->insert('additional_cost', $data);
        if ($status):
            $this->session->set_userdata('add', 'Add Additonal Cost Successfully ');
        else:
            $this->session->set_userdata('notadd', 'Failed to add Additonal Cost');
        endif;
        redirect('accounts/additionalcost');
    }

    public function editCost($id = false) {
        if (!in_array($this->session->userdata('user_role'), [1, 5])) {
            $response = [
                'success' => false,
                'error' => true,
                'message' => 'You are not authorized to do this.'
            ];

            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        if (!$id) {
            $response = [
                'success' => false,
                'error' => true,
                'message' => 'Please provide an ID.'
            ];

            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        $getCost = $this->db
                ->order_by('created_at', 'desc')
                ->where('id', $id)
                ->get('additional_cost')
                ->row();

        if (!$getCost) {
            $response = [
                'success' => false,
                'error' => true,
                'message' => 'No records found.'
            ];

            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        $response = [
            'success' => true,
            'error' => false,
            'message' => 'One Record found.',
            'costs' => $getCost
        ];

        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function updateCost() {
        if (!in_array($this->session->userdata('user_role'), [1])) {
            redirect('auth');
        }

        $id = $this->input->post('additionalCostId');
        $date = $this->input->post('editDateOfCost');
        $dateOfCost = date_format(date_create_from_format('d/m/Y', $date), 'Y-m-d');

        $purpose = $this->input->post('editPurpose');
        $amount = $this->input->post('editAmount');

        $this->db
                ->set('date', $dateOfCost)
                ->set('purpose', $purpose)
                ->set('amount', $amount)
                ->where('id', $id)
                ->update('additional_cost');

        redirect('accounts/additionalcost');
    }

    public function deleteCost($id = false) {
        if (!in_array($this->session->userdata('user_role'), [1])) {
            $response = [
                'success' => false,
                'error' => true,
                'message' => 'You are not authorized to do this.'
            ];

            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        if (!$id) {
            $response = [
                'success' => false,
                'error' => true,
                'message' => 'Please provide an ID.'
            ];

            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        $this->db
                ->where('id', $id)
                ->delete('additional_cost');

        $response = [
            'success' => true,
            'error' => false,
            'message' => 'Successfully deleted your selected additional cost.'
        ];

        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function profitAnalysis() {
        if (!in_array($this->session->userdata('user_role'), [1])) {
            redirect('auth');
        }

        $data['title'] = 'Profit Analysis';
        $data['active_menu'] = 'accounts';
        $data['sub_menu'] = 'profitanalysis';
        $data['costs'] = [];

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('accounts/profitAnalysis', $data);
        $this->load->view('common/footer', $data);
    }

    public function monthOfProfit() {
        if (!in_array($this->session->userdata('user_role'), [1])) {
            redirect('auth');
        }

        $month = $this->input->post('month');
        $response = [];

        $response['debit'] = (double) $this->db
                        ->select('SUM(accounts.delivery_cost) as debit')
                        ->from('accounts')
                        ->join('request', 'request.id = accounts.request_id')
                        ->where('accounts.collect_frmod', 1)
                        ->where('DATE_FORMAT(request.inhousedate, "%m/%Y") =', $month)
                        ->get()
                        ->row()
                ->debit;



        $additionalCost = $this->db
                        ->select('SUM(amount) as additionalCost')
                        ->from('additional_cost')
                        ->where('DATE_FORMAT(additional_cost.date, "%m/%Y") =', $month)
                        ->get()
                        ->row()
                ->additionalCost;

        $response['credit'] = $additionalCost;

        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function ledgerbalance() {
        if (!in_array($this->session->userdata('user_role'), [1, 2,5])) {
            redirect('auth');
        }
        $data['title'] = 'Ledger Analysis';
        $data['active_menu'] = 'accounts';
        $data['sub_menu'] = 'ledger';
        $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3' ")->result();
        $data['customerall'] = $this->db->query("SELECT * FROM customer  ")->result();


        $ledgertype = $this->input->post('ledger_type');
        $dealer = $this->input->post('dealerid');
        $customer = $this->input->post('customerid');

        $data['dealerdata'] = '';
        $data['customerdata'] = '';
        if ($ledgertype == 1):
            $data['dealerinfo'] = $this->db->query("SELECT  * FROM users  where id='$dealer'")->row();
            $dealerQr = $this->db->query("SELECT * FROM ledgerposting WHERE ledgerId_dealer='$dealer' order by date ASC");
            $data['dealerdata'] = $dealerQr->result();
            $data['datatotald'] = $this->db->query("SELECT SUM(credit) AS cr,SUM(debit) AS dr FROM ledgerposting WHERE  ledgerId_dealer='$dealer'")->row();
        elseif ($ledgertype == 2):
            $data['customerinfo'] = $this->db->query("SELECT  * FROM customer  where id='$customer'")->row();
            $customerQr = $this->db->query("SELECT * FROM ledgerposting  WHERE ledgerId_customer='$customer' order by date ASC ");
            $data['customerdata'] = $customerQr->result();
            $data['datatotalc'] = $this->db->query("SELECT SUM(credit) AS cr,SUM(debit) AS dr FROM ledgerposting WHERE  ledgerId_customer='$customer'")->row();
        endif;
        $data['type'] = $ledgertype;
        $data['dealer'] = $dealer;
        $data['customer'] = $customer;
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('accounts/ledger_balance', $data);
        $this->load->view('common/footer', $data);
    }

    function balance_adjust_dealer() {
        $dealerid = $this->input->post('dealeridinput');
        $adjustdate = date("Y-m-d", strtotime($this->input->post('adjustdate')));
        $adjustfor = $this->input->post('adjustfor');
        $amount = $this->input->post('amount');
        $paymenttype = $this->input->post('payment_type');
        if ($adjustfor == 1):
            $response = [
                'debit' => $amount,
                'payment_type' => $paymenttype,
                'ledgerId_dealer' => $dealerid,
                'date' => $adjustdate
            ];
        elseif ($adjustfor == 2):
            $response = [
                'credit' => $amount,
                'payment_type' => $paymenttype,
                'ledgerId_dealer' => $dealerid,
                'date' => $adjustdate
            ];
        endif;

        $status = $this->db->insert('ledgerposting', $response);
        if ($status):
            $this->session->set_userdata('add', 'Amount Adjust successfully');
        else:
            $this->session->set_userdata('notadd', 'Amount Adjust Failed');
        endif;
        redirect('Accounts/ledgerbalance');
    }

    function balance_adjust_customer() {
        $customerid = $this->input->post('customeridinput');
        $adjustdate = date("Y-m-d", strtotime($this->input->post('adjustdate')));
        $adjustfor = $this->input->post('adjustfor');
        $amount = $this->input->post('amount');
        $paymenttype = $this->input->post('payment_type');
        if ($adjustfor == 1):
            $response = [
                'debit' => $amount,
                'payment_type' => $paymenttype,
                'ledgerId_customer' => $customerid,
                'date' => $adjustdate
            ];
        elseif ($adjustfor == 2):
            $response = [
                'credit' => $amount,
                'payment_type' => $paymenttype,
                'ledgerId_customer' => $customerid,
                'date' => $adjustdate
            ];
        endif;

        $status = $this->db->insert('ledgerposting', $response);
        if ($status):
            $this->session->set_userdata('add', 'Amount Adjust successfully');
        else:
            $this->session->set_userdata('notadd', 'Amount Adjust Failed');
        endif;
        redirect('Accounts/ledgerbalance');
    }

}
