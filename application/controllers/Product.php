<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common');
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3])) {
            redirect('auth');
        }
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Product List';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'product_list';
        $data['productlist'] = $this->common->viewAll('product_category');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/product_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_product() {
        $data['base_url'] = $this->config->item('base_url');
        $data = [
            'product_name' => $this->input->post('product_name'),
            'created_date' => date('Y-m-d')
        ];
        $status = $this->db->insert('product_category', $data);
        if ($status):
            $this->session->set_userdata('add', 'Product Category Added Successfully ');
        else:
            $this->session->set_userdata('notadd', 'Failed to add Product Category');
        endif;
        redirect('Product');
    }

    public function product_sublist() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Product Sub List';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'product_sub_list';
        $data['productsub_list'] = $this->db->query("SELECT product_sub_list.*,product_category.product_name FROM product_sub_list JOIN product_category ON product_category.id=product_sub_list.product_cat")->result();
        $data['productlist'] = $this->common->viewAll('product_category');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/product_sub_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function product_unit() {
        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Product Unit';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'product_unit';

        $data['productunit'] = $this->common->viewAll('product_unit');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/product_unit', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_sub_product() {
        $data['base_url'] = $this->config->item('base_url');
        $datasub = [
            'product_cat' => $this->input->post('product_name'),
            'sublist_name' => $this->input->post('sub_category'),
            'pack_size' => $this->input->post('pack_size'),
            'created_date' => date('Y-m-d')
        ];
        $status1 = $this->db->insert('product_sub_list', $datasub);
        $p_max = $this->db->query("SELECT MAX(id) AS max FROM product_sub_list")->row()->max;
        $datap = [
            'productId' => $p_max,
            'created_date' => date('Y-m-d')
        ];
        $status = $this->db->insert('productbatch', $datap);

        if ($status):
            $this->session->set_userdata('add', 'Product  Added Successfully ');
        else:
            $this->session->set_userdata('notadd', 'Failed to add Product ');
        endif;
        redirect('Product/product_sublist');
    }

}
