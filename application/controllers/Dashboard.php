<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common');
        if (!in_array($this->session->userdata('user_role'), [1, 2, 3, 5])) {
            redirect('auth');
        }
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), [1, 2, 3, 4, 5])):
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Dashboard';
            $data['active_menu'] = 'dashboard';
            $data['sub_menu'] = '';
            $datem = date('Y-m');

            $data['total_prod'] = $this->db->query("SELECT COUNT(id) AS totl FROM product_sub_list")->row()->totl;
            $data['total_dealer'] = $this->db->query("SELECT COUNT(id) AS totl FROM users where role='3'")->row()->totl;
            $data['total_vendor'] = $this->db->query("SELECT COUNT(vendorId) AS totl FROM vendor")->row()->totl;
            $data['total_sale'] = $this->db->query("SELECT SUM(total_amount) AS totlm FROM salesmaster WHERE date LIKE '%$datem%'")->row()->totlm;
            $monthfromdate = date('Y-m', strtotime(date('Y-m') . " -11 month"));
            $data['totaldata'] = $data['period'] = [];

            for ($m = 0; $m <= 11; $m++) {
                $subval = '+' . $m . 'month';

                $data_month_query = date('Y-m', strtotime($monthfromdate . " $subval"));
                $data_month_show = date('M-y', strtotime($monthfromdate . " $subval"));

                $totalamount = $this->db
                                ->select('SUM(total_amount) as total')
                                ->from('salesmaster')
                                ->where("DATE_FORMAT(date, '%Y-%m') = '$data_month_query'")
                                ->get()
                                ->row()->total;
                $round = round($totalamount, 2);

                array_push($data['totaldata'], $round);
                array_push($data['period'], "$data_month_show");
            }
            $stockQr = $this->db->query("SELECT DISTINCT(`productBatchId`) AS productid,product_sub_list.sublist_name AS productname FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId WHERE stockposting.destinationId  ");
            $data['stock_info'] = $stockQr->result();

            $credit = $this->db->query("SELECT SUM(credit) AS credits FROM `ledgerposting` ")->row()->credits;
            $debit = $this->db->query("SELECT SUM(debit) AS debits FROM `ledgerposting` ")->row()->debits;
            $data['totaldue'] = round($credit - $debit);

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('dashboard/dashboard', $data);
            $this->load->view('common/footer', $data);
        else:
            redirect('auth');
        endif;
    }

}
