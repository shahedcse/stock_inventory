<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
    }

    public function index() {

        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Main Stock';
            $data['active_menu'] = 'inventory';
            $data['sub_menu'] = 'mainstock';
            $stockQr = $this->db->query("SELECT DISTINCT(`productBatchId`) AS productid,product_sub_list.sublist_name AS productname,product_sub_list.pack_size FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId ");
            $data['stock_info'] = $stockQr->result();

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('inventory/main_stock', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

    public function dealer_stock() {

        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Dealer Stock';
            $data['active_menu'] = 'inventory';
            $data['sub_menu'] = 'dealerstock';
            

            $stockQr = $this->db->query("SELECT DISTINCT(`productBatchId`) AS productid,product_sub_list.sublist_name AS productname FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId WHERE stockposting.destinationId  ");
            $data['stock_info'] = $stockQr->result();

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('inventory/dealer_stock', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

}
