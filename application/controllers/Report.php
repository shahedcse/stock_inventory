<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
    }

    public function delivery_report() {

        if (in_array($this->session->userdata('user_role'), array(1, 2,5))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Delivery Report';
            $data['active_menu'] = 'report';
            $data['sub_menu'] = 'deliveryreport';
            $data['role'] = $this->session->userdata('user_role');

            $data['subproductlist'] = $this->common->viewAll('product_sub_list');
            $data['disburse_list'] = $this->common->viewAll('disburse_type');
            $data['customer_list'] = $this->common->viewAll('customer');
            $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3'")->result();


            $disbursetype = $this->input->post('disburse_type');
            $product = $this->input->post('product');
            $datefrom = $this->input->post('datefrom');
            $dateto = $this->input->post('dateto');
            if (!empty($datefrom) && !empty($dateto)):
                $startdate = date("Y-m-d", strtotime($datefrom));
                $enddate = date("Y-m-d", strtotime($dateto));
            else:
                $startdate = date('Y-m-d');
                $enddate = date('Y-m-d');
            endif;
            if (!empty($disbursetype) && !empty($product)):
                $detailsdata = $this->db
                        ->query("SELECT salesmaster.*,stockposting.*,"
                        . "disburse_type.type_name,product_sub_list.sublist_name,product_sub_list.pack_size FROM salesmaster "
                        . " JOIN stockposting ON stockposting.disburseMasterId=salesmaster.salesMasterId"
                        . " JOIN disburse_type ON disburse_type.id=salesmaster.disburse_type"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " where (salesmaster.date BETWEEN '$startdate' AND '$enddate') AND salesmaster.disburse_type='$disbursetype' AND stockposting.productBatchId='$product' ");
            elseif (!empty($product)):
                $detailsdata = $this->db
                        ->query("SELECT salesmaster.*,stockposting.*,"
                        . "disburse_type.type_name,product_sub_list.sublist_name,product_sub_list.pack_size FROM salesmaster "
                        . " JOIN stockposting ON stockposting.disburseMasterId=salesmaster.salesMasterId"
                        . " JOIN disburse_type ON disburse_type.id=salesmaster.disburse_type"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " where (salesmaster.date BETWEEN '$startdate' AND '$enddate') AND stockposting.productBatchId='$product' ");
            elseif (!empty($disbursetype)):
                $detailsdata = $this->db
                        ->query("SELECT salesmaster.*,stockposting.*,"
                        . "disburse_type.type_name,product_sub_list.sublist_name,product_sub_list.pack_size FROM salesmaster "
                        . " JOIN stockposting ON stockposting.disburseMasterId=salesmaster.salesMasterId"
                        . " JOIN disburse_type ON disburse_type.id=salesmaster.disburse_type"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " where (salesmaster.date BETWEEN '$startdate' AND '$enddate') AND salesmaster.disburse_type='$disbursetype' ");

            else:
                $detailsdata = $this->db
                        ->query("SELECT salesmaster.*,stockposting.*,"
                        . "disburse_type.type_name,product_sub_list.sublist_name,product_sub_list.pack_size FROM salesmaster "
                        . " JOIN stockposting ON stockposting.disburseMasterId=salesmaster.salesMasterId"
                        . " JOIN disburse_type ON disburse_type.id=salesmaster.disburse_type"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " where (salesmaster.date BETWEEN '$startdate' AND '$enddate') ");
            endif;
            $data['stockdata'] = $detailsdata->result();
            $data['type'] = $disbursetype;
            $data['product'] = $product;
            $data['datestart'] = $startdate;
            $data['dateend'] = $enddate;

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('report/delivery_report', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

    public function purchase_report() {

        if (in_array($this->session->userdata('user_role'), array(1, 2,5))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Purchase Report';
            $data['active_menu'] = 'report';
            $data['sub_menu'] = 'purchasereport';
            $data['role'] = $this->session->userdata('user_role');

            $data['subproductlist'] = $this->common->viewAll('product_sub_list');
            $data['disburse_list'] = $this->common->viewAll('disburse_type');
            $data['customer_list'] = $this->common->viewAll('customer');
            $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3'")->result();



            $product = $this->input->post('product');
            $datefrom = $this->input->post('datefrom');
            $dateto = $this->input->post('dateto');
            if (!empty($datefrom) && !empty($dateto)):
                $startdate = date("Y-m-d", strtotime($datefrom));
                $enddate = date("Y-m-d", strtotime($dateto));
            else:
                $startdate = date('Y-m-d', strtotime('-30 days'));
                $enddate = date('Y-m-d');
            endif;
            if (!empty($product)):
                $detailsdata = $this->db
                        ->query("SELECT purchasemaster.*,stockposting.*,"
                        . "product_sub_list.sublist_name,product_sub_list.pack_size,product_category.product_name FROM purchasemaster "
                        . " JOIN stockposting ON stockposting.purchaseMasterId=purchasemaster.purchaseMasterId"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " JOIN product_category ON product_category.id=purchasemaster.prod_group"
                        . " where (purchasemaster.purchase_date BETWEEN '$startdate' AND '$enddate')  AND stockposting.stock_type='1' AND stockposting.productBatchId='$product' ");
            else:
                $detailsdata = $this->db
                        ->query("SELECT purchasemaster.*,stockposting.*,"
                        . "product_sub_list.sublist_name,product_sub_list.pack_size,product_category.product_name  FROM purchasemaster "
                        . " JOIN stockposting ON stockposting.purchaseMasterId=purchasemaster.purchaseMasterId"
                        . " JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId"
                        . " JOIN product_category ON product_category.id=purchasemaster.prod_group"
                        . " where (purchasemaster.purchase_date BETWEEN '$startdate' AND '$enddate') AND stockposting.stock_type='1' ");
            endif;
            $data['purchasedata'] = $detailsdata->result();

            $data['datestart'] = $startdate;
            $data['dateend'] = $enddate;

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('report/purchase_report', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

    public function stock_report() {

        if (in_array($this->session->userdata('user_role'), array(1, 2,5))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['title'] = 'Stock Report';
            $data['active_menu'] = 'report';
            $data['sub_menu'] = 'productstock';
            $data['role'] = $this->session->userdata('user_role');

            $data['subproductlist'] = $this->common->viewAll('product_sub_list');
            $data['disburse_list'] = $this->common->viewAll('disburse_type');
            $data['customer_list'] = $this->common->viewAll('customer');
            $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3'")->result();



            $product = $this->input->post('product');
            $datefrom = $this->input->post('datefrom');
            $dateto = $this->input->post('dateto');
            if (!empty($datefrom) && !empty($dateto)):
                $startdate = date("Y-m-d", strtotime($datefrom));
                $enddate = date("Y-m-d", strtotime($dateto));
            else:
                $startdate = date('Y-m-d');
                $enddate = date('Y-m-d');
            endif;
            if (!empty($product)):
                $stockQr = $this->db->query("SELECT productBatchId AS productid,product_sub_list.sublist_name AS productname,product_sub_list.pack_size FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId WHERE (stockposting.date BETWEEN '$startdate' AND '$enddate') AND productBatchId='$product' group by product_sub_list.id ");
            else:
                $stockQr = $this->db->query("SELECT DISTINCT(`productBatchId`) AS productid,product_sub_list.sublist_name AS productname,product_sub_list.pack_size  FROM stockposting JOIN product_sub_list ON product_sub_list.id=stockposting.productBatchId group by product_sub_list.id ");
            endif;
            $data['stock_info'] = $stockQr->result();

            $data['datestart'] = $startdate;
            $data['dateend'] = $enddate;

            $this->load->view('common/header', $data);
            $this->load->view('common/sidebar', $data);
            $this->load->view('report/stock_report', $data);
            $this->load->view('common/footer', $data);
        else :
            redirect('auth');
        endif;
    }

}
