<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserInfo extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('common');
    }

    public function index() {

        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'User List';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'Userinfo';
        $data['users_list'] = $this->common->userData();
        $data['dealer_list'] = $this->db->query("SELECT * FROM users WHERE role='3' ")->result();
        $data['role'] = $this->common->viewAll('userrole');
        $data['zone'] = $this->common->viewAll('division_zone');
        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/userinfo', $data);
        $this->load->view('common/footer', $data);
    }

    public function vendor_list() {

        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Vendor List';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'vendor';
        // $data['users_list'] = $this->common->userData();
        $data['vendordata'] = $this->common->viewAll('vendor');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/vendor_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function customer_list() {

        $data['base_url'] = $this->config->item('base_url');
        $data['title'] = 'Customer List';
        $data['active_menu'] = 'admin';
        $data['sub_menu'] = 'customer';
        // $data['users_list'] = $this->common->userData();
        $data['customerdata'] = $this->common->viewAll('customer');

        $this->load->view('common/header', $data);
        $this->load->view('common/sidebar', $data);
        $this->load->view('admin/customer_list', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_vendor() {
        $post['vendorName'] = $this->input->post('vendorName');
        $post['address'] = $this->input->post('address');
        $post['country'] = $this->input->post('country');
        $post['emailId'] = $this->input->post('emailId');
        $post['mobileNumber'] = $this->input->post('mobileNumber');
        $post['created_by'] = $this->session->userdata('user_id');
        $status = $this->common->add('vendor', $post);
        if ($status):
            $this->session->set_userdata('add', 'Vendor add Successfully');
        else:
            $this->session->set_userdata('notadd', 'Failed to add Vendor');
        endif;

        redirect('Userinfo/vendor_list');
    }

    public function addUser() {
        $post['name'] = $this->input->post('name');
        $post['password'] = $this->input->post('password');
        $post['full_name'] = $this->input->post('full_name');
        $post['email'] = $this->input->post('email');
        $post['phone'] = $this->input->post('phone');
        $post['role'] = $this->input->post('role');
        $post['dealer_point_name'] = $this->input->post('dealer_point_name');
        $post['dealerid'] = $this->input->post('dealerid');
        $post['zonename'] = $this->input->post('zonename');
        $post['present_address'] = $this->input->post('present_address');
        $post['status'] = 1;
        $post['created_date'] = date('Y-m-d h:i:s');
        $post['created_by'] = $this->session->userdata('user_id');


        $target_dir = "assets/images/uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imgFile = $_FILES['fileToUpload']['name'];
        if (empty($imgFile)) {
            $post['image_path'] = 'default.jpg';
        } elseif (!empty($imgFile)) {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                $post['image_path'] = basename($_FILES["fileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        }

        $status = $this->common->add('users', $post);

        if ($status):
            $this->session->set_userdata('add', 'User add Successfully');
        else:
            $this->session->set_userdata('notadd', 'Failed to add user');
        endif;

        redirect('userinfo');
    }

    function getDetailsData() {
        $id = $this->input->post('id');
        $query1 = $this->db->query("SELECT users.*,userrole.role_name FROM users JOIN userrole ON  users.role=userrole.id WHERE users.id = '$id'  ");
        $dataresult = $query1->row();
        $outputData = array(
            'userdata' => $dataresult,
        );

        echo json_encode($outputData);
    }

    function getcustomerData() {
        $id = $this->input->post('id');
        $query1 = $this->db->query("SELECT * FROM customer WHERE id = '$id'  ");
        $dataresult = $query1->row();
        $outputData = array(
            'userdata' => $dataresult,
        );

        echo json_encode($outputData);
    }

    public function updateUser() {
        $id = $this->input->post('edit_userid');
        $target_dir = "assets/images/uploads";
        $target_file = $target_dir . basename($_FILES["editfileToUpload"]["name"]);
        $imgFile = $_FILES['editfileToUpload']['name'];
        if (empty($imgFile)) {
            $data = [
                'full_name' => $this->input->post('editfull_name'),
                'name' => $this->input->post('editname'),
                'phone' => $this->input->post('editphone'),
                'present_address' => $this->input->post('editpresent_address'),
                'role' => $this->input->post('editrole'),
                'dealer_point_name' => $this->input->post('editdealer_point_name'),
                'zonename' => $this->input->post('editzonename'),
                'phone' => $this->input->post('editphone'),
            ];
        } elseif (!empty($imgFile)) {
            if (move_uploaded_file($_FILES["editfileToUpload"]["tmp_name"], $target_file)) :
                $image_path = basename($_FILES["editfileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
            $data = [
                'full_name' => $this->input->post('editfull_name'),
                'name' => $this->input->post('editname'),
                'phone' => $this->input->post('editphone'),
                'present_address' => $this->input->post('editpresent_address'),
                'role' => $this->input->post('editrole'),
                'dealer_point_name' => $this->input->post('editdealer_point_name'),
                'zonename' => $this->input->post('editzonename'),
                'phone' => $this->input->post('editphone'),
                'image_path' => $image_path
            ];
        }

        $this->db->where('id', $id);
        $update = $this->db->update('users', $data);

        if ($update):
            $this->session->set_userdata('add', 'User Edit Successfully');
        else:
            $this->session->set_userdata('notadd', 'Failed to edit user');
        endif;

        redirect('userinfo');
    }

    public function updatePass() {
        $id = $this->input->post('id');
        $npassword = $this->input->post('npassword');

        $userdata = array(
            'password' => $npassword,
        );

        $status = $this->common->update('users', $id, $userdata);
        if ($status):
            $this->session->set_userdata('add', 'Password updated successfully !!!');
        else:
            $this->session->set_userdata('notadd', 'Password updated fail. Try again !!!');
        endif;
        redirect('Userinfo');
    }

    public function delete_user() {
        $sessionuser = $this->session->userdata('user_id');
        $id = $this->input->post('user_id');
        if ($sessionuser == $id):
            $this->session->set_userdata('notadd', 'Sorry you cant delete your own id');
            redirect('Userinfo');
        else:
            $status = $this->common->delete('users', $id, 'id');
            if ($status):
                $this->session->set_userdata('add', 'User Deleted successfully !!!');
            else:
                $this->session->set_userdata('notadd', 'user deletion failed. Try again !!!');
            endif;
            redirect('Userinfo');

        endif;
    }

    public function delete_vendor() {
        $id = $this->input->post('vendor_id');
        $status = $this->common->delete('vendor', $id, 'vendorId');
        if ($status):
            $this->session->set_userdata('add', 'Vendor Deleted successfully !!!');
        else:
            $this->session->set_userdata('notadd', 'Vendor deletion failed. Try again !!!');
        endif;
        redirect('Userinfo/vendor_list');
    }

    public function checkPin() {
        echo $this->common->checkPin($this->input->get('pin'));
    }

    public function changePin() {
        $pin = $this->input->post('pin');
        $id = $this->input->post('id');
        $getdata = $this->common->getPin($id, $pin);
        $getpin = $getdata ? true : false;
        echo json_encode($getpin);
    }

    public function addcustomer() {
        $post['name'] = $this->input->post('name');
        $post['phone'] = $this->input->post('phone');
        $post['address'] = $this->input->post('address');
        $post['created_date'] = date('Y-m-d');
        $post['created_by'] = $this->session->userdata('user_id');
        $this->common->add('customer', $post);
        redirect('Inventory/add_disburse');
    }

    public function addcustomer2() {
        $post['name'] = $this->input->post('name');
        $post['phone'] = $this->input->post('phone');
        $post['address'] = $this->input->post('address');
        $post['created_date'] = date('Y-m-d');
        $post['created_by'] = $this->session->userdata('user_id');
        $this->common->add('customer', $post);
        redirect('Order/neworder');
    }

    public function addcustomer3() {
        $post['name'] = $this->input->post('name');
        $post['phone'] = $this->input->post('phone');
        $post['address'] = $this->input->post('address');
        $post['created_date'] = date('Y-m-d');
        $post['created_by'] = $this->session->userdata('user_id');
        $this->common->add('customer', $post);
        redirect('Userinfo/customer_list');
    }

    public function updatecustomer() {
        $id = $this->input->post('customerid');
        $post['name'] = $this->input->post('editname');
        $post['phone'] = $this->input->post('editphone');
        $post['address'] = $this->input->post('editaddress');
        $status = $this->common->update('customer', $id, $post);

        if ($status):
            $this->session->set_userdata('add', 'Customer Info Edited Successfully');
        else:
            $this->session->set_userdata('notadd', 'Customer Info Edited failed');
        endif;
        redirect('Userinfo/customer_list');
    }

}
