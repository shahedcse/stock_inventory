<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-10 col-lg-offset-1">

                    <div class="card">
                        <a href="<?= base_url('Inventory/challan_list'); ?>">
                            <input type="button" class="btn btn-subscribe pull-left" value="<< Back Challan List" />
                        </a>
                        <input type="button" class="btn btn-success pull-right" onclick="printDiv('printvouchar')" value="print Challan" />


                        <div class="body" id="printvouchar">
                            <table style="font-size: 14px;  width: 100%; " class="table table-striped table-bordered table-hover ">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <div class="col-sm-3">
                                                        <img height="55px;" width="140px;" src="<?= base_url('assets/images/company.jpg'); ?>">
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div style="text-align: center">
                                                            <h4>CHALLAN COPY</h4>
                                                        </div>
                                                        <div style="border: 1px solid gray; height: 1px;"> </div>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-lg-12">
                                                    <div class="col-sm-8 pull-left">
                                                        <?php
                                                        if (!empty($allinfo->dealerid)):
                                                            $dealer_name = $this->db->query("SELECT full_name,present_address FROM users WHERE id='$allinfo->dealerid' ")->row();
                                                            $name = $dealer_name->full_name;
                                                            $address = $dealer_name->present_address;

                                                        else:
                                                            $customer_name = $this->db->query("SELECT name,address FROM customer WHERE id='$allinfo->customerid' ")->row();
                                                            $name = $customer_name->name;
                                                            $address = $customer_name->address;

                                                        endif;
                                                        ?>
                                                        <p>
                                                            Name:<?= $name; ?> <br>
                                                            Address:<?= $address; ?> <br>
                                                            Invoice No:<?= $allinfo->invoiceno; ?><br>
                                                            Purchase Order No:<?= $allinfo->parchase; ?> <br>
                                                            Purchase Order Date:<?= $allinfo->order_date; ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4 pull-right">
                                                        <p>
                                                            Challan No:<?= '100' . $allinfo->id; ?> <br>
                                                            Date:<?= $allinfo->create_date; ?> <br>


                                                        </p>
                                                    </div>
                                                </div><br><br><br><br>
                                                <div class="adv-table">
                                                    <table class="display table table-bordered table-striped edit-table" id="cloudAccounting1" style="font-size: 12px;padding-top: 10px;">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">Sl</th>
                                                                <th class="text-center">Product Name</th>

                                                                <th class="text-center">Qty</th>
                                                                <th class="text-center">Batch</th>
                                                                <th class="text-center">Expire Date</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="addprintrowoffice">
                                                            <?php
                                                            $id = 0;
                                                            foreach ($challan_info as $value):
                                                                $id++;
                                                                ?>
                                                                <tr id="rowoffice1">
                                                                    <td style="text-align: center"><?= $id; ?> </td>
                                                                    <td style="text-align: center"><?= $value->sublist_name; ?> </td>
                                                                    <td style="text-align: center"><?= $value->qty; ?> </td>
                                                                    <td style="text-align: center; "><?= $value->batch; ?></td>
                                                                    <td style="text-align: center; "><?= $value->exdate; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>


                                                        </tbody>

                                                    </table><br><br>



                                                    <table style="float:left;width:100%; text-align: center; font-size: 12px">
                                                        <thead>
                                                            <tr>

                                                                <th colspan="2">Receiver Signature:</th>

                                                                <th  colspan="2"> Authorized Signature:</th>
                                                            </tr>
                                                        </thead>
                                                    </table><br><br><br><br><br><br><br><br><br>

                                                    <table style="float:left; text-align: center; margin: 10px 5px 0px 5px; font-size: 10px">
                                                        <tbody>
                                                            <tr>
                                                                <td><b> House-5, Road-8, Nikunja, Dhaka-1229</b><td>
                                                                <td ><b>Thanks for choosing little Feat as a business partner.</b></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
        function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

    document.body.innerHTML = originalContents;
    }
</script>



