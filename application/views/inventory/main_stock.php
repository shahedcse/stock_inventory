<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Main Go-down Stock.
                            </h2>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <th>SL.</th>
                                            <th >Product Name</th>
                                            <th>Quantity</th>
                                            <th>DP Rate</th>
                                            <th style="background-color:#c3f0ca; ">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $sl = 0;
                                        foreach ($stock_info as $value):
                                            $sl++;
                                            $stockuser = $this->db->query("SELECT GROUP_CONCAT(`id`) as user FROM users WHERE `role`IN(1,2) ")->row()->user;
                                            $stockinqr = $this->db->query("SELECT SUM(inwardQuantity) AS qty FROM stockposting WHERE productBatchId='$value->productid' AND destinationId IN ($stockuser) AND stock_type='1'")->row()->qty;
                                            $stockoutqr = $this->db->query("SELECT SUM(outwardQuantity) AS qty1 FROM stockposting WHERE productBatchId='$value->productid' AND stock_type='2'")->row()->qty1;
                                            $stock = ($stockinqr - $stockoutqr);
                                            $detailsQr = $this->db->query("SELECT rate,product_unit.unit_name  FROM stockposting JOIN product_unit ON product_unit.id=stockposting.unitId WHERE productBatchId='$value->productid'");
                                            $rate = $detailsQr->row()->rate;
                                            ?>
                                            <tr>
                                                <td><?= $sl; ?></td>
                                                <td ><?= $value->productname ?> - <?= $value->pack_size; ?></td>
                                                <td><?= $stock . '  - ' . $detailsQr->row()->unit_name ?></td>
                                                <td><?= $rate; ?></td>
                                                <td style="background-color:#c3f0ca; "><?= $stock * $rate ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
