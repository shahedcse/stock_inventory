<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Product Challan List
                                <a href="<?= base_url('Inventory/add_challan'); ?>">
                                    <button type="button" class=" btn bg-cyan pull-right">Add Challan</button>
                                </a>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Challan No.</th>
                                            <th>Invoice No.</th>
                                            <th>Challan Date</th>
                                            <th>Challan To</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        foreach ($challan_list AS $value):
                                            if (!empty($value->dealerid)):
                                                $dealer_name = $this->db->query("SELECT full_name,present_address FROM users WHERE id='$value->dealerid' ")->row();
                                                $name = $dealer_name->full_name;
                                            else:
                                                $customer_name = $this->db->query("SELECT name,address FROM customer WHERE id='$value->customerid' ")->row();
                                                $name = $customer_name->name;
                                            endif;
                                            ?>
                                            <tr>
                                                <td><?= '100' . $value->id ?></td>
                                                <td><?= $value->invoiceno ?></td>
                                                <td><?= $value->create_date ?></td>
                                                <td><?= $name ?></td>
                                                <td>
                                        <li><a href="<?= base_url('Inventory/challan_print?challanid=' . $value->id); ?>"style="color:green;" class=" waves-effect waves-block">Print Challan</a></li>
                                        </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
