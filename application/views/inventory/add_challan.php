<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-10 col-lg-offset-1" >
                    <div class="card" style="background-color: #f1c8d9;">
                        <div class="header">
                            <h2>
                                Add Challan Information.
                            </h2>
                        </div>

                        <div class="body">
                            <form class="form-horizontal" action="<?= base_url('Inventory/challan_insert'); ?>" method="POST">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Challan Create Date :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line" id="bs_datepicker_container">
                                                <input type="text" readonly="" class="form-control"  value="<?= date('Y-m-d'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Purchase Order No:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="parchase"   name="parchase" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Purchase Order Date:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" id="order_date"   name="order_date" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Invoice No:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="invoiceno"   name="invoiceno" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Challan For :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <div class="demo-radio-button">
                                                    <input name="challan_type" onclick=" return makechange()" type="radio" value="1" id="radio_2">
                                                    <label for="radio_2">Dealer Point</label>
                                                    <input name="challan_type" onclick=" return makechange()" type="radio" value="2" id="radio_1">
                                                    <label for="radio_1">Customer Point</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix" id="dealer"  style="display:none;">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Dealer Name :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" id="dealerid" name="dealerid">
                                                    <option value="">-Select Dealer Point -</option>
                                                    <?php foreach ($dealer_list AS $value): ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->full_name; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix" id="customer"  style="display:none;">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Dealer Name :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick " data-live-search="true" id="customerid" name="customerid">
                                                    <option value="">-- Select Customer Point --</option>

                                                    <?php foreach ($customerall AS $value): ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->name; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-bordered table-sm" style="cursor: pointer;color:black; border: 1px solid black;font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Product Name</th>
                                            <th>Qty</th>
                                            <th>Batch</th>
                                            <th>Expire Date</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $id = 0;
                                        foreach ($product_list AS $value):
                                            $id++;
                                            ?>
                                            <tr>
                                                <td><?= $id ?></td>
                                                <td>
                                                    <?= $value->sublist_name; ?> - <?= $value->pack_size; ?>
                                                    <input  id="productid" name="productid[]" type="hidden" class="form-control" value="<?= $value->id; ?>">
                                                </td>

                                                <td><input   onkeypress="return isNumberKey(event)"id="qty" name="qty[]" type="number" class="form-control" value="0"></td>
                                                <td><input id="batch" name="batch[]" style="background-color: palegreen;"  type="text" class="form-control"></td>
                                                <td><input id="exdate" name="exdate[]" style="background-color: honeydew;"  type="date" class="form-control"></td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>







                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 pull-right">
                                        <a href="<?= base_url('Inventory/challan_list'); ?>">
                                            <button type="button" class="btn btn-default m-t-15 waves-effect">Cancel</button>
                                        </a>
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Create Challan</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
    function calculate_total() {
        var pprice = $("#unit_price").val();
        var quantity = $("#quantity").val();

        var total = Math.round(pprice * quantity);
        $("#total_price").val(total);
    }

    function makechange() {
        var type = $('input[name=challan_type]:checked').val();
        if (type == 1) {
            $("#dealer").show();
            $("#customer").hide();
        }
        else {
            $("#dealer").hide();
            $("#customer").show();
        }
    }


    $("#prod_cat").change(function() {
        var prod_cat = $("#prod_cat").val();
        var dataString = 'prod_cat=' + prod_cat;
        $.ajax({
            type: 'POST',
            url: "<?= base_url('Inventory/get_sub'); ?>",
            data: dataString,
            success: function(responseData) {
                //  console.log(responseData);
                $("#prod_sublist").html(responseData);
                $('#prod_sublist').selectpicker('refresh');
            }
        });
    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

