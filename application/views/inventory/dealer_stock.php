<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                All dealer Current Stock.
                            </h2>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <th>SL.</th>
                                            <th style="background-color:#d597ce; ">Product Name</th>
                                            <th>Quantity</th>
                                            <th style="background-color:#c3f0ca; ">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $sl = 0;
                                        foreach ($stock_info as $value):
                                            $sl++;
                                            $dealeruser = $this->db->query("SELECT GROUP_CONCAT(`id`) as user FROM users WHERE `role`=3 ")->row()->user;
                                            $stockin = $this->db->query("SELECT SUM(outwardQuantity) AS qty FROM stockposting WHERE productBatchId='$value->productid' AND destinationId IN($dealeruser) AND stock_type='2'")->row()->qty;
                                            $stockout = $this->db->query("SELECT SUM(outwardQuantity) AS qty FROM stockposting WHERE productBatchId='$value->productid' AND destinationId IN($dealeruser) AND stock_type='3'")->row()->qty;
                                            $stock = ($stockin - $stockout);

                                            $detailsQr = $this->db->query("SELECT rate,product_unit.unit_name  FROM stockposting JOIN product_unit ON product_unit.id=stockposting.unitId WHERE productBatchId='$value->productid'");
                                            $rate = $detailsQr->row()->rate;
                                            ?>
                                            <tr>
                                                <td><?= $sl; ?></td>
                                                <td style="background-color:#d597ce; "><?= $value->productname ?></td>
                                                <td><?= $stock . '  - ' . $detailsQr->row()->unit_name ?></td>

                                                <td style="background-color:#c3f0ca; "><?= $stock * $rate ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
