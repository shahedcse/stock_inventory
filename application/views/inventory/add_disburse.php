<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                            <form class="form-horizontal" action="<?= base_url('Inventory/disburse_add'); ?>" method="POST">
                                <div class="row clearfix" style="background-color: honeydew; padding-top: 10px;">
                                    <div class="col-sm-4">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 form-control-label">
                                            <label for="email_address_2">Disburse Cat:</label>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select class="form-control show-tick" id="disburse_type" name="disburse_type" required="">
                                                        <option value="">-select -</option>
                                                        <?php foreach ($disburse_list AS $value): ?>
                                                            <option value="<?= $value->id; ?>"><?= $value->type_name; ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="trade">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                            <label for="email_address_2">Customer:</label>
                                        </div>
                                        <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select class="form-control show-tick" data-live-search="true" id="customer_name" name="customer_name">
                                                        <option value="">-select -</option>
                                                        <?php foreach ($customer_list AS $value): ?>
                                                            <option value="<?= $value->id; ?>"><?= $value->name; ?>(<?= $value->phone; ?>)<br>
                                                            <?= $value->address; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <button type="button" onclick="customeradd_modal()" class="btn-success">New Customer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="dealer" style="display:none">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                            <label for="email_address_2">Dealer Point:</label>
                                        </div>
                                        <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select class="form-control show-tick" id="disburse_to" name="disburse_to" >
                                                        <option value="">-select -</option>
                                                        <?php foreach ($dealer_list AS $value): ?>
                                                            <option value="<?= $value->id; ?>"><?= $value->dealer_point_name; ?> - (<?= $value->full_name; ?>) </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                            <label for="email_address_2"> Date :</label>
                                        </div>
                                        <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="date" id="invoice_date" name="invoice_date" class="form-control" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="background-color: #f8f9f9; color:black">
                                    <div class="row clearfix">
                                        <div class="col-sm-5">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                                <label for="email_address_2">Product:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <select class="form-control show-tick selectpicker" data-live-search="true" id="product_sub" name="product_sub" required="">
                                                            <option value="">-- Please select --</option>
                                                            <?php foreach ($subproductlist AS $value): ?>
                                                                <option value="<?= $value->id ?>"><?= $value->id ?> - <?= $value->sublist_name ?> - <?= $value->pack_size ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="col-sm-3">
                                                <input type="hidden" class="form-control" name="unit" id="unit" placeholder="Unit">
                                                <input type="text" class="form-control"  id="unitshow" placeholder="Unit" readonly>

                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text"id="qty" placeholder="Qty" name="qty" class="form-control" >
                                                <input type="hidden" id="qtymsg1" name="qtymsg1" class="form-control" >
                                                <span id="qtymsg"></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" readonly="" id="rate"  placeholder="Rate"name="rate" class="form-control">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="col-sm-6">
                                                <select class="form-control" name="discount_type" id="discount_type">
                                                    <option value="1">Discount Percentage</option>
                                                    <option value="2">Discount Amount</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" id="discounts"  onkeyup="calculate_drate()"  placeholder="Discount"name="discounts" class="form-control">(%)
                                            </div>
                                            <div class="col-sm-3" style="display:none;" id="discountamount_div">
                                                <input type="text" id="discount_amount"onkeyup="calculate_discount_rate()" onkeyup="calculate_drate2()"  placeholder="Amount" name="discount_amount" class="form-control">(Tk.)
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="col-sm-6">
                                                <input type="text" readonly="" id="discountrate"  placeholder="Discount Rate"name="discountrate" class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="adddisburse" class="btn-lg btn-primary">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="display table table-bordered table-striped " id="cloudAccounting1">
                                        <thead style="background-color:#b1dfee;color:black">
                                            <tr>
                                                <th></th>
                                                <th>Product Name</th>
                                                <th>Qty</th>
                                                <th>Unit</th>
                                                <th>Rate</th>
                                                <th>Discount(%)</th>
                                                <th>Discount Rate</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody id="addnewrow">
                                        </tbody>                            
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th id="viewtotalqty">  </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th id="viewtotalamount">  </th>
                                        </tr>
                                    </table>
                                </div>


                                <div class="row clearfix " >
                                    <div class="col-sm-6  pull-left">
                                        <div class="col-sm-12">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                                <label for="email_address_2">Delivery by:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control"id="delivery_by" name="delivery_by" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                                <label for="email_address_2">Remarks:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <textarea name="remarks" id="remarks" cols="30" rows="5" class="form-control no-resize" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6  pull-right">
                                        <ul class="unstyled amounts">
                                            <li  style="color:green;font-size: 20px;"class="text-center"><strong>Total Amount : </strong><span id="total_amout" class="align-left "> 0.00 </span> <input name="total_amout" id="totalamount" class="total_amout" value="0" type="hidden"/></li><br>
                                            <li class="text-center" id="charge"><strong>Deli. Charge : </strong> <input name="delivery_charge" onkeyup="calculate_amount()" onkeypress="return isNumberKey(event);"  id="delivery_charge" class="align-left " type="text"/></li>

                                            <li class="text-center"><strong>Net Amount : </strong> <input name="netamount"   id="netamount" class="align-left " readonly="" value="" type="text"/></li>
                                            <li class="text-center"><strong>Paid Amount : </strong> <input name="paid_amount" onkeyup="checkdue();" id="paid_amount" class="align-left " value=""required=""type="text"/></li>
                                            <li  style="color:red;"class="text-center"><strong>Due Amount : </strong> <input name="due_amount" id="due_amount" class="align-left " value="" readonly="" type="text"/></li>
                                            <li style="display: none" class="text-center"><strong>Grand Total :</strong> <span  id="grandtotal" class="align-right ">0.00</span><input name="grandtotal" class="grandtotal" type="hidden"/></li>
                                            <li  style="display: none" class="text-center"><strong>Net Amount :</strong> <span id="net_amout" class="align-right ">0.00</span><input name="net_amout" class="net_amout" type="hidden"/><input name="count_product" class="count_product" type="hidden"/></li>
                                        </ul>

                                    </div>

                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 pull-right">
                                        <a href="#">
                                            <button type="button" class="btn btn-default m-t-15 waves-effect">Cancel</button>
                                        </a>
                                        <button type="submit" id="disburse" class="btn btn-success m-t-15 waves-effect">Submit Disburse</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Add New Customer Info. :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/addcustomer'); ?>" method="POST">
                <div class="modal-body " >
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="name" required="" name="name" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required=""  id="phone" name="phone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="address" id="address" cols="20" rows="1" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function customeradd_modal() {
        $('#usermodal').modal("show");
    }

    function calculate_discount_rate() {
        var rate = $("#rate").val();
        var amount = $("#discount_amount").val();
        var discount_rate = rate - amount;
        var percentage = (amount * 100) / rate;
        var dpercentage = percentage.toFixed(1);
        $("#discounts").val(dpercentage);
        $("#discountrate").val(discount_rate);
    }

    $("#discount_type").change(function() {
        var type = $("#discount_type").val();
        if (type == '2') {
            $("#discounts").prop("readonly", true);
            $("#discountamount_div").show("");
        }
        else {
            $("#discounts").prop("readonly", false);
            $("#discountamount_div").hide("");
        }

    });

    function calculate_drate() {
        var discounts = $("#discounts").val();
        var rate = $("#rate").val();
        var percent = (rate * discounts) / 100;
        var amount = Math.round(rate - percent);
        $("#discountrate").val(amount);
    }

    function calculate_amount() {

        var totalamount = $("#totalamount").val();
        var charge = $("#delivery_charge").val();

        var amount = Math.round(totalamount);
        if (charge == "") {
            var netamount = parseInt(amount);
        }
        else {
            var netamount = parseInt(amount) + parseInt(charge);
        }
        $("#netamount").val(netamount);
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


    $("#disburse_type").change(function() {
        var cate = $("#disburse_type").val();
        if (cate == '5') {
            $("#dealer").show("");
            $("#trade").hide("");
        }
        else {
            $("#trade").show("");
            $("#dealer").hide("");
        }
//        if (cate == '3') {
//            $("#charge").show("");
//        }
//        else {
//            $("#charge").hide("");
//        }

    });

    function checkdue() {
        var tatlamount = $("#netamount").val();
        var paidamount = $("#paid_amount").val();
        var due = tatlamount - paidamount;
        $("#due_amount").val(due);
    }


    var count = 0;
    var countqtyrate = 0;
    var totalqty = 0;
    $("#adddisburse").click(function() {

        var product_name = $("#product_sub").val();
        var qty = $("#qty").val();
        var unit = $("#unit").val();
        var rate = $("#rate").val();
        var discounts = $("#discounts").val();
        var discountrate = $("#discountrate").val();
        var qtymsg = $("#qtymsg").text();

        if (qtymsg == "Limit exceeds!") {
            //  alert('Sorry !Stock is exceeds')
            return false;
        }
        if (product_name == "") {
            $("#product_name").css('border-color', 'red');
            return false;
        }
        if (discountrate == "") {
            $("#discountrate").css('border-color', 'red');
            return false;
        }
        if (qty == "") {
            $("#qty").css('border-color', 'red');
            return false;
        }
        if (rate == "") {
            $("#rate").css('border-color', 'red');
            return false;
        }

        count = count + 1;

        var dataString = "count=" + count + "&product_name=" + product_name + "&qty=" + qty + "&unit=" + unit + "&rate=" + rate + "&discounts=" + discounts + "&discountrate=" + discountrate;

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Inventory/add_view_table'); ?>",
            data: dataString,
            success: function(data) {
                var outputdata = JSON.parse(data);
                var rowdata = outputdata.rowdata;
                var printdataoffice = outputdata.printdataoffice;
                var printdatacustomer = outputdata.printdatacustomer;
                $("#addnewrow").append(rowdata);
                $("#addprintrowoffice").append(printdataoffice);
                $("#addprintrowcustomer").append(printdatacustomer);

                //3rd part
                var qty2 = parseFloat($("#qty" + count).val());
                var rate2 = parseFloat($("#discountrate" + count).val());
                totalqty += qty2;

                var qtyrate = qty2 * rate2;
                var previoustotal = $(".total_amout").val();
                countqtyrate = parseFloat(previoustotal);
                countqtyrate = countqtyrate + qtyrate;  //Total quantity * rate

                var grandtotal = parseFloat(countqtyrate);  //total amount
                var discount = $("#discount").val();
                var transport = parseFloat($("#transport").val());
                var net_amout = parseFloat(grandtotal) + parseFloat(transport) - discount;

                var grandtotal = Math.ceil(grandtotal * 2) / 2;
                var net_amout = Math.ceil(net_amout * 2) / 2;

                document.getElementById("total_amout").innerHTML = grandtotal;
                $(".total_amout").val(grandtotal);

                document.getElementById("grandtotal").innerHTML = grandtotal;
                $(".grandtotal").val(grandtotal);

                document.getElementById("net_amout").innerHTML = net_amout;
                $(".net_amout").val(net_amout);


                //For print
                $("#netvalues1").text(grandtotal);
                $("#netvalues2").text(grandtotal);
                grandtotal = parseFloat(grandtotal).toFixed(2);

                if (grandtotal > 0) {
                    var grandtotaltext = grandtotal + " dr.";
                } else if (grandtotal < 0) {
                    var grandtotaltext = Math.abs(grandtotal) + " cr.";
                } else {
                    var grandtotaltext = "0.00";
                }
                $("#invoiceamount1").text(grandtotaltext);
                $("#invoiceamount2").text(grandtotaltext);

                $("#viewtotalamount").text(grandtotal);
                $("#viewtotalqty").text(totalqty);
                $("#totalqty1").text(totalqty);
                $("#totalqty2").text(totalqty);





                //count_product
                $(".count_product").val(count);
            }
        });

        //clear value fields
        var product = $("#product_sub option:selected").val();
        if (product == product_name) {
            $("#product_sub option:selected").hide();
        }
        //    $("#product_sub").val("");
        $("#qty").val("");
        $("#unit").val("");
        $("#unitshow").val("");
        $("#rate").val("");
        $("#discounts").val("");
        $("#discountrate").val("");
    });




    $("#product_sub").change(function() {
        var product_id = $(this).val();
        var cate = $("#disburse_type").val();
        if (cate == "") {
            alert('Please Select Didburse category First');
            return false;
        }

        //alert(product_id);
        var dataString = "product_id=" + product_id + "&cat_type=" + cate;

        //for unit
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Inventory/unit_name'); ?>",
            data: dataString,
            success: function(data) {
                var res = data.split(",");
                $("#unitshow").val(res[1]);
                $("#unit").val(res[0]);
            }
        });

        //for rate
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Inventory/product_salerate'); ?>",
            data: dataString,
            success: function(data) {
                $("#rate").val(data);
            }
        });
        //for rate
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Inventory/product_qty'); ?>",
            data: dataString,
            success: function(data) {
                var range = data;
                if (range == "") {
                    range = 0;
                }
                $("#qtymsg").text("Available " + range);
                $("#qtymsg1").val(range);
                $("#qtymsg").css('color', 'green');
                $("#qty").change(function() {
                    var qtyval = parseInt($("#qty").val());
                    if (qtyval > range) {      //Allow Sales if Stock is zero or less than zero!
                        $("#qty").css('border-color', 'red');
                        $("#qtymsg").text("Limit exceeds!");
                        $("#qtymsg").css('color', 'red');
                        //  $("#disburse").attr("disabled", true);
                    } else {
                        $("#qty").css('border-color', '#898990');
                        $("#qtymsg").text("Available " + range);
                        $("#qtymsg").css('color', 'green');
                        //  $("#disburse").attr("disabled", false);
                    }
                });
            }
        });
    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

</script>

