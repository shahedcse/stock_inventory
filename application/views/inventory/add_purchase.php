<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-10 col-lg-offset-1">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Add Purchase Information.
                            </h2>
                        </div>

                        <div class="body">
                            <form class="form-horizontal" action="<?= base_url('Inventory/purchase_insert'); ?>" method="POST">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Purchase Date :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line" id="bs_datepicker_container">
                                                <input type="date" id="purchase_date" name="purchase_date" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Invoice No. :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="purchaseInvoiceNo" name="purchaseInvoiceNo" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Purchase From(Vendor) :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" id="purchase_from" name="purchase_from" required="">
                                                    <option value="">-- Please select --</option>
                                                    <?php foreach ($vendorlist AS $value): ?>
                                                        <option value="<?= $value->vendorName ?>"><?= $value->vendorName ?></option>
                                                    <?php endforeach; ?>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Product Group :</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" id="prod_cat" name="prod_cat" required="">
                                                    <option value="">-- Please select --</option>
                                                    <?php foreach ($productlist AS $value): ?>
                                                        <option value="<?= $value->id ?>"><?= $value->product_name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Product Name:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" data-live-search="true"   name="prod_sublist" id="prod_sublist" required="">
                                                    <option value="">--Please select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Product Unit:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" id="unit" data-live-search="true" name="unit" required="">
                                                    <option value="">-- Please select --</option>
                                                    <?php foreach ($unitlist AS $value): ?>
                                                        <option value="<?= $value->id ?>"><?= $value->unit_name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Purchase Price Unit(৳):</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="unit_price" onkeypress="return isNumberKey(event);" name="unit_price" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">DP(৳):</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sale_price" onkeypress="return isNumberKey(event);" name="sale_price" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">TP(৳):</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="trade_price" onkeypress="return isNumberKey(event);" name="trade_price" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">MRP(৳):</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="mrp" onkeypress="return isNumberKey(event);" name="mrp" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Quantity:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="quantity" onkeyup="calculate_total();" onkeypress="return isNumberKey(event);"  name="quantity" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 form-control-label">
                                        <label for="email_address_2">Total Price:</label>
                                    </div>
                                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="total_price" readonly="" name="total_price" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 pull-right">
                                        <a href="<?= base_url('Inventory/purchase_list'); ?>">
                                            <button type="button" class="btn btn-default m-t-15 waves-effect">Cancel</button>
                                        </a>
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
    function calculate_total() {
        var pprice = $("#unit_price").val();
        var quantity = $("#quantity").val();

        var total = Math.round(pprice * quantity);
        $("#total_price").val(total);
    }


    $("#prod_cat").change(function() {
        var prod_cat = $("#prod_cat").val();
        var dataString = 'prod_cat=' + prod_cat;
        $.ajax({
            type: 'POST',
            url: "<?= base_url('Inventory/get_sub'); ?>",
            data: dataString,
            success: function(responseData) {
                //  console.log(responseData);
                $("#prod_sublist").html(responseData);
                $('#prod_sublist').selectpicker('refresh');
            }
        });
    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

