<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Home Delivery Invoice List

                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table  class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Invoice date</th>
                                            <th>Invoice </th>
                                            <th>Disburse To </th>
                                            <th>Delivery by </th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($home_list as $value):
                                            if (!empty($value->disburse_to)):
                                                $dealer_name = $this->db->query("SELECT dealer_point_name FROM users WHERE id='$value->disburse_to' ")->row()->dealer_point_name;
                                            else:
                                                $customer_name = $this->db->query("SELECT name FROM customer WHERE id='$value->customer' ")->row()->name;
                                            endif;
                                            ?>
                                            <tr>
                                                <td><?= date("d-m-Y", strtotime($value->date)); ?></td>
                                                <td><?= $value->salesInvoiceNo; ?></td>
                                                <td>
                                                    <?php
                                                    if (!empty($value->disburse_to)):
                                                        echo $dealer_name;
                                                    else:
                                                        echo $customer_name;
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><?= $value->delivery_by; ?></td>
                                                <td><?= $value->netamount; ?></td>
                                                <?php if (empty($value->delivery_status)): ?>
                                                    <td style="background-color: yellow">Processing<br>
                                                        <?php
                                                        if (empty($value->payment_status)):
                                                            echo '<button type="button" class="btn bg-red waves-effect">UNPAID</button>';
                                                        else:
                                                            echo '<button type="button" class="btn bg-light-green waves-effect">PAID</button>';
                                                        endif;
                                                        ?>
                                                    </td>

                                                <?php else: ?>
                                                    <td style="background-color: green;color:#fff">Delivered<br>
                                                        <?php
                                                        if (empty($value->payment_status)):
                                                            echo '<button type="button" class="btn bg-red waves-effect">UNPAID</button>';
                                                        else:
                                                            echo '<button type="button" class="btn bg-light-green waves-effect">PAID</button>';
                                                        endif;
                                                        ?>
                                                    </td>
                                                <?php endif; ?>
                                                <td>
                                                    <?php if (empty($value->delivery_status)): ?>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                ACTION <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="<?= base_url('Inventory/vouchar_print?salesMasterId=' . $value->salesMasterId); ?>"style="color:green;" class=" waves-effect waves-block">Print Invoice</a></li>
                                                                <li><a href="#" onclick="statuschange('<?= $value->salesMasterId; ?>')"style="color:green;" class=" waves-effect waves-block">Delivered</a></li>
                                                                <li><a href="#" onclick="delete_invoice('<?= $value->salesMasterId; ?>')" style="color:red;" class=" waves-effect waves-block">Order Cancel</a></li>
                                                            </ul>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                ACTION <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="statuschange('<?= $value->salesMasterId; ?>')"style="color:green;" class=" waves-effect waves-block">Delivered</a></li>
                                                                <li><a href="<?= base_url('Inventory/vouchar_print?salesMasterId=' . $value->salesMasterId); ?>"style="color:green;" class=" waves-effect waves-block">Print Invoice</a></li>
                                                            </ul>
                                                        </div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

<div class="modal fade " id="deletemodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('Inventory/delete_invoice'); ?>" method="post">
            <div class="modal-content modal-col-red">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="master_id"  name="master_id" class="form-control">
                    <p style="font-size: 20px;">  Are you sure , you want to delete this Order & Invoice ? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade " id="statusmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('Inventory/delevery_status'); ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Are you sure , you want to Change it delivered?</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="master_id2"  name="master_id2" class="form-control">
                    <p style="font-size: 20px;"> Payment Status : </p>
                    <div class="demo-radio-button">
                        <input name="payment_status" class="payment" value="1" type="radio" id="radio_1">
                        <label for="radio_1">PAID</label>
                        <input name="payment_status" value="0" class="payment" type="radio" id="radio_2">
                        <label for="radio_2">UNPAID</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" onclick="return check_payment();" class="btn btn-success waves-effect">Delivered</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function check_payment() {
        var paymentstatus = $(".payment:checked").val();
        if (paymentstatus == '1' || paymentstatus == '0') {
            return true;
        }
        else {
            alert('Please select payment Status');
            return false;
        }
    }


    function delete_invoice(masterid) {
        $('#master_id').val(masterid);
        $('#deletemodal').modal("show");
    }

    function statuschange(masterid) {
        $('#master_id2').val(masterid);
        $('#statusmodal').modal("show");
    }
</script>
