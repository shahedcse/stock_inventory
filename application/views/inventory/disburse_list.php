<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Product Disburse List
                                <a href="<?= base_url('Inventory/add_disburse'); ?>">
                                    <button type="button" class=" btn bg-cyan pull-right">Add Disburse</button>
                                </a>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table  class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Disburse date</th>
                                            <th>Invoice Number</th>
                                            <th>Disburse To </th>
                                            <th> Total Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Disburse date</th>
                                            <th>Invoice Number</th>
                                            <th>Disburse To </th>
                                            <th> Total Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        foreach ($disburse_list as $value):
                                            if (!empty($value->disburse_to)):
                                                $dealer_name = $this->db->query("SELECT dealer_point_name FROM users WHERE id='$value->disburse_to' ")->row()->dealer_point_name;
                                            else:
                                                $customer_name = $this->db->query("SELECT name FROM customer WHERE id='$value->customer' ")->row()->name;
                                            endif;
                                            ?>
                                            <tr>
                                                <td><?= date("F j, Y", strtotime($value->date)); ?></td>
                                                <td><?= $value->salesInvoiceNo; ?></td>
                                                <td>
                                                    <?php
                                                    if (!empty($value->disburse_to)):
                                                        echo $dealer_name;
                                                    else:
                                                        echo $customer_name;
                                                    endif;
                                                    ?></td>
                                                <td><?= $value->netamount; ?></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            ACTION <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?= base_url('Inventory/vouchar_print?salesMasterId=' . $value->salesMasterId); ?>"style="color:green;" class=" waves-effect waves-block">Print Invoice</a></li>
                                                            <li><a href="#" onclick="delete_invoice('<?= $value->salesMasterId; ?>')" style="color:red;" class=" waves-effect waves-block">Delete Invoice</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

<div class="modal fade " id="deletemodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('Inventory/delete_invoice'); ?>" method="post">
            <div class="modal-content modal-col-red">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="master_id"  name="master_id" class="form-control">
                    <p style="font-size: 20px;">  Are you sure , you want to delete this Invoice ? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function delete_invoice(masterid) {
        $('#master_id').val(masterid);
        $('#deletemodal').modal("show");
    }
</script>
