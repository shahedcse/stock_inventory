<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-10 col-lg-offset-1">

                    <div class="card">
                        <a href="<?= base_url('Inventory/disburse_list'); ?>">
                            <input type="button" class="btn btn-subscribe pull-left"  value="<< Back to Disburse List" />
                        </a>&nbsp;&nbsp;&nbsp;
                        <a href="<?= base_url('Inventory/add_disburse'); ?>">
                            <input type="button" class="btn btn-primary pull-left"  value="Add Disburse" />
                        </a>
                        <input type="button" class="btn btn-success pull-right" onclick="printDiv('printvouchar')" value="print vouchar" />
                        <div class="body" id="printvouchar">
                            <?php if ($allinfo->print_type == 1): ?>
                                <table style="font-size: 14px;  width: 100%; " class="table table-striped table-bordered table-hover ">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <div class="col-sm-3">
                                                            <img height="55px;" width="140px;" src="<?= base_url('assets/images/company.jpg'); ?>">
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div style="text-align: center">
                                                                <p>
                                                                    <img height="45px;" width="100px;" src="<?= base_url('assets/images/dia_kidz-283x179.png'); ?>">
                                                                    <br> Contact:<b> +8801914939632.</b>
                                                                    <br> Web: www.littlefeatbd.com
                                                                </p>
                                                            </div>
                                                            <div style="border: 1px solid gray; height: 1px;"> </div>
                                                            <div style="text-align: right">
                                                                <img height="30px;" width="120px;" src="<?= base_url('assets/images/1505477115167095.png'); ?>">
                                                                <br>
                                                                Invoice No: <?= $allinfo->salesInvoiceNo; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div style="text-align: left">
                                                            <?php
                                                            if (!empty($allinfo->disburse_to)):
                                                                $dealer_name = $this->db->query("SELECT full_name,phone,present_address FROM users WHERE id='$allinfo->disburse_to' ")->row();
                                                            else:
                                                                $customer_name = $this->db->query("SELECT name,phone,address FROM customer WHERE id='$allinfo->customer' ")->row();
                                                            endif;
                                                            ?>
                                                            <b>  Disburse To </b>: <span id="customername">
                                                                <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->full_name;
                                                                else:
                                                                    echo $customer_name->name;
                                                                endif;
                                                                ?>
                                                            </span>

                                                            <br>
                                                            <b>  Phone </b>: <span id="salesmanname1">  <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->phone;
                                                                else:
                                                                    echo $customer_name->phone;
                                                                endif;
                                                                ?></span>
                                                            <br>
                                                            <b>  Address </b>: <span id="salesdate1">  <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->present_address;
                                                                else:
                                                                    echo $customer_name->address;
                                                                endif;
                                                                ?></span>
                                                            <br>
                                                            <br>

                                                        </div>
                                                    </div>


                                                    <div class="adv-table">
                                                        <table class="display table table-bordered table-striped edit-table" id="cloudAccounting1" style="font-size: 12px">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Sl</th>
                                                                    <th class="text-left">Product Name</th>
                                                                    <th class="text-center">Qty</th>
                                                                    <th class="text-center">Unit</th>
                                                                    <th class="text-center">Sale Rate</th>
                                                                    <th class="text-center">Amount</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody id="addprintrowoffice">
                                                                <?php
                                                                $id = 0;
                                                                foreach ($disburse_info as $value):
                                                                    $id++;
                                                                    ?>
                                                                    <tr id="rowoffice1">
                                                                        <td style="text-align: center"><?= $id; ?> </td>
                                                                        <td style="text-align: left"><?= $value->sublist_name; ?> </td>
                                                                        <td style="text-align: center"><?= $value->outwardQuantity; ?> </td>
                                                                        <td style="text-align: center; "><?= $value->unit_name; ?></td>
                                                                        <td style="text-align: center"><?= $value->rate; ?> </td>
                                                                        <td style="text-align: center; "><?= $value->rate * $value->outwardQuantity ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>

                                                                <tr>
                                                                    <td style="text-align: right"colspan="5">Total: </td>
                                                                    <td style="text-align: center"><?= $allinfo->total_amount; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: left"colspan="6">
                                                                        In Words: ( <span id="inword"></span> .)
                                                                    </td>
                                                                </tr>
                                                            </tbody>

                                                        </table>
                                                        <ul class="list-group pull-right">
                                                            <li class="list-group-item">
                                                                <table style=" text-align:right; width:100%; font-size: 12px; font-weight: bold;margin-bottom: 5px;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Product Total Amount: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->total_amount; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Discount: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->discount; ?> %
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Delivery Charge: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->delivery_charge; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b style="font-size: 15px;">Net Amount : </b></td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <b> <?= $allinfo->netamount ?> </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Paid Amount : </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->paid_amount ?> </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>Due Amount: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= ($allinfo->due_amount) ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </li>
                                                        </ul>

                                                        <table style="float:left;width:100%; text-align: center; font-size: 12px">
                                                            <thead>
                                                                <tr>

                                                                    <th colspan="2">Receiver Signature:</th>

                                                                    <th  colspan="2"> Authorized Signature:</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        <table style="float:left; text-align: center; margin: 10px 5px 0px 5px; font-size: 12px">
                                                            <tbody>
                                                                <tr>
                                                                    <td><b> House-5, Road-8, Nikunja, Dhaka-1229</b><td>
                                                                    <td><b>Thanks for choosing little Feat as a business partner.</b></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            <?php else: ?>
                                <table style="font-size: 14px;  width: 100%; " class="table table-striped table-bordered table-hover ">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <div class="col-sm-3">
                                                            <img height="55px;" width="140px;" src="<?= base_url('assets/images/company.jpg'); ?>">
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div style="text-align: center">
                                                                <p>
                                                                    <img height="45px;" width="100px;" src="<?= base_url('assets/images/dia_kidz-283x179.png'); ?>">
                                                                    <br> Contact:<b> +8801914939632.</b>
                                                                    <br> Web: www.littlefeatbd.com
                                                                </p>
                                                            </div>
                                                            <div style="border: 1px solid gray; height: 1px;"> </div>
                                                            <div style="text-align: right">
                                                                <img height="30px;" width="120px;" src="<?= base_url('assets/images/1505477115167095.png'); ?>">
                                                                <br>
                                                                Invoice No: <?= $allinfo->salesInvoiceNo; ?><br>
                                                                Invoice Date: <?= $allinfo->date; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div style="text-align: left">
                                                            <?php
                                                            if (!empty($allinfo->disburse_to)):
                                                                $dealer_name = $this->db->query("SELECT full_name,phone,present_address FROM users WHERE id='$allinfo->disburse_to' ")->row();
                                                            else:
                                                                $customer_name = $this->db->query("SELECT name,phone,address FROM customer WHERE id='$allinfo->customer' ")->row();
                                                            endif;
                                                            ?>
                                                            <b>  Disburse To </b>: <span id="customername">
                                                                <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->full_name;
                                                                else:
                                                                    echo $customer_name->name;
                                                                endif;
                                                                ?>
                                                            </span>

                                                            <br>
                                                            <b>  Phone </b>: <span id="salesmanname1">  <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->phone;
                                                                else:
                                                                    echo $customer_name->phone;
                                                                endif;
                                                                ?></span>
                                                            <br>
                                                            <b>  Address </b>: <span id="salesdate1">  <?php
                                                                if (!empty($allinfo->disburse_to)):
                                                                    echo $dealer_name->present_address;
                                                                else:
                                                                    echo $customer_name->address;
                                                                endif;
                                                                ?></span>
                                                            <br>
                                                            <br>

                                                        </div>
                                                    </div>


                                                    <div class="adv-table">
                                                        <table class="display table table-bordered table-striped edit-table" id="cloudAccounting1" style="font-size: 14px">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Sl</th>
                                                                    <th class="text-left">Product Name</th>
                                                                    <th class="text-center">Qty</th>
                                                                    <th class="text-center">Unit</th>
                                                                    <th class="text-center">
                                                                        <?php
                                                                        if ($allinfo->disburse_type == 1 || $allinfo->disburse_type == 2 || $allinfo->disburse_type == 4):
                                                                            echo 'TP Rate';
                                                                        elseif ($allinfo->disburse_type == 3):
                                                                            echo 'MRP Rate';
                                                                        elseif ($allinfo->disburse_type == 5):
                                                                            echo 'DP Rate';
                                                                        endif;
                                                                        ?>
                                                                    </th>
                                                                    <th class="text-center">Discount</th>
                                                                    <th class="text-center">Discount Rate</th>
                                                                    <th class="text-center">Amount</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody id="addprintrowoffice">
                                                                <?php
                                                                $id = 0;
                                                                foreach ($disburse_info as $value):
                                                                    $id++;
                                                                    ?>
                                                                    <tr id="rowoffice1">
                                                                        <td style="text-align: center"><?= $id; ?> </td>
                                                                        <td style="text-align: left"><?= $value->sublist_name; ?><br>-<?= $value->pack_size; ?> </td>
                                                                        <td style="text-align: center"><?= $value->outwardQuantity; ?> </td>
                                                                        <td style="text-align: center; "><?= $value->unit_name; ?></td>
                                                                        <td style="text-align: center"><?= $value->rate; ?> </td>
                                                                        <td style="text-align: center"><?= $value->discounts; ?> % </td>
                                                                        <td style="text-align: center"><?= $value->discountrate; ?> </td>
                                                                        <td style="text-align: center; "><?= $value->discountrate * $value->outwardQuantity ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>

                                                                <tr>
                                                                    <td style="text-align: right"colspan="7">Total: </td>
                                                                    <td style="text-align: center"><?= $allinfo->total_amount; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: left"colspan="8">
                                                                        In Words: ( <span id="inword"></span> .)
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="text-align: left"colspan="4">
                                                                        Delivery by: <?= $allinfo->delivery_by; ?>
                                                                    </td>

                                                                    <td style="text-align: left"colspan="4">
                                                                        Remarks: <?= $allinfo->remarks; ?>
                                                                    </td>
                                                                </tr>
                                                            </tbody>

                                                        </table>
                                                        <ul class="list-group pull-right">
                                                            <li class="list-group-item">
                                                                <table style=" text-align:right; width:100%; font-size: 12px; font-weight: bold;margin-bottom: 5px;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Product Total Amount: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->total_amount; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Delivery Charge: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->delivery_charge; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="font-size: 15px;color:green;"><span>Net Amount : </span></td>
                                                                            <td id="invoiceamount1" style="text-align:right;font-size: 15px; color:green;">
                                                                                <b> <?= $allinfo->netamount ?> </b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Paid Amount : </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= $allinfo->paid_amount ?> </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>Due Amount: </td>
                                                                            <td id="invoiceamount1" style="text-align:right">
                                                                                <?= ($allinfo->due_amount) ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </li>
                                                        </ul>

                                                        <table style="float:left;width:100%; text-align: center; font-size: 12px">
                                                            <thead>
                                                                <tr>

                                                                    <th colspan="2">Receivers Signature:</th>

                                                                    <th  colspan="2"> Authorized Signature:</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        <table style="float:left; text-align: center; margin: 10px 5px 0px 5px; font-size: 12px">
                                                            <tbody>
                                                                <tr>
                                                                    <td><b> House-5, Road-8, Nikunja, Dhaka-1229</b><td>
                                                                    <td><b>Thanks for choosing little Feat as a business partner.</b></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php endif; ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
    var amount =<?= $allinfo->total_amount; ?>;
    window.onload = function() {
        convertNumberToWords(amount);
    };
    function convertNumberToWords(amount) {
        var words = new Array();
        words[0] = '';
        words[1] = 'One';
        words[2] = 'Two';
        words[3] = 'Three';
        words[4] = 'Four';
        words[5] = 'Five';
        words[6] = 'Six';
        words[7] = 'Seven';
        words[8] = 'Eight';
        words[9] = 'Nine';
        words[10] = 'Ten';
        words[11] = 'Eleven';
        words[12] = 'Twelve';
        words[13] = 'Thirteen';
        words[14] = 'Fourteen';
        words[15] = 'Fifteen';
        words[16] = 'Sixteen';
        words[17] = 'Seventeen';
        words[18] = 'Eighteen';
        words[19] = 'Nineteen';
        words[20] = 'Twenty';
        words[30] = 'Thirty';
        words[40] = 'Forty';
        words[50] = 'Fifty';
        words[60] = 'Sixty';
        words[70] = 'Seventy';
        words[80] = 'Eighty';
        words[90] = 'Ninety';
        amount = amount.toString();
        var atemp = amount.split(".");
        var number = atemp[0].split(",").join("");
        var n_length = number.length;
        var words_string = "";
        if (n_length <= 9) {
            var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
            var received_n_array = new Array();
            for (var i = 0; i < n_length; i++) {
                received_n_array[i] = number.substr(i, 1);
            }
            for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                n_array[i] = received_n_array[j];
            }
            for (var i = 0, j = 1; i < 9; i++, j++) {
                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    if (n_array[i] == 1) {
                        n_array[j] = 10 + parseInt(n_array[j]);
                        n_array[i] = 0;
                    }
                }
            }
            value = "";
            for (var i = 0; i < 9; i++) {
                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    value = n_array[i] * 10;
                } else {
                    value = n_array[i];
                }
                if (value != 0) {
                    words_string += words[value] + " ";
                }
                if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Crores ";
                }
                if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Lakhs ";
                }
                if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Thousand ";
                }
                if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                    words_string += "Hundred and ";
                } else if (i == 6 && value != 0) {
                    words_string += "Hundred ";
                }
            }
            words_string = words_string.split("  ").join(" ");
        }
        $("#inword").text(words_string);
    }

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>


