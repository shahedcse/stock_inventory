<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Product Purchase List
                                <a href="<?= base_url('Inventory/add_purchase'); ?>">
                                    <button type="button" class=" btn bg-cyan pull-right">Add Purchase</button>
                                </a>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Purchase date</th>
                                            <th>Invoice Number</th>
                                            <th>Vendor</th>
                                            <th>Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Purchase date</th>
                                            <th>Invoice Number</th>
                                            <th>Vendor</th>
                                            <th>Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($purchase_list as $value): ?>
                                            <tr>
                                                <td><?= date("F j, Y", strtotime($value->purchase_date)); ?></td>
                                                <td><?= $value->purchaseInvoiceNo; ?></td>
                                                <td><?= $value->vendorId; ?></td>
                                                <td><?= $value->amount; ?></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            ACTION <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?= base_url('Inventory/edit_purchase?id=' . $value->purchaseMasterId); ?>"style="color:green;" class=" waves-effect waves-block">View Details</a></li>
                                                            <li><a href="javascript:void(0);" style="color:red;" class=" waves-effect waves-block">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
