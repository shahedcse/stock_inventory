<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if ($this->session->userdata('notadd')): ?>
                        <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?=
                            $this->session->userdata('notadd');
                            $this->session->unset_userdata('notadd');
                            ?>
                        </div>
                    <?php elseif ($this->session->userdata('add')): ?>
                        <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?=
                            $this->session->userdata('add');
                            $this->session->unset_userdata('add');
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                Ledger Balance Result For.
                            </h2>
                            <div class="row clearfix">
                                <form action="<?= base_url('Accounts/ledgerbalance'); ?>" method="POST">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="demo-radio-button">
                                                <input name="ledger_type" onclick=" return makechange()" type="radio" value="1" id="radio_2"<?php if ($type == 1): ?> checked=""<?php endif; ?>>
                                                <label for="radio_2">Dealer Point</label>
                                                <input name="ledger_type" onclick=" return makechange()" type="radio" value="2" id="radio_1" <?php if ($type == 2): ?> checked=""<?php endif; ?>  >
                                                <label for="radio_1">Customer Point</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xs-3" id="dealer"  style="display:none;">
                                        <div class="form-group">
                                            <select class="form-control show-tick" id="dealerid" name="dealerid">
                                                <option value="">-Select Dealer Point -</option>
                                                <?php
                                                foreach ($dealer_list AS $value):
                                                    if ($dealer == $value->id):
                                                        ?>
                                                        <option value="<?= $value->id; ?>"selected=""><?= $value->full_name; ?> </option>
                                                    <?php else: ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->full_name; ?> </option>

                                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3"  id="customer" style="display:none;">
                                        <div class="form-group">
                                            <select class="form-control show-tick " data-live-search="true" id="customerid" name="customerid">
                                                <option value="">-- Select Customer Point --</option>

                                                <?php
                                                foreach ($customerall AS $value):
                                                    if ($customer == $value->id):
                                                        ?>
                                                        <option value="<?= $value->id; ?>"selected=""><?= $value->name; ?> </option>
                                                    <?php else: ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->name; ?> </option>
                                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <button type="submit" onclick=" return checkinput()" class="btn btn-primary m-t-15 waves-effect">Show Result</button>
                                    </div>

                                    <div class="col-xs-3">
                                        <button type="button" onclick="balance_adjust();" class="btn btn-default m-t-15 waves-effect">Balance Adjust ৳</button>
                                    </div>

                                </form>

                            </div>
                            <?php if (!empty($dealerdata) || !empty($customerdata)): ?>
                                <input type="button" class="btn btn-success pull-right" onclick="printDiv('printledger')" value="print Ledger" />
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-1"id="printledger">
                                    <div class="card">
                                        <div class="header ">
                                            <img height="45px;" width="110px;" src="<?= base_url('assets/images/company.jpg'); ?>">

                                            <center>
                                                <?php if ($type == 1): ?>
                                                    <p style="font-size: 12px;">
                                                        Comapany: Kids Diapers.<br>
                                                        Ledger Name:<?= $dealerinfo->full_name; ?><br> 
                                                        Phone:<?= $dealerinfo->phone; ?><br>
                                                        Address:<?= $dealerinfo->present_address; ?><br>
                                                        Created Date:<?= date('Y-m-d'); ?>
                                                    </p>
                                                <?php elseif ($type == 2): ?>
                                                    <p>
                                                        Comapany: Kids Diapers.<br>
                                                        Ledger Name:<?= $customerinfo->name; ?><br> 
                                                        Phone:<?= $customerinfo->phone; ?><br>
                                                        Address:<?= $customerinfo->address; ?><br>
                                                        Created Date:<?= date('Y-m-d'); ?>
                                                    </p>
                                                <?php endif; ?>

                                            </center>
                                        </div>
                                        <div class="body table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">Date</th>
                                                        <th style="text-align: center">Invoice No.</th>
                                                        <th style="background-color:  #f06c7e ;color:#fff;text-align: center">Credit</th>
                                                        <th style="background-color:  #70cd77 ;color:#fff;text-align: center">Debit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($dealerdata)): ?>
                                                        <?php foreach ($dealerdata AS $value): ?>
                                                            <tr>
                                                                <th style="text-align: center" scope="row"><?= $value->date; ?></th>
                                                                <th style="text-align: center" scope="row"><?= $value->voucherNumber; ?></th>
                                                                <td style="background-color:  #f06c7e ;color:#fff;text-align: center"><?= $value->credit; ?></td>
                                                                <td style="background-color:  #70cd77 ;color:#fff;text-align: center"><?= $value->debit; ?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <?php foreach ($customerdata AS $value): ?>
                                                            <tr>
                                                                <th scope="row"><?= $value->date; ?></th>
                                                                <th style="text-align: center" scope="row"><?= $value->voucherNumber; ?></th>
                                                                <td style="background-color:  #f06c7e ;color:#fff;text-align: center"><?= $value->credit; ?></td>
                                                                <td style="background-color:  #70cd77 ;color:#fff;text-align: center"><?= $value->debit; ?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>

                                            <ul class="list-group pull-right">
                                                <li class="list-group-item">
                                                    <table style=" text-align:right; width:100%; font-size: 10px; font-weight: bold;margin-bottom: 5px;">
                                                        <tbody>
                                                            <?php if (!empty($dealerdata)): ?>
                                                                <tr>
                                                                    <td>Total Credit: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotald->cr; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Debit: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotald->dr; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Current Due Balance: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotald->cr - $datatotald->dr ?></td>
                                                                </tr>
                                                            <?php else: ?>
                                                                <tr>
                                                                    <td>Total Credit: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotalc->cr; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Debit: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotalc->dr; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Current Due Balance: </td>
                                                                    <td id="invoiceamount1" style="text-align:right"><?= $datatotalc->cr - $datatotalc->dr ?></td>
                                                                </tr>
                                                            <?php endif; ?>

                                                        </tbody>
                                                    </table>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<div class="modal fade" id="adjustcustomer" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Balance Adjust Info For Customer. :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Accounts/balance_adjust_customer'); ?>" method="POST">
                <div class="modal-body " >
                    <input type="hidden" id="customeridinput" name="customeridinput" class="form-control">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Adjust Date:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="date" id="adjustdate" name="adjustdate" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Adjust For :</label>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick " id="adjustfor" name="adjustfor" required="">
                                        <option value="">-- Please select --</option>
                                        <option value="1">Dr.</option>
                                        <option value="2">Cr</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Adjust  Amount:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required="" onkeypress="isNumberKey(event)"id="amount" name="amount" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Payment Type:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick " id="payment_type" name="payment_type" >
                                        <option value="">-- Please select --</option>
                                        <option value="1">Cash Payment</option>
                                        <option value="2">Bank Payment</option>
                                        <option value="3">Mobile Banking Payment</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="adjustdealer" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Balance Adjust Info For Dealer :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Accounts/balance_adjust_dealer'); ?>" method="POST">
                <div class="modal-body " >
                    <input type="hidden" id="dealeridinput" name="dealeridinput" class="form-control">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Adjust Date:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="date" id="adjustdate" name="adjustdate" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Adjust For :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick " id="adjustfor" name="adjustfor" required="">
                                        <option value="">-- Please select --</option>
                                        <option value="1">Dr.</option>
                                        <option value="2">Cr</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Adjust  Amount:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required="" onkeypress="isNumberKey(event)"id="amount" name="amount" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Payment Type:</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick " id="payment_type" name="payment_type">
                                        <option value="">-- Please select --</option>
                                        <option value="1">Cash Payment</option>
                                        <option value="2">Bank Payment</option>
                                        <option value="3">Mobile Banking Payment</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
<?php if ($type == 1): ?>
        $("#dealer").show();
<?php endif; ?>
<?php if ($type == 2): ?>
        $("#customer").show();
<?php endif; ?>


    function checkinput() {
        var type = $('input[name=ledger_type]:checked').val();
        var dealer = $('#dealerid').val();
        var customer = $('#customerid').val();
        if (type == "") {
            alert('Please Select leadger Type ');
            return false;
        }
        if (dealer == "" && customer == "") {
            alert('Please Select Customer Or Dealer Name Properly');
            return false;
        }
    }


    function balance_adjust() {
        var type = $('input[name=ledger_type]:checked').val();
        var dealer = $('#dealerid').val();
        var customer = $('#customerid').val();
        if (type == "") {
            alert('Please Select leadger Type ');
            return false;
        }
        if (dealer == "" && customer == "") {
            alert('Please Select Customer Or Dealer Name Properly');
            return false;
        }
        if (type == 1) {
            $('#dealeridinput').val(dealer);
            $('#adjustdealer').modal("show");
        }
        if (type == 2) {
            $('#customeridinput').val(customer);
            $('#adjustcustomer').modal("show");
        }
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    function makechange() {
        var type = $('input[name=ledger_type]:checked').val();
        if (type == 1) {
            $("#dealer").show();
            $("#customer").hide();
        }
        else {
            $("#dealer").hide();
            $("#customer").show();
        }


    }
</script>

