<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?= base_url('assets/images/uploads/' . $this->session->userdata('image_path')); ?>" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hello !!   <?= $this->session->userdata('user_name'); ?></div>
                <div class="email"><?= $this->session->userdata('email'); ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?= base_url('Profile'); ?>"><i class="material-icons">person</i>My Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('Profile'); ?>"><i class="material-icons">group</i>Change Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('Auth/logout'); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <?php if (in_array($this->session->userdata('user_role'), [1, 2])): ?>
                    <li class="<?= $active_menu == 'dashboard' ? 'active open' : ''; ?>">
                        <a href="<?= base_url('Dashboard'); ?>">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="<?= $active_menu == 'admin' ? 'active open' : ''; ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Admin</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $sub_menu == 'Userinfo' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Userinfo'); ?>">User List</a>
                            </li>
                            <li class="<?= $sub_menu == 'dealer' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Profile/dealer_info'); ?>">Dealer Info</a>
                            </li>
                            <li class="<?= $sub_menu == 'product_list' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Product'); ?>">Product Group</a>
                            </li>
                            <li class="<?= $sub_menu == 'product_sub_list' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Product/product_sublist'); ?>">Product List</a>
                            </li>
                            <li class="<?= $sub_menu == 'product_unit' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Product/product_unit'); ?>">Product Unit</a>
                            </li>
                            <li class="<?= $sub_menu == 'vendor' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Userinfo/vendor_list'); ?>">Vendor List</a>
                            </li>
                            <li class="<?= $sub_menu == 'customer' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Userinfo/customer_list'); ?>">Customer List</a>
                            </li>
                            <li class="<?= $sub_menu == 'costcat' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Accounts/cost_category'); ?>">Cost Category List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?= $active_menu == 'inventory' ? 'active open' : ''; ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-light-green">widgets</i>
                            <span>Inventory</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $sub_menu == 'purchase' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Inventory/purchase_list'); ?>">Product Purchase </a>
                            </li>
                            <li class="<?= $sub_menu == 'disburse' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Inventory/disburse_list'); ?>">Product Disburse</a>
                            </li>
                            <li class="<?= $sub_menu == 'challan' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Inventory/challan_list'); ?>">Product Challan</a>
                            </li>
                            
                              <li class="<?= $sub_menu == 'homedelivery' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Inventory/home_delivery'); ?>">Home Delivery</a>
                            </li>
                            <li class="<?= $sub_menu == 'mainstock' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Stock'); ?>">Main Stock</a>
                            </li>

                        </ul>
                    </li>
                <?php endif; ?>
                <?php if (in_array($this->session->userdata('user_role'), [1, 2,3,4])): ?>
                    <li class="<?= $active_menu == 'SR' ? 'active open' : ''; ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">account_box</i>
                            <span>SR</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $sub_menu == 'neworder' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Order/neworder'); ?>">New Order</a>
                            </li>
                            <li class="<?= $sub_menu == 'orderlist' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Order/order_list'); ?>">Order List</a>
                            </li>
                            <li class="<?= $sub_menu == 'cancelorder' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Order/order_listcancel'); ?>">Cancel Order </a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if (in_array($this->session->userdata('user_role'), [1, 2, 5])): ?>
                    <li class="<?= $active_menu == 'report' ? 'active open' : ''; ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span>Report</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $sub_menu == 'deliveryreport' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Report/delivery_report'); ?>">Delivery Report</a>
                            </li>
                            <li class="<?= $sub_menu == 'purchasereport' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Report/purchase_report'); ?>">Purchase Report</a>
                            </li>
                            <li class="<?= $sub_menu == 'productstock' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Report/stock_report'); ?>">Product Stock Report</a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= $active_menu == 'accounts' ? 'active open' : ''; ?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-light-blue">trending_down</i>
                            <span>Accounting</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="<?= $sub_menu == 'ad_cost' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Accounts/additionalCost'); ?>">Additional Cost</a>
                            </li>
                            <li class="<?= $sub_menu == 'ledger' ? 'active open' : ''; ?>">
                                <a href="<?= base_url('Accounts/ledgerbalance'); ?>">Ledger Balance</a>
                            </li>
                            <li>
                                <a href="#">Profit & Loss Analysis </a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= $active_menu == 'system' ? 'active open' : ''; ?>">
                        <a href="<?= base_url('Auth/about'); ?>">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>System Information</span>
                        </a>
                    </li>

                    <li class="<?= $active_menu == 'backup' ? 'active open' : ''; ?>">
                        <a href="<?= base_url('Backup'); ?>">
                            <i class="material-icons col-light-green">donut_large</i>
                            <span>DB Backup</span>
                        </a>
                    </li>
                <?php endif; ?>
                <li class="<?= $active_menu == 'logout' ? 'active open' : ''; ?>">
                    <a href="<?= base_url('Auth/logout'); ?>">
                        <i class="material-icons">input</i>
                        <span>Sign Out</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; <?= date("Y") ?> <a href="javascript:void(0);">Developed By</a>.
            </div>
            <div class="version">
                <b>Logic & Thoughts </b> 
            </div>
        </div>
        <!-- #Footer -->
    </aside>
</section>