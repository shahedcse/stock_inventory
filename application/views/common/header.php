<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?= $title; ?></title>
        <!-- Favicon-->
        <link rel="icon" href="<?= base_url(); ?>assets/inventory-icon-10.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core Css -->
        <link href="<?= base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?= base_url(); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?= base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="<?= base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

        <!-- Bootstrap DatePicker Css -->
        <link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
        <!-- JQuery DataTable Css -->
        <link href="<?= base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
        <!-- Morris Chart Css-->
        <link href="<?= base_url(); ?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="<?= base_url(); ?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?= base_url(); ?>assets/css/themes/all-themes.css" rel="stylesheet" />

    </head>
</html>

<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <?php
            $logopath = $this->db->query("SELECT  logo_path FROM system where id='1'")->row()->logo_path;
            ?>
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url('assets/images/' . $logopath); ?>" style="padding: 2px;" width="98" height="38" alt="Logo" />
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons"  style="color:black;">keyboard_arrow_down</i>

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('Profile'); ?>"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('Profile'); ?>"><i class="material-icons">group</i>Change Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('Auth/logout'); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </li>
        </div>

    </div>
</div>
</nav>
<!-- #Top Bar -->