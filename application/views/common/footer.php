<!-- Jquery Core Js -->

<script src="<?= base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<!-- Bootstrap Core Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>
<!-- Autosize Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/autosize/autosize.js"></script>
<!-- Select Plugin Js -->

<script src="<?= base_url(); ?>assets/plugins/momentjs/moment.js"></script>
<script src="<?= base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/node-waves/waves.js"></script>
<!-- Sparkline Chart Plugin Js -->
<script src="<?= base_url(); ?>assets/js/pages/tables/jquery-datatable.js"></script>
<!-- Jquery DataTable Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<!-- Jquery CountTo Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="<?= base_url(); ?>assets/plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?= base_url(); ?>assets/plugins/flot-charts/jquery.flot.js"></script>
<script src="<?= base_url(); ?>assets/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="<?= base_url(); ?>assets/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="<?= base_url(); ?>assets/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="<?= base_url(); ?>assets/plugins/flot-charts/jquery.flot.time.js"></script>
<script src="<?= base_url(); ?>assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
<!-- Custom Js -->
<script src="<?= base_url(); ?>assets/js/admin.js"></script>
<script src="<?= base_url(); ?>assets/js/pages/index.js"></script>






<!-- Demo Js -->
<script src="<?= base_url(); ?>assets/js/demo.js"></script>