<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Delivery(Stock Out) Report.
                            </h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                <form action="<?= base_url('Report/delivery_report'); ?>" method="POST">
                                    <div class="col-xs-2">
                                        <h2 class="card-inside-title">Delivery To:</h2>
                                        <div class="form-group">
                                            <select class="form-control show-tick" id="disburse_type" name="disburse_type">
                                                <option value="">-select -</option>
                                                <?php
                                                foreach ($disburse_list AS $value):
                                                    if ($type == $value->id):
                                                        ?>
                                                        <option value="<?= $value->id; ?>"selected=""><?= $value->type_name; ?> </option>
                                                    <?php else: ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->type_name; ?> </option>
                                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <h2 class="card-inside-title">Select Product :</h2>
                                        <div class="form-group">
                                            <select class="form-control show-tick selectpicker" data-live-search="true" id="product" name="product">
                                                <option value="">-- Please select --</option>
                                                <?php
                                                foreach ($subproductlist AS $value):
                                                    if ($product == $value->id):
                                                        ?>
                                                        <option value="<?= $value->id ?>"selected=""><?= $value->sublist_name ?></option>
                                                    <?php else: ?>
                                                        <option value="<?= $value->id ?>"><?= $value->sublist_name ?>-<?= $value->pack_size ?></option>
                                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-5">
                                        <h2 class="card-inside-title">Date Range :</h2>
                                        <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                            <div class="form-line">
                                                <input type="date" name="datefrom" value="<?= $datestart ?>" class="form-control" required="">
                                            </div>
                                            <span class="input-group-addon">to</span>
                                            <div class="form-line">
                                                <input type="date" name="dateto" value="<?= $dateend ?>" class="form-control" required="">
                                            </div>
                                        </div><br>
                                        <span id="price" style="font-size: 20px;color:green;"></span>
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="submit" class="btn btn-success m-t-15 waves-effect">Search</button>
                                        <a href="<?= base_url('Report/delivery_report'); ?>"class="btn btn-primary m-t-15 waves-effect">Reset</a>
                                    </div>

                                </form>
                            </div>
                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover js-exportable " id="res">
                                    <thead>
                                        <tr>
                                            <th colspan="5" style="text-align: center;color:green"> Date From: <b><?= $datestart ?></b> Date TO : <b><?= $dateend ?></b> </th>
                                        </tr>
                                        <tr style="background-color:#54f0eb; ">
                                            <th>Date</th>
                                            <th>Disburse Cat.</th>
                                            <th>Disburse To</th>
                                            <th>Product </th>
                                            <th>Quantity</th>
                                            <th>Amount</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($stockdata AS $value):
                                            if ($value->disburse_type == '5'):
                                                $dealer = $this->db->query("SELECT dealer_point_name FROM users where id='$value->disburse_to' ")->row()->dealer_point_name;
                                            else:
                                                $customer = $this->db->query("SELECT name FROM customer where id='$value->customer' ")->row()->name;
                                            endif;
                                            ?>
                                            <tr>
                                                <td><?= date("Y-m-d", strtotime($value->date)); ?></td>
                                                <td><?= $value->type_name; ?></td>
                                                <td>
                                                    <?php
                                                    if ($value->disburse_type == '5'):
                                                        echo $dealer;
                                                    else:
                                                        echo $customer;
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><?= $value->sublist_name; ?>-<?= $value->pack_size; ?></td>
                                                <td> <?= $value->outwardQuantity; ?></td>
                                                <td class="countables">
                                                    <?php if (empty($value->discountrate)): ?>
                                                        <?= $value->rate * $value->outwardQuantity ?>
                                                    <?php else: ?>
                                                        <?= $value->outwardQuantity * $value->discountrate ?>
                                                    <?php endif; ?>

                                                </td>

                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
    $(document).ready(function() {
        var sum = 0;
        var table = document.getElementById("res");
        var ths = table.getElementsByTagName('th');
        var tds = table.getElementsByClassName('countables');
        for (var i = 0; i < tds.length; i++) {
            sum += isNaN(tds[i].innerText) ? 0 : parseInt(tds[i].innerText);
        }
        $("#price").html('Total Delivery Amount-> ' + sum);

    });
</script>

