<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Product Purchase(Stock In) Report.
                            </h2>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                <form action="<?= base_url('Report/purchase_report'); ?>" method="POST">
                                    <div class="col-xs-2">
                                        <h2 class="card-inside-title">Select Product :</h2>
                                        <div class="form-group">
                                            <select class="form-control show-tick selectpicker" data-live-search="true" id="product" name="product">
                                                <option value="">-- Please select --</option>
                                                <?php foreach ($subproductlist AS $value): ?>
                                                    <option value="<?= $value->id ?>"><?= $value->sublist_name ?>-<?= $value->pack_size ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-5">
                                        <h2 class="card-inside-title">Date Range :</h2>
                                        <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                            <div class="form-line">
                                                <input type="date" name="datefrom" class="form-control" required="">
                                            </div>
                                            <span class="input-group-addon">to</span>
                                            <div class="form-line">
                                                <input type="date" name="dateto" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="submit" class="btn btn-success m-t-15 waves-effect">Search</button>
                                    </div>

                                </form>
                            </div>
                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover js-exportable ">
                                    <thead>
                                        <tr>
                                            <th colspan="5" style="text-align: center;color:green"> Date From: <b><?= $datestart ?></b> Date TO : <b><?= $dateend ?></b> </th>
                                        </tr>
                                        <tr style="background-color:#54f0eb; ">
                                            <th>Purchase Date</th>
                                            <th>Vendor.</th>
                                            <th>Product Group</th>
                                            <th>Product Name </th>
                                            <th>Quantity</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($purchasedata AS $value):
                                            ?>
                                            <tr>
                                                <td><?= $value->purchase_date; ?></td>
                                                <td><?= $value->vendorId; ?></td>
                                                <td><?= $value->product_name; ?></td>
                                                <td><?= $value->sublist_name; ?> -<?= $value->pack_size; ?></td>
                                                <td> <?= $value->inwardQuantity; ?></td>

                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

