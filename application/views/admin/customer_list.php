<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Registered Customer List
                                <button type="button" class=" btn bg-cyan pull-right"data-target="#customeradd_modal" data-toggle="modal">Add Customer</button>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Customer Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Customer Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Option</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($customerdata AS $value): ?>
                                            <tr>
                                                <td><?= $value->id; ?></td>
                                                <td><?= $value->name; ?></td>
                                                <td><?= $value->phone; ?></td>
                                                <td><?= $value->address; ?></td>
                                                <td>
                                                    <button onclick="edit_customer('<?= $value->id; ?>');" type="button" class=" btn bg-green">Edit</button>
                                                    <button onclick="delete_user2s('<?= $value->id; ?>');" type="button" class=" btn bg-red">Delete</button>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<div class="modal fade " id="deletemodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('Userinfo/delete_vendor'); ?>" method="post">
            <div class="modal-content modal-col-red">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="vendor_id"  name="vendor_id" class="form-control">
                    <p style="font-size: 20px;">  Are you sure , you want to delete this Customer ? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="customeradd_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Add New Customer Info. :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/addcustomer3'); ?>" method="POST">
                <div class="modal-body " >
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="name" required="" name="name" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required=""  id="phone" name="phone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="address" id="address" cols="20" rows="1" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Edit Customer Info. :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/updatecustomer'); ?>" method="POST">
                <div class="modal-body " >
                    <input type="hidden" id="customerid"  name="customerid" class="form-control">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="editname" required="" name="editname" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required="" onkeypress="allowNumbersOnly(event)" maxlength="11" id="editphone" name="editphone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="editaddress" id="editaddress" cols="20" rows="1" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function edit_customer(customer) {
        $("#customerid").val(customer);

        $.ajax({
            type: "POST",
            url: "<?= base_url('Userinfo/getcustomerData'); ?>",
            data: 'id=' + customer,
            success: function(data) {
                var outputData = JSON.parse(data);
                var response = outputData.userdata;
                $("#editname").val(response.name);
                $("#editphone").val(response.phone);
                $("#editaddress").val(response.address);

                $('#editmodal').modal("show");
            }
        });

    }



    function delete_user2(user_id) {
        $('#vendor_id').val(user_id);
        $('#deletemodal').modal("show");
    }
</script>

