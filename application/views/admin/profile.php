<html>
    <body class="theme-red">
        <section class="content">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-3">
                        <div class="card profile-card">
                            <div class="profile-header">&nbsp;</div>
                            <div class="profile-body">
                                <div class="image-area">
                                    <img width="128" height="128" src="<?= base_url('assets/images/uploads/') . $user_data->image_path; ?>" alt="Name" />
                                </div>
                                <div class="content-area">
                                    <h3><?= $user_data->full_name; ?></h3>
                                    <p><?= $user_data->email; ?></p>
                                    <p><?= $user_data->role_name; ?></p>
                                </div>
                            </div>
                            <div class="profile-footer">
                                <a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab">
                                    <button class="btn btn-primary btn-lg waves-effect btn-block">Update My Info</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9" >
                        <div class="card" style="border-radius: 30px;">
                            <?php if ($this->session->userdata('notadd')): ?>
                                <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <?=
                                    $this->session->userdata('notadd');
                                    $this->session->unset_userdata('notadd');
                                    ?>
                                </div>
                            <?php elseif ($this->session->userdata('add')): ?>
                                <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <?=
                                    $this->session->userdata('add');
                                    $this->session->unset_userdata('add');
                                    ?>
                                </div>
                            <?php endif; ?>
                            <div class="body">
                                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">My Info</a></li>
                                        <li role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab">Update Info</a></li>
                                        <li role="presentation"><a href="#profile_image" aria-controls="settings" role="tab" data-toggle="tab">Update Image</a></li>
                                        <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab">Change Password</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="OldPassword" class="col-sm-3 control-label">Full Name:</label>
                                                    <div class="col-sm-9">

                                                        <?= $user_data->full_name; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPassword" class="col-sm-3 control-label">Email:</label>
                                                    <div class="col-sm-9">

                                                        <?= $user_data->email; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPasswordConfirm" class="col-sm-3 control-label">Phone:</label>
                                                    <div class="col-sm-9">

                                                        <?= $user_data->phone; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPasswordConfirm" class="col-sm-3 control-label">Address:</label>
                                                    <div class="col-sm-9">

                                                        <?= $user_data->present_address; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPasswordConfirm" class="col-sm-3 control-label">User Name:</label>
                                                    <div class="col-sm-9">

                                                        <?= $user_data->name; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPasswordConfirm" class="col-sm-3 control-label">Status:</label>
                                                    <div class="col-sm-9">

                                                        Active

                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="profile_settings">
                                            <form class="form-horizontal" action="<?= base_url('profile/edit'); ?>" method="POST">
                                                <div class="form-group">
                                                    <label for="NameSurname" class="col-sm-2 control-label">Full Name</label>
                                                    <input type="hidden" class="form-control" value="<?= $user_data->id; ?>" id="user_id" name="user_id">
                                                    <div class="col-sm-10">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" value="<?= $user_data->full_name; ?>" id="full_name" name="full_name"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Email" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" value="<?= $user_data->email; ?>" id="email" name="email"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Email" class="col-sm-2 control-label">Phone</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" value="<?= $user_data->phone; ?>" id="phone" name="phone"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Email" class="col-sm-2 control-label">User Name</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" value="<?= $user_data->name; ?>" id="name" name="name"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="InputExperience" class="col-sm-2 control-label">Address</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" value="<?= $user_data->present_address; ?>" id="present_address" name="present_address">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-success">UPDATE</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="profile_image">
                                            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Profile/updateImage'); ?>" method="post">
                                                <div class="form-group">
                                                    <label for="OldPassword" class="col-sm-3 control-label">Select Image</label>
                                                    <div class="col-sm-9">
                                                        <div class="form-line">
                                                            <input type="file" class="form-control" id="fileToUpload" name="fileToUpload">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="submit" class="btn btn-success">UPDATE</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                                            <form class="form-horizontal" action="<?= base_url('Profile/updatePassword'); ?>" method="POST">
                                                <div class="form-group">
                                                    <label for="OldPassword" class="col-sm-3 control-label">Old Password</label>
                                                    <div class="col-sm-9">
                                                        <div class="form-line">
                                                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Old Password" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPassword" class="col-sm-3 control-label">New Password</label>
                                                    <div class="col-sm-9">
                                                        <div class="form-line">
                                                            <input type="password" class="form-control" id="npassword" name="npassword" placeholder="New Password" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="NewPasswordConfirm" class="col-sm-3 control-label">New Password (Confirm)</label>
                                                    <div class="col-sm-9">
                                                        <div class="form-line">
                                                            <input type="password" class="form-control" id="NewPasswordConfirm" name="NewPasswordConfirm" placeholder="New Password (Confirm)" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="submit" class="btn btn-success">UPDATE</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>