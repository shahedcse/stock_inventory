<style>
    .profile-card .profile-header {
        background-color: #00B2C9;
        padding: 42px 0;
    }
</style>

<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Dealer Info:
                            </h2>
                        </div>

                        <div class="row clearfix">
                            <?php foreach ($all_dealer AS $value): ?>
                                <div class="col-xs-12 col-sm-4" style="">
                                    <div class="card profile-card">
                                        <div class="profile-header">&nbsp;</div>
                                        <div class="profile-body">
                                            <div class="image-area">
                                                <img src="<?= base_url('assets/images/uploads/' . $value->image_path); ?>" alt="img" width="128" height="128">
                                            </div>
                                            <div class="content-area">
                                                <h3><?= $value->dealer_point_name; ?></h3>
                                                <p>Owner: <?= $value->full_name; ?></p>
                                                <p>Contact: <?= $value->phone; ?></p>
                                                <p>Email: <?= $value->email; ?></p>
                                                <p>Address: <?= $value->present_address; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
