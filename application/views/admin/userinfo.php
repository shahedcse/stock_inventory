<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                System User List
                                <button type="button" class=" btn bg-cyan pull-right"data-target="#defaultModal" data-toggle="modal">Add User</button>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Role Name</th>
                                            <th>Address</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Role Name</th>
                                            <th>Address</th>
                                            <th>Option</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($users_list as $value): ?>
                                            <tr>
                                                <td><?= $value->id ?></td>
                                                <td><?= $value->full_name ?></td>
                                                <td><?= $value->phone ?></td>
                                                <td><?= $value->role_name ?></td>
                                                <td><?= $value->present_address ?></td>
                                                <td> 
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            ACTION <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="edit_user('<?= $value->id; ?>');"style="color:green;" class=" waves-effect waves-block">Edit</a></li>
                                                            <li><a href="#" onclick="passresetmodal('<?= $value->id; ?>');"style="color:green;" class=" waves-effect waves-block">Change password</a></li>
                                                            <li><a href="#" onclick="delete_user2('<?= $value->id; ?>');" style="color:red;" class=" waves-effect waves-block">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Add User Information:</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/addUser'); ?>" method="POST">
                <div class="modal-body " >
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Full Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="full_name" required="" name="full_name" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Email :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="email"  name="email" class="form-control" placeholder="Enter Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required="" onkeypress="allowNumbersOnly(event)" maxlength="11" id="phone" name="phone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">User Role :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" required="" id="role" name="role">
                                        <option value="">-- Please select --</option>
                                        <?php foreach ($role AS $value): ?>
                                            <option value="<?= $value->id; ?>"><?= $value->role_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix" id="delaerdiv" style="display: none;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Under Dealer :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" id="dealerid" name="dealerid">
                                        <option value="">--Select Dealer--</option>
                                        <?php foreach ($dealer_list AS $value): ?>
                                            <option value="<?= $value->id; ?>"><?= $value->dealer_point_name; ?> - <?= $value->full_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="delaer_det" style="display: none;">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                <label for="password_2">Dealer Point Name :</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="dealer_point_name" name="dealer_point_name" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                <label for="password_2">Dealer Zone :</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="zonename" name="zonename">
                                            <option value="">-- Please select --</option>
                                            <?php foreach ($zone AS $value): ?>
                                                <option value="<?= $value->id; ?>"><?= $value->divisionname; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">User Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="name" required="" name="name" class="form-control" placeholder="Enter your User Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Password :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" required="" id="password" name="password" class="form-control" placeholder="Enter your password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="present_address" id="present_address" cols="30" rows="1" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Profile Image :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="file" id="fileToUpload" name="fileToUpload" class="form-control">
                                    <div id="preview" ></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Edit User Information:</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/updateUser'); ?>" method="POST">
                <input type="hidden" id="edit_userid"  name="edit_userid" class="form-control">
                <div class="modal-body " >
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Full Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="editfull_name" required="" name="editfull_name" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Email :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="editemail"  name="editemail" class="form-control" placeholder="Enter Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required="" onkeypress="allowNumbersOnly(event)" maxlength="11" id="editphone" name="editphone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">User Role :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" required="" id="editrole" name="editrole">
                                        <option value="">-- Please select --</option>
                                        <?php foreach ($role AS $value): ?>
                                            <option value="<?= $value->id; ?>"><?= $value->role_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="editdelaer_det" style="display: none;">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                <label for="password_2">Dealer Point Name :</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="editdealer_point_name" name="editdealer_point_name" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                                <label for="password_2">Dealer Zone :</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" id="editzonename" name="editzonename">
                                            <option value="">-- Please select --</option>
                                            <?php foreach ($zone AS $value): ?>
                                                <option value="<?= $value->id; ?>"><?= $value->divisionname; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">User Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="editname" required="" name="editname" class="form-control" placeholder="Enter your User Name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="editpresent_address" id="editpresent_address" cols="30" rows="3" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Profile Image :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="file" id="editfileToUpload" name="editfileToUpload" class="form-control">
                                    <div id="previewto" ></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade " id="deletemodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('Userinfo/delete_user'); ?>" method="post">
            <div class="modal-content modal-col-red">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id"  name="user_id" class="form-control">
                    <p style="font-size: 20px;">  Are you sure , you want to delete this user ? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>
<form class="form-horizontal form-group"
      action="<?= base_url('Userinfo/updatepass'); ?>" method="post">
    <div id="myModal" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header table-background">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true"></button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="row clearfix">
                                <div class="row clearfix">
                                    <div class="col-lg-6">
                                        <input class=" form-control" id="userid"
                                               name="id" value=""
                                               type="hidden"/>
                                        <span id="update_cpassword"></span>
                                    </div>
                                </div>
                                <div class="form-group  row clearfix">
                                    <label for="newpassword"
                                           class="control-label col-lg-4">New
                                        Password :
                                    </label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" style="border: 1px solid gray;"
                                               id="npassword" name="npassword"
                                               type="password"/>
                                        <span id="update_npassword"></span>
                                    </div>
                                </div><br>
                                <div class="form-group row clearfix">
                                    <label for="verifynewpassword"
                                           class="control-label col-lg-4">VerifyNew
                                        Password :</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" style="border: 1px solid gray;"
                                               id="rpassword" name="rpassword"
                                               type="password"/>
                                        <span id="update_rpassword"></span>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button type="submit" id="submit"
                                                class="btn btn-success"
                                                onclick="return updatePassword()">
                                            Update
                                        </button>
                                        <button type="reset"
                                                class="btn btn-danger">Clear
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal"
                            class="btn dark btn-outline">Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function delete_user2(user_id) {
        $('#user_id').val(user_id);
        $('#deletemodal').modal("show");
    }


    function passresetmodal(userid) {
        $('#userid').val(userid);
        $('#myModal').modal('show');
    }

    function updatePassword() {

        var npassword = $("#npassword").val();
        var rpassword = $("#rpassword").val();

        if (npassword == '') {
            $("#update_cpassword").text("");
            $("#update_npassword").css('color', 'red');
            $("#update_npassword").text("New Password required !!!");
            return false;
        }
        else if (rpassword == '') {
            $("#update_npassword").text("");
            $("#update_rpassword").css('color', 'red');
            $("#update_rpassword").text("Repeat Password required !!!");
            return false;
        }
        else if (npassword != rpassword) {
            $("#update_rpassword").text("");
            $("#update_rpassword").css('color', 'red');
            $("#update_rpassword").text("Password not matched !!!");
            return false;
        }
        else {
            return true;
        }
    }
    function edit_user(userId) {
        $("#edit_userid").val(userId);
//editdealer_point_name
        $.ajax({
            type: "POST",
            url: "<?= base_url('Userinfo/getDetailsData'); ?>",
            data: 'id=' + userId,
            success: function(data) {
                var outputData = JSON.parse(data);
                var response = outputData.userdata;
                $("#editfull_name").val(response.full_name);
                $("#editemail").val(response.email);
                $("#editphone").val(response.phone);
                $('#editrole').val(response.role).change();
                $("#editname").val(response.name);
                $("#editpresent_address").val(response.present_address);
                var dealerpoint = response.dealer_point_name;
                if (dealerpoint != null) {
                    $("#editdelaer_det").show("");
                    $("#editdealer_point_name").val(dealerpoint);
                    $("#editzonename").val(response.zonename);
                }

                $('#previewto').html('<a target="_blank" href="<?php echo $base_url; ?>/assets/images/uploads/' + response.image_path + '">' + '<img src="' + '<?= $base_url; ?>/' + 'assets/images/uploads/' + response.image_path + '" style="width: 150px; height=350px; border-radius:50px;"/>' + '</a>');
                $('#editmodal').modal("show");
            }
        });

    }


    $("#role").change(function() {
        var roleid = $("#role").val();
        if (roleid == 3) {
            $("#delaer_det").show("");
        }
        else {
            $("#delaer_det").hide("");
        }
        if (roleid == 4) {
            $("#delaerdiv").show("");
        }
        else {
            $("#delaerdiv").hide("");
        }
    });
    $("#editrole").change(function() {
        var roleid = $("#editrole").val();
        if (roleid == 3) {
            $("#editdelaer_det").show("");
        }
        else {
            $("#editdelaer_det").hide("");
        }
    });

    function allowNumbersOnly(e) {
        var code = (e.which) ? e.which : e.keyCode;
        if (code > 31 && (code < 48 || code > 57)) {
            e.preventDefault();
        }
    }



    function previewImages() {

        var $preview = $('#preview').empty();
        if (this.files)
            $.each(this.files, readAndPreview);

        function readAndPreview(i, file) {

            if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                return alert(file.name + " is not an image");
            } // else...

            var reader = new FileReader();

            $(reader).on("load", function() {
                $preview.append($("<img/>", {src: this.result, height: 100}));
            });

            reader.readAsDataURL(file);

        }
    }

    $('#fileToUpload').on("change", previewImages);
</script>