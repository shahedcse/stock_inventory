<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                SR Cancel Order  List

                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Order For</th>
                                            <th>Order From</th>
                                            <th>Create by</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Order For</th>
                                            <th>Order From</th>
                                            <th>Create by</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        foreach ($order_details as $value):
                                            $orderby = $this->db->query("SELECT full_name FROM users WHERE id='$value->order_by' ")->row()->full_name;
                                            ?>
                                            <tr>
                                                <td><?= date("d-m-Y", strtotime($value->created_date)) ?></td>
                                                <td><?= $value->full_name; ?></td>
                                                <td><?= $value->name; ?></td>
                                                <td><?= $orderby ?></td>
                                                <td style="background-color:<?= $value->color; ?> "><?= $value->status_name; ?></td>
                                                <td><?= $value->total_amount; ?></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            ACTION <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?= base_url('Order/vouchar_create?orderid=' . $value->id); ?>"style="color:green;" class=" waves-effect waves-block">View Order</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>


