<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                SR Order  List
                                <a href="<?= base_url('Order/neworder'); ?>">
                                    <button type="button" class=" btn bg-cyan pull-right">Add Order</button>
                                </a>
                            </h2>
                        </div>
                        <?php if ($this->session->userdata('notadd')): ?>
                            <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('notadd');
                                $this->session->unset_userdata('notadd');
                                ?>
                            </div>
                        <?php elseif ($this->session->userdata('add')): ?>
                            <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?=
                                $this->session->userdata('add');
                                $this->session->unset_userdata('add');
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Order For</th>
                                            <th>Order From</th>
                                            <th>Create by</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($order_details as $value):
                                            $orderby = $this->db->query("SELECT full_name FROM users WHERE id='$value->order_by' ")->row()->full_name;
                                            ?>
                                            <tr>
                                                <td><?= date("d-m-Y", strtotime($value->created_date)) ?></td>
                                                <td><?= $value->full_name; ?></td>
                                                <td><?= $value->name; ?></td>
                                                <td><?= $orderby ?></td>
                                                <td style="background-color:<?= $value->color; ?> "><?= $value->status_name; ?></td>
                                                <td><?= $value->total_amount; ?></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            ACTION <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?= base_url('Order/vouchar_create?orderid=' . $value->id); ?>"style="color:green;" class=" waves-effect waves-block">View/Print Vouchar</a></li>
                                                            <?php if ($value->status == 1): ?>
                                                                <li><a href="#" onclick="order_delivered('<?= $value->id; ?>');" style="color:green;" class=" waves-effect waves-block">Order Delivered</a></li>
                                                                <li><a href="#" onclick="order_cancel('<?= $value->id; ?>');" style="color:red;" class=" waves-effect waves-block">Order Cancel</a></li>
                                                            <?php endif; ?>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<div class="modal fade in" id="cancel_modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" action="<?= base_url('Order/order_cancel'); ?>" method="POST">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <center>
                        <input type="hidden" id="order" name="order" class="form-control">
                        <h2 style="color:red;">Are You sure,You want to cancel this order ??</h2>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">YES</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="delivered_modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" action="<?= base_url('Order/order_delivered'); ?>" method="POST">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body">
                    <center>
                        <input type="hidden" id="order1" name="order1" class="form-control">
                        <h2 style="color:green;">Are You sure,You want to make this delivered  ??</h2>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">YES</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

    function order_delivered(id) {
        var order_id = id;
        $('#order1').val(order_id);
        $('#delivered_modal').modal('show');
    }
    function order_cancel(id) {
        var order_id = id;
        $('#order').val(order_id);
        $('#cancel_modal').modal('show');
    }

</script>

