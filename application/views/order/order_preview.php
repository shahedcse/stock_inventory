<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-10 col-lg-offset-1">

                    <div class="card">
                        <?php if ($orderdmaster->status == '4'): ?>
                            <input type="button" class="btn btn-danger pull-right"  value="Canceled Order" />
                        <?php else: ?>
                            <input type="button" class="btn btn-success pull-right" onclick="printDiv('printvouchar')" value="print Order" />
                        <?php endif; ?>

                        <div class="body" id="printvouchar">
                            <table style="font-size: 14px;  width: 100%; " class="table table-striped table-bordered table-hover ">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <div class="col-sm-3">
                                                        <img height="55px;" width="140px;" src="<?= base_url('assets/images/company.jpg'); ?>">
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div style="text-align: center">
                                                            <p>
                                                                <img height="45px;" width="100px;" src="<?= base_url('assets/images/dia_kidz-283x179.png'); ?>">
                                                                <br> Contact: +8801914939632.
                                                                <br> Web: www.littlefeatbd.com
                                                            </p>
                                                        </div>
                                                        <div style="border: 1px solid gray; height: 1px;"> </div>
                                                        <div style="text-align: right">
                                                            <img height="30px;" width="120px;" src="<?= base_url('assets/images/1505477115167095.png'); ?>">
                                                            <br>
                                                            Order No: <?= 'lfkd00' . $orderdmaster->id; ?><br>
                                                            Order Date:<?= date("d-m-Y", strtotime($orderdmaster->created_date)) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div style="text-align: left">
                                                        <?php
                                                        if (!empty($orderdmaster->dealer_id)):
                                                            $dealer_name = $this->db->query("SELECT full_name,dealer_point_name,phone,present_address FROM users WHERE id='$orderdmaster->dealer_id' ")->row();
                                                        endif;
                                                        $srinfo = $this->db->query("SELECT full_name,phone FROM users WHERE id='$orderdmaster->order_by' ")->row();
                                                        ?>
                                                        <b>  Order For </b>: <span id="customername">
                                                            <?php
                                                            if (!empty($orderdmaster->dealer_id)):
                                                                echo $dealer_name->dealer_point_name . '-' . ($dealer_name->full_name);
                                                            endif;
                                                            ?>
                                                        </span>

                                                        <br>
                                                        <b>  Phone </b>: <span id="salesmanname1">  <?php
                                                            echo $dealer_name->phone;
                                                            ?></span>
                                                        <br>
                                                        <b>  Address </b>: <span id="salesdate1">  <?php
                                                            echo $dealer_name->present_address;
                                                            ?></span>
                                                        <br>
                                                        <b>  SR Name </b>: <span id="salesmanname1">  <?php
                                                            echo $srinfo->full_name . '-' . $srinfo->phone;
                                                            ?></span>
                                                        <br>
                                                        <br>

                                                    </div>
                                                </div>


                                                <div class="adv-table">
                                                    <table class="display table table-bordered table-striped edit-table" id="cloudAccounting1" style="font-size: 10px">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">Sl</th>
                                                                <th class="text-center">Product Name</th>
                                                                <th class="text-center">Price</th>
                                                                <th class="text-center">Qty</th>
                                                                <th class="text-center">Amount</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="addprintrowoffice">
                                                            <?php
                                                            $id = 0;
                                                            foreach ($orderdetails as $value):
                                                                $id++;
                                                                $tradeprice = $this->db->query("SELECT trade_price FROM productbatch WHERE productId='$value->product_id'");
                                                                if (!empty($tradeprice)):
                                                                    $price = $tradeprice->row()->trade_price;
                                                                else:
                                                                    $price = "";
                                                                endif;
                                                                ?>
                                                                <tr id="rowoffice1">
                                                                    <td style="text-align: center"><?= $id; ?> </td>
                                                                    <td style="text-align: center"><?= $value->sublist_name; ?> </td>
                                                                    <td style="text-align: center"><?= $price; ?> </td>
                                                                    <td style="text-align: center"><?= $value->quantity; ?> </td>
                                                                    <td style="text-align: center; "><?= $value->total; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>

                                                            <tr>
                                                                <td style="text-align: right"colspan="4">Total: </td>
                                                                <td style="text-align: center"><?= $orderdmaster->total_amount; ?></td>
                                                            </tr>
                                                        </tbody>

                                                    </table>
                                                    <ul class="list-group pull-right">
                                                        <li class="list-group-item">
                                                            <table style=" text-align:right; width:100%; font-size: 10px; font-weight: bold;margin-bottom: 5px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Net Total Amount: </td>
                                                                        <td id="invoiceamount1" style="text-align:right">
                                                                            <?= $orderdmaster->total_amount; ?>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </li>
                                                    </ul>


                                                    <div class="row clearfix" style="padding-top: 10px;padding-bottom: 10px;">
                                                        <div class="col-sm-6" style="text-align: center;">
                                                            Receivers Signature:
                                                        </div>
                                                        <div class="col-sm-6" style="text-align: center;">
                                                            Authorised Signature:
                                                        </div>
                                                    </div>

                                                    <table style="float:left; text-align: center; margin: 10px 5px 0px 5px; font-size: 10px">
                                                        <tbody>
                                                            <tr>
                                                                <td> House-5, Road-8, Nikunja, Dhaka-1229<td>
                                                                <td >Thanks for choosing little Feat as a business partner.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>



