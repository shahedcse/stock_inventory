<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <?php if ($this->session->userdata('notadd')): ?>
                    <div class="alert bg-red alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?=
                        $this->session->userdata('notadd');
                        $this->session->unset_userdata('notadd');
                        ?>
                    </div>
                <?php elseif ($this->session->userdata('add')): ?>
                    <div class="alert bg-green alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?=
                        $this->session->userdata('add');
                        $this->session->unset_userdata('add');
                        ?>
                    </div>
                <?php endif; ?>
                <form class="form-horizontal" action="<?= base_url('Order/order_insert'); ?>" method="POST">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <div class="row clearfix" style="background-color: honeydew; padding: 5px;">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" disabled="" value="<?= date('Y-m-d'); ?>" class="form-control" placeholder="col-sm-3">
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($userrole == 1 || $userrole == 2): ?>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select class="form-control show-tick" data-live-search="true" required="" id="dealer_id" name="dealer_id">
                                                        <option value="">-Select Dealer -</option>
                                                        <?php foreach ($dealer_list AS $value): ?>
                                                            <option value="<?= $value->id; ?>"><?= $value->dealer_point_name; ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <?php
                                                $userid = $this->session->userdata('user_id');
                                                $dealerid = $this->db->query("SELECT dealerid FROM users WHERE id='$userid'")->row()->dealerid;
                                                $dealername = $this->db->query("SELECT dealer_point_name FROM users WHERE id='$dealerid'")->row()->dealer_point_name;
                                                ?>
                                                <div class="form-line">
                                                    <input type="text" readonly=""  value="<?= $dealername; ?>" class="form-control" >
                                                    <input type="hidden"  name="dealer_id" id="dealer_id" value="<?= $dealerid; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                    <?php endif; ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="demo-radio-button">
                                                <input name="order_type" onclick=" return makechange()" type="radio" value="2" id="radio_2" checked="">
                                                <label for="radio_2">Re Order</label>
                                                <input name="order_type" onclick=" return makechange()" type="radio" value="1" id="radio_1"  >
                                                <label for="radio_1">New Order</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" data-live-search="true" required="" id="customer_name" name="customer_name">
                                                    <option value="">-Select Customer -</option>
                                                    <?php foreach ($customer_list AS $value): ?>
                                                        <option value="<?= $value->id; ?>"><?= $value->name; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <button type="button" onclick="customeradd_modal()" style="display: none" id="addnew111" class="btn-success">New Customer</button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="body table-responsive">
                                <table class="table table-bordered table-sm" style="cursor: pointer; border: 1px solid black;font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Product Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($product_list AS $value):
                                            $tradeprice = $this->db->query("SELECT trade_price FROM productbatch WHERE productId='$value->id'");
                                            if (!empty($tradeprice)):
                                                $price = $tradeprice->row()->trade_price;
                                            else:
                                                $price = "";
                                            endif;
                                            ?>
                                            <tr>
                                                <td style="text-align: center;" >
                                                    <?= $value->sublist_name; ?> - <?= $value->pack_size; ?>
                                                    <input  id="productid" name="productid[]" type="hidden" class="form-control" value="<?= $value->id; ?>">
                                                </td>
                                                <td style="text-align:center;">
                                                    <input id="price" name="price" style="width: 85px; background-color:#7dad5b ;color:#fff; height:25px; " type="text" readonly="" class="form-control unit-price " value=" <?php
                                                    if (!empty($price)):
                                                        echo floor($price);
                                                    else:
                                                        echo '0';
                                                    endif;
                                                    ?>">

                                                </td>
                                                <td style="text-align: center;"><input   onkeypress="return isNumberKey(event)" style="width: 85px;height:25px;" id="qty" name="qty[]" type="number" class="form-control auto-calc amount" value="0"></td>
                                                <td style="text-align: center;"><input id="total" style="width: 105px; height:25px; background-color: #32efe9  " name="total[]"  type="text" readonly="" class="form-control total-cost "></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td colspan="3" style="text-align: right;">Total :</td>
                                            <td><input type="text" name="total_amount"id="total-invoice" readonly=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 pull-right">
                                        <a href="<?= base_url('Order/neworder'); ?>">  <button type="button"  class="btn btn-default m-t-15 waves-effect">Reset</button></a>
                                        <button type="submit" id="order" class="btn btn-success m-t-15 waves-effect">Submit Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
    </section>
</body>
<div class="modal fade" id="usermodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00BCD4;color:#fff;">
                <h4 class="modal-title" id="defaultModalLabel">Add New Customer Info. :</h4>
            </div>
            <form class="form-horizontal form-group" enctype="multipart/form-data" action="<?= base_url('Userinfo/addcustomer2'); ?>" method="POST">
                <div class="modal-body " >
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Name :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="name" required="" name="name" class="form-control" placeholder="Enter Name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Phone :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" required=""  id="phone" name="phone" class="form-control" placeholder="Enter Phone No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
                            <label for="password_2">Address :</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="address" id="address" cols="20" rows="1" class="form-control no-resize"  aria-required="true" aria-invalid="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default m-t-15 waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).on("keyup change paste", "td > input.auto-calc", function() {

        // Determine parent row
        row = $(this).closest("tr");

        // Get first and second input values
        first = row.find("td input.unit-price").val();
        second = row.find("td input.amount").val();

        // Print input values to output cell
        row.find(".total-cost").val(first * second);


        // Update total invoice value
        var sum = 0;
        // Cycle through each input with class total-cost
        $("input.total-cost").each(function() {
            // Add value to sum
            sum += +$(this).val();
        });

        // Assign sum to text of #total-invoice
        // Using the id here as there is only one of these
        $("#total-invoice").val(sum);


    });
    function makechange() {
        var type = $('input[name=order_type]:checked').val();
        if (type == 1) {
            $("#addnew111").show();
        }
        else {
            $("#addnew111").hide();
        }
    }

    function customeradd_modal() {
        $('#usermodal').modal("show");
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>


