<!DOCTYPE html>
<html>
    <style>
        #container {
            height: 382px; 
        }
    </style>
    <body class="theme-red">
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <?php
                    $app_title = $this->db->query("SELECT  app_title FROM system where id='1'")->row()->app_title;
                    ?>
                    <h2><marquee style="color:orangered;font-size: 24px;">Welcome To <?= $app_title; ?> .</marquee></h2>
                </div>

                <!-- Widgets -->
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-pink hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text">TOTAL PRODUCT</div>
                                <div class="number count-to" data-from="0" data-to="<?= $total_prod; ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-cyan hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">person_add</i>
                            </div>
                            <div class="content">
                                <div class="text">NO OF DEALER</div>
                                <div class="number count-to" data-from="0" data-to="<?= $total_dealer; ?>" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-light-green hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">person_add</i>
                            </div>
                            <div class="content">
                                <div class="text">NO OF VENDOR</div>
                                <div class="number count-to" data-from="0" data-to="<?= $total_vendor; ?>" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-orange hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">money's</i>
                            </div>
                            <div class="content">
                                <div class="text">SALE THIS MONTH</div>
                                <div class="number count-to" data-from="0" data-to="<?= $total_sale; ?>" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-purple hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">money's</i>
                            </div>
                            <div class="content">
                                <div class="text">Current Stock price</div>
                                <div class="number "  data-to="" data-speed="1000" data-fresh-interval="20"><span id="price"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-black hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">money's</i>
                            </div>
                            <div class="content">
                                <div class="text">Due Amount In Market</div>
                                <div class="number count-to" data-from="0" data-to="<?= $totaldue; ?>" data-speed="500" data-fresh-interval="15"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Widgets -->
                <!-- CPU Usage -->
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="header">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                        <h2>Server Bandwidth (%)</h2>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <div class="switch panel-switch-btn">
                                            <span class="m-r-10 font-12">REAL TIME</span>
                                            <label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>
                                        </div>
                                    </div>
                                </div>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div id="real_time_chart" class="dashboard-flot-chart"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="card">
                            <figure class="highcharts-figure">
                                <div id="container"></div>

                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2>Recent Product In Stock</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover dashboard-task-infos" id="res">
                                        <thead>
                                            <tr>
                                                <th>Sl</th>
                                                <th>Product Name</th>
                                                <th>Quantity</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sl = 0;
                                            foreach ($stock_info as $value):
                                                $sl++;
                                                $stockuser = $this->db->query("SELECT GROUP_CONCAT(`id`) as user FROM users WHERE `role`IN(1,2) ")->row()->user;
                                                $stockinqr = $this->db->query("SELECT SUM(inwardQuantity) AS qty FROM stockposting WHERE productBatchId='$value->productid' AND destinationId IN ($stockuser) AND stock_type='1'")->row()->qty;
                                                $stockoutqr = $this->db->query("SELECT SUM(outwardQuantity) AS qty1 FROM stockposting WHERE productBatchId='$value->productid' AND stock_type='2'")->row()->qty1;
                                                $stock = ($stockinqr - $stockoutqr);
                                                $detailsQr = $this->db->query("SELECT rate,product_unit.unit_name  FROM stockposting JOIN product_unit ON product_unit.id=stockposting.unitId WHERE productBatchId='$value->productid'");
                                                $rate = $detailsQr->row()->rate;
                                                ?>
                                                <tr>
                                                    <td><?= $sl; ?></td>
                                                    <td ><span class="label bg-blue"><?= $value->productname ?></span></td>
                                                    <td><span class="label bg-cyan"><?= $stock; ?> </span></td>
                                                    <td class="countables" style="display:none;"><span class="label bg-primary"><?= $stock * $rate ?> </span></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </body>

</html>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    $(document).ready(function() {
        var sum = 0;
        var table = document.getElementById("res");
        var ths = table.getElementsByTagName('th');
        var tds = table.getElementsByClassName('countables');
        for (var i = 0; i < tds.length; i++) {
            sum += isNaN(tds[i].innerText) ? 0 : parseInt(tds[i].innerText);
        }
        $("#price").html(sum);

    });


    var chart = Highcharts.chart('container', {
        title: {
            text: ''
        },
        subtitle: {
            text: 'Month Wise Sale Report'
        },
        xAxis: {
            categories: [<?php foreach ($period as $p): ?> '<?= $p ?>', <?php endforeach; ?>]
        },
        series: [{
                type: 'column',
                colorByPoint: true,
                data: [<?= implode(',', $totaldata) ?>],
                showInLegend: false
            }]

    });
</script>
